<?php
// HTTP
define('HTTP_SERVER', 'http://new.ardicraft.com/');

// HTTPS
define('HTTPS_SERVER', 'http://new.ardicraft.com/');

// DIR
$dir = dirname(__FILE__);
define('DIR_APPLICATION', $dir . '/catalog/');
define('DIR_SYSTEM', $dir . '/system/');
define('DIR_LANGUAGE', $dir . '/catalog/language/');
define('DIR_TEMPLATE', $dir . '/catalog/view/theme/');
define('DIR_CONFIG', $dir . '/system/config/');
define('DIR_IMAGE', $dir . '/image/');
define('DIR_CACHE', $dir . '/system/storage/cache/');
define('DIR_DOWNLOAD', $dir . '/system/storage/download/');
define('DIR_LOGS', $dir . '/system/storage/logs/');
define('DIR_MODIFICATION', $dir . '/system/storage/modification/');
define('DIR_UPLOAD', $dir . '/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'new.ardicraft.com');
define('DB_PASSWORD', '0C7j8V1b');
define('DB_DATABASE', 'new.ardicraft.com');

define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

define('CACHE_DRIVER', 'apc');
define('CACHE_PREFIX', DB_DATABASE);
if (FALSE !== strpos($_SERVER['HTTP_USER_AGENT'], 'Google Page Speed')) {
	define('FORCE_INLINE_OPTIMIZATION', 1);
}
