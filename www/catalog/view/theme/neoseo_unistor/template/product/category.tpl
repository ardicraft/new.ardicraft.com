<?php echo $header; ?>
<?php $detect = new Mobile_Detect(); ?>
<!-- NeoSeo Product Link - begin -->
<?php if (isset( $edit_link ) ) { ?>
<script>
    $(document).ready(function(){
        $("h1").after('<div class="edit"><a target="_blank" href="<?php echo $edit_link; ?>">Редактировать ( видит только админ )</a></div>');
    });
</script>
<?php } ?>
<!-- NeoSeo Product Link - end --><div class="container">
    <div class="category-main-container">
        <?php if (file_exists(DIR_MODIFICATION . '/catalog/view/theme/neoseo_unistor/template/common/breadcrumbs.tpl')) { ?>
        <?php require_once(DIR_MODIFICATION . '/catalog/view/theme/neoseo_unistor/template/common/breadcrumbs.tpl'); ?>
        <?php } else { ?>
        <?php  require_once(DIR_TEMPLATE . 'neoseo_unistor/template/common/breadcrumbs.tpl'); ?>
        <?php } ?>
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-12 col-md-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class=" <?php echo $class; ?>"><?php echo $content_top; ?>
                <div class="content-top box-shadow box-corner">
                    <h1><span><?= $heading_title; ?></span><?php echo $sharing_code; ?></h1>
                    <? if ($products) : ?>
                    <? if ($categories && $subcategories_show) : ?>
                    <div class="row-categories">

                        <? foreach ($categories as $subcategory) : ?>
                        <div class="information">
                            <div class="image">
                                <a href="<?php echo $subcategory['href']; ?>">
                                    <img src="<?php echo $subcategory['thumb']; ?>" alt="<?php echo $subcategory['name']; ?>" title="<?php echo $subcategory['name']; ?>" class="img-responsive"/>
                                </a>
                            </div>
                            <div class="subcategory-link"><a href="<?php echo $subcategory['href']; ?>" ><?php echo $subcategory['name']; ?></a></div>
                        </div>
                        <? endforeach; ?>

                    </div>
                    <? endif; ?>
                    <? endif; ?>
                    <?php if (($thumb || $description) && !$desc_position) { ?>

                    <div class="row">
                        <?php if ($thumb) { ?>
                        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail"/></div>
                        <?php } ?>
                        <?php if ($description ) { ?>
                        <div class="<?php echo $thumb ? 'col-sm-10' : 'col-sm-12'; ?>">
                            <article class="article-description" style="overflow-y: auto;height: 300px;"><?php echo $description; ?></article>
                        </div>
                        <?php } ?>

                    </div>
                    <?php } ?>
                    <?php echo $sharing_code; ?>
                </div>

                <?php if ($products) { ?>
					<script type="text/javascript" src="catalog/view/theme/neoseo_unistor/javascript/jquery.formstyler.js"></script>
					<script>
                        (function($) {
                            $(function() {
                                $('.filters-box .selectpicker').styler({
                                    selectSearch: true
                                });
                            });
                        })(jQuery);
					</script>
                <?php if (!$detect->isMobile() || !$detect->isMobile()) { ?>
                <div class="filters-box box-shadow box-corner">
                    <div class="sort-list col-xs-12 col-sm-12 col-md-6 col-lg-5">
                        <div class="col-md-5 text-right sort-div">
                            <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                        </div>
                        <div class="col-md-7 text-left">
                            <select id="input-sort" class="selectpicker" onchange="location = this.value;">
                                <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="show-list col-sm-4 hidden-xs hidden-sm">
                        <div class="col-md-6 text-right limit-div">
                            <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                        </div>
                        <div class="col-md-6 text-right">
                            <select id="input-limit" class="selectpicker" onchange="location = this.value;">
                                <?php foreach ($limits as $limits) { ?>
                                <?php if ($limits['value'] == $limit) { ?>
                                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="style-list col-sm-3 text-center hidden-xs hidden-sm">
                        <div class="btn-group">
                            <button type="button" id="list-view" class="btn btn-view-type btn-default<?php if ($category_view_type == 'list') { echo ' active'; } ?>" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
                            <button type="button" id="grid-view" class="btn btn-view-type btn-default<?php if ($category_view_type == 'grid') { echo ' active'; } ?>" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th-large" aria-hidden="true"></i></i></button>
                            <button type="button" id="table-view" class="btn btn-view-type btn-default<?php if ($category_view_type == 'table') { echo ' active'; } ?>" data-toggle="tooltip" title="<?php echo $button_table; ?>"><i class="fa fa-list"></i></button>
                        </div>
                    </div>
                    <div class="filter-container visible-xs visible-sm">
                        <button type="button" style="border-radius: 4px;" onclick="showFilter();return true;">
                            <i class="fa fa-sliders" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <?php } else { ?>
                <div class="portable-sorts">
                    <div class="portable-sorts__filter">
                        <button type="button" onclick="showFilter();return true;">
                            <span>Фильтровать</span>
                            <i class="fa fa-filter"></i>
                        </button>
                    </div>
                    <div class="portable-sorts__sort">
                        <button onclick="$(this).next().toggle();">
                            <span>Сортировать</span>
                            <i class="fa fa-sliders"></i>
                        </button>
                        <div class="portable-sorts__sort-list">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <a onclick="location = this.value;" href="<?php echo $sorts['href']; ?>" class="active"><?php echo $sorts['text']; ?></a>
                            <?php } else { ?>
                            <a onclick="location = this.value;" href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <div class="row products-content">
                    <?php foreach ($products as $product) { ?>
                    <div class="product-layout product-<?php echo $category_view_type; ?> col-xs-12">
                        <div itemscope="" class="product-thumb box-shadow box-corner clearfix">
                            <div class="product-thumb_top">

                            <?php if( isset($product['labels']) && count($product['labels'])>0 ) { ?>
                            <!-- NeoSeo Product Labels - begin -->
                            <?php foreach($product['labels'] as $label_wrap => $group_label) { ?>
                                <?php foreach($group_label as $label) { ?>
                                    <div class="product-preview-label <?php echo $label['label_type']; ?> <?php echo $label['position']; ?> <?php echo $label['class']; ?>">
                                        <span style="<?php echo $label['style']; ?>">
                                            <?php echo $label['text']; ?>
                                        </span>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <!-- NeoSeo Product Labels - end -->
                            <?php } ?>
                                <div class="image">
                                    <a href="<?php echo $product['href']; ?>">
                                        <?php if ($product['thumb']) { ?>
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="hoverable img-responsive" data-over="<?php echo $product['thumb1']; ?>" data-out="<?php echo $product['thumb']; ?>" />
                                        <?php } else { ?>
                                        <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                            <div class="product-thumb_middle">
                                <div class="rating-container">
                                    <div class="caption">
                                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                    </div>
                                    <span class="rating">
                                    <?php if($product['rating']){ ?>
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($product['rating'] < $i) { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <?php } else { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                    <?php }?>
                                    <?php } ?>
                                    <?php } ?>
                                    </span>
                                </div>
                                <div class="caption">
                                    <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                    <?php if ($product['discounts']) { ?>
                                    <ul class="product-discount">
                                        <?php foreach ($product['discounts'] as $discount) { ?>
                                        <li><?= $discount['price'] ?> <?= $text_from; ?> <?= $discount['quantity']; ?> <?= $text_by_count; ?></li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </div>

                                <?php if ($product['price']) { ?>
                                <div class="price-and-cart-add">
                                    <div class="price-wrapper">
                                        <p class="price">
                                            <?php if (!$product['special']) { ?>
                                            <?php echo $product['price']; ?>
                                            <?php } else { ?>
                                            <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span>
                                            <?php } ?>
                                            <?php if ($product['tax']) { ?>
                                            <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                            <?php } ?>
                                        </p>
                                    </div>
                                    <?php } ?>
                                    <div class="input-group input-quantity-group" data-min-quantity="<?php echo $product['minimum']; ?>">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" data-type="minus" data-field="input-quantity">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                  </span>
                                        <input type="text" name="quantity" value="<?php echo $product['minimum']; ?>" size="2" class="form-control quantity">
                                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-type="plus" data-field="input-quantity">
                                      <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                    </span>
                                    </div>

                                    <div class="button-group-cart">
                                        <span class="text-right stock-status-text-<?php echo $product['stock_status_id']; ?>" style="color:<?php echo $product['stock_status_color'] ?>;"><?php echo $product['stock_status']; ?></span>
                                        <button class="cart-add-button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs"><?php echo $button_cart; ?></span></button>
                                    </div>
                                </div>
                            </div>
                            <div class="labels-wrap-table">
    <!-- NeoSeo Product Labels - begin -->
                        <?php if( isset($product['labels']) && count($product['labels'])>0 ) { ?>
                            <?php foreach($product['labels'] as $label_wrap => $group_label) { ?>
                                    <div class="wr-tag <?php echo $label_wrap; ?>">
                                       <?php foreach($group_label as $label) { ?>
                                       <div class="tag <?php echo $label['label_type']; ?> <?php echo $label['position']; ?> <?php echo $label['class']; ?>"><span style="<?php echo $label['style']; ?>"><?php echo $label['text']; ?></span></div>
                                       <?php } ?>
                                    </div>
                            <?php } ?>
                        <?php } ?>
                        <!-- NeoSeo Product Labels - end -->
</div>
                            <div class="description">
                                <div class="description-top">
                                    <?php if (isset($product['additional_attributes']) && $product['additional_attributes']) { ?>
                                    <div class="attributes-top">
                                        <?php $counter = 1; ?>
                                        <?php foreach ($product['additional_attributes'] as $key => $attribute) { ?>
                                        <span><b><?php echo $attribute['name']; ?></b> <?php echo $attribute['text']; ?></span><?php if ($counter < $product['total_attributes']) { echo $divider ? $divider : ''; } ?>
                                        <?php $counter++;
											} ?>
                                    </div>
                                    <?php } ?>
                                    <div class="description-text"><?php echo $product['short_description']; ?></div>
                                </div>
                                <div class="description-bottom">
                                    <div class="button-group">
                                        <a class="wishlist-button" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                            <i class="fa fa-heart"></i>
                                            <span><?php echo $text_wishlist; ?></span>
                                        </a>
                                        <a class="compare-button" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                            <i class="fa fa-exchange"></i>
                                            <span><?php echo $text_compare; ?></span>
                                        </a>
                                        <?php if($neoseo_quick_order_status) { ?>
                                        <a class="buy-one-click" onclick="showQuickOrder('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                                            <i class="ns-mouse" aria-hidden="true"></i>
                                            <span><?php echo $text_one_click_buy; ?></span>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="row paginator">
                    <div class="col-sm-7 col-md-12 col-lg-7 text-left"><?php echo $pagination; ?></div>
                    <div class="col-sm-5 col-md-12 col-lg-5 text-right"><?php echo $results; ?></div>
                </div>
                <?php if (($thumb || $description) && $desc_position) { ?>
                <div class="content-bottom box-shadow box-corner">
                    <div class="row">

                        <?php if ($thumb) { ?>
                        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail"/></div>
                        <?php } ?>
                        <?php if ($description ) { ?>
                        <div class="<?php echo $thumb ? 'col-sm-10' : 'col-sm-12'; ?>">
                            <article class="article-description" style="overflow-y: auto;height: 300px;"><?php echo $description; ?></article>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>


                <?php } ?>

                <?php if (!$products) { ?>
                <?php if ($categories && $subcategories_show) { ?>
                <div class="row products-content">
                    <?php foreach ($categories as $subcategory) { ?>
                    <div class="category-layout col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="category-thumb box-shadow box-corner ">
                            <div class="image">
                                <a href="<?php echo $subcategory['href']; ?>">
                                    <img src="<?php echo $subcategory['thumb']; ?>" alt="<?php echo $subcategory['name']; ?>" title="<?php echo $subcategory['name']; ?>" class="img-responsive"/>
                                </a>
                            </div>
                            <div class="category-caption">
                                <a href="<?php echo $subcategory['href']; ?>" ><?php echo $subcategory['name']; ?></a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php }
                else { ?>
                <div class="empty-box box-shadow box-corner">
                    <p class="empty-title"><?php echo $text_empty; ?></p>
                </div>
                <?php } ?>
                <?php if (($thumb || $description) && $desc_position) { ?>
                <div class="content-bottom box-shadow box-corner">
                    <div class="row">
                        <?php if ($thumb) { ?>
                        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail"/></div>
                        <?php } ?>
                        <?php if ($description ) { ?>
                        <div class="<?php echo $thumb ? 'col-sm-10' : 'col-sm-12'; ?>">
                            <article class="article-description" style="overflow-y: auto;height: 300px;"><?php echo $description; ?></article>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>

                <?php } ?>

            </div>
            <?php echo $column_right; ?>
        </div>
        <?php echo $content_bottom; ?>


    </div>
</div>
<script>

    // input-quantity
    $('.input-quantity-group .input-group-btn button').on('click', function () {
        var field = $(this).attr('data-field');
        if( !field ) return;
        var type = $(this).attr('data-type');
        if( !type ) return;

        var I = $(this).parent().siblings('input:first');
        var o = $(this).parents('.input-quantity-group');
        var min = $(this).parents('.input-quantity-group').attr('data-min-quantity');
        var value = Number(I.val());
        if( type == "minus") {
            value -= 1;
        } else {
            value += 1;
        }
        if( value < min ) { setInvalid(I); value = min; }
        I.val(value);
        setQuantity(value,o);
    });
    $('.input-quantity-group input.quantity').keydown(function(e) {
        if(e.which == 38){ // plus
            var value = Number($(this).val());
            var o = $(this).parents('.input-quantity-group');
            value++;
            if(value > 100) value = 100;
            $(this).val(value);
            setQuantity(value, o);
        }
        if(e.which == 40){ // minus
            var value = Number($(this).val());
            var o = $(this).parents('.input-quantity-group');
            var min = $(this).parents('.input-quantity-group').attr('data-min-quantity');
            value--;
            if(value < min) { setInvalid($(this)); value = min; }
            $(this).val(value);
            setQuantity(value, o);
        }
    });
    function setQuantity(v,o){
        o.siblings('.button-group').find('button').each(function(){
            var t = $(this).attr('onclick');
            if( t.indexOf('cart.add') > -1 ){
                var i = t.slice(9,-2).split(',')[0].slice(1,-1);
                var q = "c = cart;c.add('" + i + "', '" + v + "');";
                $(this).attr("onclick",q);
            }
        })
    }
    function setInvalid(o){
        o.addClass('invalid');
        setTimeout(function(){
            o.removeClass('invalid');
        },120);
    }

</script>
<?php echo $footer; ?>
