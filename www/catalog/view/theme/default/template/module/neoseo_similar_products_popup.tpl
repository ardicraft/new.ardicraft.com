<div class="popupTitle"><span><?php echo $heading_title; ?></span>
    <div class="customNavigation"><a class="btn btn-secondary prev"><i class="icon-arrow-pointing-left"></i></a><a class="btn btn-secondary next"><i class="icon-right-arrow"></i></a></div>
</div>
<div class="owl-fullwidth">
    <div id="owl-analog" class="owl-carousel activeminus">
<?php foreach ($products as $product) { ?>
        <div class="item">
            <div class="product">
                <div class="productInner">
                    <div class="product-pic">
                        <i class="icon-plus-zoom-or-search-symbol-of-interface zoom"></i>
                        <div class="tag"><span class="hit">Хит</span></div><a href="#" class="mainImage"><img alt="" src="img/product.jpg"></a>
                        <div class="morePics"><span class="slideUp"><i class="icon-arrow-down-sign-to-navigate"></i></span>
                            <div class="listOuter">
                                <ul>
                                    <li><img alt="" src="img/product/main-image.jpg"></li>
                                    <li><img alt="" src="img/product.jpg"></li>
                                    <li><img alt="" src="img/product/main-image.jpg"></li>
                                    <li><img alt="" src="img/product.jpg"></li>
                                    <li><img alt="" src="img/product/main-image.jpg"></li>
                                    <li><img alt="" src="img/product.jpg"></li>
                                </ul>
                            </div><span class="slideDown"><i class="icon-arrow-down-sign-to-navigate"></i></span>
                        </div>
                    </div>
                    <div class="product-allOther">
                        <div class="descPart">
                            <a href="#" class="product-title">Дырокол Leitz 50050025 NeXXt 25/04116 25 листов пластиковый красный</a>
                            <div class="ratingandfeedback">
                                <input data-show-caption="false" data-size="xxs" data-show-clear="false" value="3" data-min="0" data-max="5" data-step="0.5" displayOnly="true" class="ratingMy"><span class="revCount"><a href="#">3 Отзыва</a></span>
                            </div>
                            <div class="productFeatures">
                                <ul>
                                    <li data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Обзор товара" class="video"><i class="icon-video-camera"></i></li>
                                    <li data-toggle="tooltip" data-placement="bottom" title="" data-original-title="В избранное" class="favorites"><i class="icon-favorite-heart-button"></i></li>
                                    <li data-toggle="tooltip" data-placement="bottom" title="" data-original-title="К сравнению" class="comparison"><i class="icon-comparison"></i></li>
                                </ul>
                                <div class="code">Арт.:<span>25040307</span></div>
                                <div class="shortDesc">Бумага цвета «слоновая кость», плотность 70 г / Две закладки: белого и светло-коричневого цвета...</div>
                                <div class="shortDescMore">Корпус выполнен из качественного ударопрочного ABS пластика. Основание и все рабочие механизмы металлические. Пробивная способность до 10 листов плотностью 80г/м, за один раз. Оснащен выдвижной ограничительной линейкой с точной шкалой форматов бумаги... </div>
                            </div>
                        </div>
                        <div class="priceRetail">
                            <div class="priceInfo"><span class="number">240.00</span><span class="curr">грн</span>
                            </div>
                            <div class="priceActions">
                                <div class="productQuantity">
                                    <div class="productQuantityTitle">Кол-во шт.</div>
                                    <div class="productQuantityInput"><span class="minus">–</span>
                                        <input type="number" id="retailInput" name="retailInput" value="1" class="QuantityInput"><span class="plus">+</span>
                                    </div>
                                </div>
                                <div class="productBuy"><a class="toCart"><i class="icon-shopping-cart"></i><span>купить</span></a></div>
                            </div>
                        </div>
                        <div class="priceWhole">
                            <div class="priceTitle"><span>от 20 шт. и более</span></div>
                            <div class="priceInfo"><span class="number">224.00</span><span class="curr">грн</span>
                            </div>
                            <div class="priceActions">
                                <div class="productQuantity">
                                    <div class="productQuantityTitle">Кол-во шт.</div>
                                    <div class="productQuantityInput"><span class="minus">–</span>
                                        <input type="number" id="wholeInput" name="wholeInput" value="20" data-minimum="20" class="QuantityInput"><span class="plus">+</span>
                                    </div>
                                </div>
                                <div class="productBuy"><a class="toCart"><i class="icon-package-transport-for-delivery1"></i><span>купить</span></a></div>
                            </div>
                        </div>
                        <div class="codeRow">Арт.:<span>25040307</span></div>
                        <ul class="mobileActions">
                            <li class="zoomM"><a href="#"><i class="icon-plus-zoom-or-search-symbol-of-interface"></i></a></li>
                            <li class="favorites"><a href="#"><i class="icon-favorite-heart-button"></i></a></li>
                            <li class="mobileCart"><a href="#"><i class="icon-shopping-cart"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>
    </div>
    <div class="owlScroll">
        <div class="owlScrollFull"></div>
        <div class="owlScrollInner"><span></span><span></span><span></span></div>
    </div>
</div>
