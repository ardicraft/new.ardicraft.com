<?php
$_['text_select_region'] = "Select correct options";
$_['text_btn_cancel'] = 'Cancel';
$_['text_not_detected'] = 'Not determined';
$_['text_region_is'] = 'Your region: ';
$_['text_select_correct'] = 'If not, choose the correct one.';
$_['text_region_notice'] = 'If we detect your region incorrectly, enter it yourself';
$_['text_detect'] = 'Select';
$_['text_ok'] = 'Confirm';
$_['text_language_selector'] = 'Select language';
$_['text_currency_selector'] = 'Select currency';
$_['text_language_notice'] = 'Selected language: ';
$_['text_currency_notice'] = 'Selected currency: ';
$_['text_selected_region'] = 'Selected region';