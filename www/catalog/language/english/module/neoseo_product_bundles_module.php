<?php

$_['button_bundle_buy'] = 'Buy kit';
$_['text_economy'] = 'You save:';
$_['text_your_product'] = 'Your product';
$_['text_select'] = 'Select';
$_['text_required_options_head'] = 'To complete the kit purchase, select options.';
$_['text_no_options'] = 'No options for product';