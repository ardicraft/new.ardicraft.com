<?php

// Heading
$_['actions_heading'] = 'Actions';
$_['text_action_more'] = 'Learn more';

$_['action_status_list'][0] = 'Present';
$_['action_status_list'][1] = 'Discount';
$_['action_status_list'][2] = '';

$_['action_status_data'][0] = 'Present';
$_['action_status_data'][1] = 'Discount';
$_['action_status_data'][2] = '';

$_['read_more'] = 'Detail';
$_['till_finish'] = 'Until the end of the action';
$_['action_finish'] = 'Action has ended';


$_['days_left'] = 'Days';
$_['hours_left'] = 'Hours';
$_['minutes_left'] = 'Min.';
$_['seconds_left'] = 'Sec.';
$_['text_action_title'] = 'Products involved in the action';
