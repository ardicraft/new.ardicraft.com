<?php

// Heading
$_['heading_title'] = 'Request a call back';

// Text
$_['text_prompt'] = 'Enter your details';
$_['text_name'] = 'Your name';
$_['text_phone'] = 'Contact number';
$_['text_email'] = 'Email';
$_['text_message'] = 'What are you interested in';


// Button
$_['button_continue'] = 'Continue shopping';
$_['button_action'] = 'Waiting for a call';
