<?php
// Text
$_['text_characteristics_title']              = 'Characteristics';
$_['text_product_action']                     = 'Special promotions! Super stock prices from our store!';
$_['text_follow_price']                       = 'Keep track of the price';
$_['text_all_characteristics']                = 'See all features';
$_['text_see_all']                            = 'See all';
$_['text_collapse_all']                       = 'Collapse';
$_['text_more']                               = 'Read completely';
$_['text_comment_answer']                     = 'Reply';
$_['text_reviews_quantity']                   = 'Reviews';
$_['text_give_feedback']                      = 'Give feedbacks';
$_['text_see_all_reviews']                    = 'See all reviews';
$_['text_shipping']                           = 'Delivery';
$_['text_payment']                            = 'Payment';
$_['text_guarantee']                          = 'Warranty';
$_['text_wishlist_menu']                      = 'Featured Products';
$_['text_compare_menu']                       = 'Compare Products';

$_['text_social'] = 'Login with your social profile:';
$_['text_quickview'] = 'Quick view';
$_['prev_text'] = 'Previous';
$_['next_text'] = 'Next';
$_['text_read_more'] = 'Read more';
$_['text_read_less'] = 'Read less';
$_['text_instock'] = 'In Stock';
$_['text_no_phone'] = 'Enter your telephone';
$_['text_dimension'] = 'Dimensions (L x W x H):';
$_['text_watched'] = 'Watched products';
$_['text_compare'] = 'Compare';
$_['text_wishlist'] = 'Wishlist';
$_['text_compare'] = 'Compare';
$_['text_wishlist_menu'] = 'Wishlist';
$_['text_callback_menu'] = 'Callback';
$_['text_all_categories'] = 'All categories';
$_['text_price'] = 'Price:';
$_['text_subscribe_title'] = 'Learn about new promotions and latest products';
$_['text_subscribe_par'] = 'And get news about promotions and special offers';
$_['text_payments_title'] = 'We accept';
$_['text_tax'] = 'Ex Tax:';
$_['text_work_hours'] = 'Work hours:';
$_['text_information'] = 'Information';
$_['text_callback'] = 'Callback';
$_['text_social'] = 'Enter with social';
$_['text_need_account'] = 'You don\'t have an account?';
$_['text_or'] = '<span>or</span>';
$_['text_enter'] = 'Enter the store';
$_['text_forgotten'] = 'I forgot';
$_['text_password'] = 'Password';
$_['text_email'] = 'Email';
$_['text_track_order'] = 'Track order';
$_['text_category'] = 'Category';
$_['text_items'] = '<div class="cart__total-products"><div class="cart__total-items">%s</div>Cart</div><br><div class="cart__total-cost">%s</div>';
$_['text_menu_name'] = 'Catalog';
$_['text_sku']        		= 'Sku:';
$_['text_weight']        	= 'Weight:';
$_['text_manufacturer']     = 'Manufacturer:';
$_['text_model']        	= 'Product code:';
$_['text_read_more']        = 'Read more';
$_['text_read_less']        = 'Hide';
$_['text_all_reviews']      = 'Show all reviews';

// Button
$_['button_update'] = 'Save';
$_['button_write'] = 'Write';
$_['button_subscribe'] = 'Submit';
$_['button_table'] = 'Table';
$_['button_search'] = 'SEARCH';

// Entry
$_['entry_email'] = 'E-mail';

// TimeFlicr
$_['text_days'] = 'Days';
$_['text_hours'] = 'Hours';
$_['text_minutes'] = 'Minutes';
$_['text_seconds'] = 'Seconds';

//Menu
$_['main_menu_category_quantity'] = 'Total categories: ';
