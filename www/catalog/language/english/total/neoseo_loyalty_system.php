<?php
$_['text_customer_discount'] = 'Personal discount';
$_['text_group_discount'] = 'Group discount';
$_['text_total_discount'] = 'Discount on order amount';
$_['text_total_markup'] = 'Extra charge for order amount';
$_['text_cumulative_discount'] = 'Cumulative discount';