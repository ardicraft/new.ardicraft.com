<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_page']          = 'page';
$_['text_menu']          = 'Menu';
$_['text_all_categories']= 'All categories';
$_['text_hide']          = 'Hide';
$_['text_callback_2']    = 'Back call';

// Text mobile
$_['text_currency_mobile']      = 'Currency';
$_['text_wishlist_mobile']      = 'Wishlist';
$_['text_compare_mobile']       = 'Compare';
$_['text_cart_mobile']          = 'Cart';
