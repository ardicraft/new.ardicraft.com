<?php
$_['heading_title']     = 'Blog search';

// Text
$_['text_search']       = 'Articles meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_viewed']       = 'Viewed %s';
$_['text_more']         = 'More';
$_['text_author']       = 'Author: ';

// Entry
$_['entry_search']      = 'Search Criteria';