<?php
// Text
$_['text_title']       = 'Payment +';
$_['text_description'] = 'Payment +';
$_['text_instruction']			= 'Payment instructions';
$_['text_payment']				= 'Your order will not ship until we receive payment.';