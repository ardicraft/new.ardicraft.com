<?php

// Heading
$_['heading_title'] = 'customer Reviews';

$_['text_average'] = 'Rating:';
$_['text_stars'] = '%s of 5 !';
$_['text_no_rating'] = 'No rating';

// Text
$_['text_error'] = 'No comment!';
$_['text_empty'] = 'No comment!';
$_['text_pagination'] = 'Reviews {start} - {end} of {total} (total pages: {pages})';
$_['text_showall'] = 'Show all reviews';
$_['text_write'] = 'Write review';
$_['text_author'] = 'Author: %s ';

// Text
$_['text_message'] = '<p>Your comment was successfully sent!</p>';
$_['text_note'] = '<span style="color: #FF0000;">Important:</span> HTML is not translated!';
$_['text_average'] = 'Rating:';
$_['text_stars'] = '%s of 5!';
$_['text_conditions'] = 'Here You can leave your opinion about the store or ask a question.';
$_['text_add'] = 'There are new comments.';
$_['admin_answer'] = 'store Administrator';

// Entry Fields
$_['entry_name'] = 'Name:';
$_['entry_yt'] = 'Link to videosif (YouTube):';
$_['entry_user_image'] = 'Upload a picture: ';
$_['entry_yt_desc'] = 'https://www.youtube.com/watch?v=jFWUHiJ0qtw';
$_['entry_email'] = 'E-Mail:';
$_['entry_title'] = 'Subject:';
$_['entry_enquiry'] = 'Message:';
$_['entry_captcha'] = 'Enter code:';
$_['entry_rating'] = 'Rating:';
$_['entry_good'] = 'Excellent';
$_['entry_bad'] = 'Bad';




// Email
$_['email_subject'] = 'Request %s';

// Errors
$_['error_name'] = 'Name must be between 3 and 32 characters!';
$_['error_title'] = 'Title must be between 3 and 64 characters!';
$_['error_email'] = 'Wrong email!';
$_['error_youtube'] = 'Not the right format for YouTube links!';
$_['error_enquiry'] = 'Message must be between 25 and 1000 characters!';
$_['error_captcha'] = 'Verification code does not match!';
?>