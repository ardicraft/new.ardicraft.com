<?php

$_['heading_title'] = 'Invoice';

$_['button_neoseo_cash_memo'] = 'Invoice';
$_['text_total_str'] = 'Suma in cuirsive';
$_['text_invoice'] = 'Invoice';
$_['text_product_order'] = 'Order';
$_['text_sku'] = 'Vendor code';
$_['text_model'] = 'Model';
$_['text_final'] = 'Releaser';

$_['text_fecha'] = 'Date';
$_['text_cliente'] = 'Client';
$_['text_cif'] = 'Tax Number';
$_['text_address'] = 'Address';
$_['text_phone'] = 'Phone';
$_['text_payment'] = 'Payment';
$_['text_shipment'] = 'Shipment';

$_['text_web'] = 'Web';

$_['column_id'] = '#';
$_['column_product'] = 'Product';
$_['column_sku'] = 'Vendor code';
$_['column_model'] = 'Model';
$_['column_count'] = 'Amount';
$_['column_weight_class'] = 'Unit';
$_['column_price'] = 'Price';
$_['column_sum'] = 'Cost';
$_['column_comment'] = 'Order note';

$_['nul'] = 'zero';
$_['ten'] = array(
	array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'),
	array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'),
);
$_['a20'] = array('ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen');
$_['tens'] = array(2 => 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety');
$_['hundred'] = array('', 'one hundred,', 'two hundred', 'three hundred', 'four hundred', 'five hundred', 'six hundred', 'seven hundred', 'eight hundred', 'nine hundred');
$_['unit'] = array(// Units
	array('cent', 'cent', 'cent', 1),
	array('euro', 'euro', 'euro', 0),
	array('thousand', 'thousand', 'thousand', 1),
	array('million', 'million', 'millions', 0),
	array('billion', 'billion', 'billion', 0),
);
