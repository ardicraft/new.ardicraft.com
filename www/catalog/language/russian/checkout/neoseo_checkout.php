<?php

// Heading 
$_['heading_title'] = 'Оформление заказа';
$_['heading_title_login'] = 'Авторизация';

// Text
$_['text_checkout_option'] = 'Авторизация';
$_['text_checkout_account'] = 'Покупатель';
$_['text_checkout_payment_address'] = 'Детали оплаты';
$_['text_checkout_shipping_method'] = 'Доставка и оплата';
$_['text_checkout_payment_method'] = 'Оплата';
$_['text_checkout_confirm'] = 'Подтвердить заказ';
$_['text_modify'] = 'Modify &raquo;';
$_['text_new_customer'] = 'Новый покупатель';
$_['text_returning_customer'] = 'Постоянный покупатель';
$_['text_checkout'] = 'Опции оформления заказа:';
$_['text_i_am_returning_customer'] = 'Постоянный покупатель';
$_['text_register'] = 'Зарегистрироваться';
$_['text_guest'] = 'Оформить без регистрации';
$_['text_register_account'] = 'Создав учетную запись, вы сможете делать покупки быстрее, быть в курсе состояния заказов и отслеживать заказы, которые вы ранее делали.';
$_['text_forgotten'] = 'Забыли пароль?';
$_['text_your_details'] = 'Ваши личные данные';
$_['text_your_address'] = 'Ваш адрес';
$_['text_your_password'] = 'Ваш пароль';
$_['text_agree'] = 'Я прочитал и согласен с <a class="colorbox" href="%s">%s</a>';
$_['text_shipping_method'] = 'Способ доставки:';
$_['text_payment_method'] = 'Способ оплаты:';
$_['text_comments'] = 'Примечание к заказу';
$_['text_create_account'] = 'Зарегистрироваться';
$_['text_please_wait'] = 'Ожидайте...';
$_['text_coupon'] = 'Успех: Скидочный купон был применен!';
$_['text_voucher'] = 'Успех: Подарочный сертифкат был применен!';
$_['text_reward'] = 'Успех: Бонусные балы были применены!';
$_['text_use_coupon'] = 'Использовать';
$_['text_use_voucher'] = 'Использовать';
$_['text_use_reward'] = 'Использовать';
$_['text_cart'] = 'Товары в корзине';
$_['text_image'] = 'Изображение';
$_['text_name'] = 'Продукт';
$_['text_quantity'] = 'Кол-во';
$_['text_price'] = 'Цена';
$_['text_total'] = 'Итого';
$_['text_action'] = 'Действия';
$_['text_cart_items'] = '%s';
$_['text_shipping'] = 'Доставка';
$_['text_country'] = 'Страна:';
$_['text_region'] = 'Регион:';
$_['text_city'] = 'Город:';
$_['text_payment'] = 'Оплата:';
$_['text_login'] = 'Я постоянный покупатель';
$_['text_сontact_details'] = 'Контактные данные';
$_['text_delivery_payment_details'] = 'Способ доставки и оплаты';
$_['text_edit'] = 'Редактировать';
$_['text_not_have_account'] = 'Я новый покупатель';
$_['text_next'] = 'Далее';
$_['text_unit'] = 'шт x';
$_['text_files_upload'] = 'Выбрать файлы';

// Column
$_['column_name'] = 'Продукт';
$_['column_model'] = 'Модель';
$_['column_quantity'] = 'Кол-во';
$_['column_price'] = 'Цена';
$_['column_total'] = 'Итого';

// Entry
$_['entry_account'] = 'Учётная запись:';
$_['entry_coupon'] = 'Использовать купон';
$_['entry_voucher'] = 'Использовать сертификат';
$_['entry_reward'] = 'Использовать бонусные балы';
$_['entry_email'] = 'Электронный ящик ( email ):';
$_['entry_password'] = 'Пароль:';

// Error
$_['error_required'] = 'Поле обязательно для ввода';
$_['error_warning'] = 'There was a problem while trying to process your order! If the problem persists please try selecting a different payment method or you can contact the store owner by <a href="%s">clicking here</a>.';
$_['error_login'] = 'Warning: No match for E-Mail Address and/or Password.';
$_['error_approved'] = 'Внимание: Ваша учетная запись должна быть утверждена прежде чем вы сможете авторизоваться.';
$_['error_exists'] = 'Внимание: Этот электронный адрес уже зарегистрирован!';
$_['error_firstname'] = 'Ваше имя должно быть от 1 до 32 символов!';
$_['error_lastname'] = 'Ваша фамилия должна быть от 1 до 32 символов!';
$_['error_email'] = 'Электронный адрес содержит ошибку!';
$_['error_telephone'] = 'Телефон должен быть от 3 до 32 символов!';
$_['error_fax'] = 'Fax must be between 3 and 32 characters!';
$_['error_company'] = 'Company must be between 3 and 32 characters!';
$_['error_password'] = 'Пароль должен быть от 3 до 20 символов!';
$_['error_confirm'] = 'Подтверждение пароля не совпадает с паролем!';
$_['error_company_id'] = 'Укажите ОКПО Компании!';
$_['error_tax_id'] = 'Укажите ИНН!';
$_['error_vat'] = 'VAT number is invalid!';
$_['error_address_1'] = 'Адрес 1 должен быть от 3 до 128 символов!';
$_['error_address_2'] = 'Адрес 2 должен быть от 3 до 128 символов!';
$_['error_postcode'] = 'Почтовый индекс должен быть от 2 до 10 символов!';
$_['error_country'] = 'Укажите страну!';
$_['error_zone'] = 'Укажите регион / область!';
$_['error_city'] = 'Укажите город!';
$_['error_agree'] = 'Ошибка: Вы должны принять "%s"!';
$_['error_address'] = 'Ошибка: Вы должны указать адрес!';
$_['error_shipping'] = 'Ошибка: Не указан метод доставки!';
$_['error_no_shipping'] = 'Ошибка: Нет подходящего метода доставки. Свяжитесь с <a href="%s">нами</a> для решения проблемы!';
$_['error_payment'] = 'Ошибка: Не указан метод оплаты!';
$_['error_no_payment'] = 'Ошибка: Нет подходящего метода оплаты. Свяжитесь с <a href="%s">нами</a> для решения проблемы!';
$_['error_coupon'] = 'Ошибка: Купон некорректен, просрочен или достигнут лимит его использования!';
$_['error_voucher'] = 'Ошибка: Подарочный сертификат некорректен или его баланс уже был использован!';
$_['error_maximum'] = 'Ошибка: Вы можете использовать не более %s бонусных баллов!';
$_['error_reward'] = 'Ошибка: Введите число бонусных балов для использования!';
$_['error_points'] = 'Ошибка: У вас нет %s бонусных баллов!';
$_['error_shipping'] = 'Укажите доставку!';
$_['error_min_amount'] = 'Минимальная сумма заказа: %s';
$_['error_validation'] = 'Ошибка: Внимательно проверьте значения полей';
$_['error_upload_file'] = 'Вы можете прикрепить только изображение, текстовый файл, word-документ либо excel-документ!';
$_['text_contact_info'] = 'Контактные данные';

// buttons
$_['button_checkout'] = 'Подтвердить заказ';

$_['entry_edit_order'] = 'Редактировать заказ';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_watched'] = 'Просмотренные товары';
$_['text_wishlist'] = 'Избранные товары';
$_['text_orders'] = 'История заказов';
$_['text_favorite'] = 'Любимые товары';
$_['text_product'] = 'Товар';
$_['text_price'] = 'Стоимость';
$_['text_pick_yourself'] = 'Забрать самостоятельно';
$_['text_order_delivery'] = 'Заказать доставку';
