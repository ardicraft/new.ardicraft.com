<?php
$_['text_customer_discount'] = 'Персональная скидка';
$_['text_group_discount'] = 'Групповая скидка';
$_['text_total_discount'] = 'Скидка на сумму заказа';
$_['text_total_markup'] = 'Наценка на сумму заказа';
$_['text_cumulative_discount'] = 'Накопительная скидка';