<?php
$_['text_select_region'] = "Выберите удобные Вам настройки";
$_['text_btn_cancel'] = 'Отмена';
$_['text_not_detected'] = 'Не определен';
$_['text_region_is'] = 'Для предоставление лучшего <br> сервиса, пожалуйста, уточните: ';
$_['text_region_notice'] = 'Если мы определили Ваш регион неверно, впишите его самостоятельно';
$_['text_select_correct'] = 'Если это не так, выберите правильный';
$_['text_detect'] = 'Изменить';
$_['text_ok'] = 'Подтвердить';
$_['text_language_selector'] = 'Выберите язык магазина';
$_['text_currency_selector'] = 'Выберите валюту магазина';
$_['text_region'] = '1. Ваш регион: ';
$_['text_currency_notice'] = '2. Удобная валюта: ';
$_['text_language_notice'] = '3. Версия сайта: ';
$_['text_selected_region'] = '1. Выбранный регион:';
