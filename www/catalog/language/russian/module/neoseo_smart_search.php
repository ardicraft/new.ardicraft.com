<?php
// Label
$_['label_name'] = 'по имени';
$_['label_model'] = 'по модули';
$_['label_sku'] = 'по артикулу';

// Text
$_['text_filter'] = 'фильтрация по:';
$_['text_search_page'] = 'страница поиска:';
$_['text_look_results'] = 'Посмотреть все результаты';