<?php

// Heading
$_['heading_title'] = 'Заказать обратный звонок';

// Text
$_['text_prompt'] = 'Укажите ваши данные';
$_['text_name'] = 'Ваше имя';
$_['text_phone'] = 'Контактный телефон';
$_['text_email'] = 'Email';
$_['text_message'] = 'Что вас интересует';
$_['text_title_finish'] = 'Спасибо!';
$_['text_time1'] = 'Удобное время для звонка';
$_['text_time2'] = 'Перезваниваем по будням с 08:30 до 18:00. ';
$_['text_time_from'] = 'с';
$_['text_time_to'] = 'по';

// Button
$_['button_continue'] = 'Продолжить покупки';
$_['button_action'] = 'Жду звонка';
