<?php

// Heading
$_['text_popup_cart_title'] = 'Товар добавлен в корзину!';
$_['text_subtotal'] = "Итого";

// Column
$_['column_id'] = '#';
$_['column_name'] = 'Продукт';
$_['column_quantity'] = 'Кол-во';
$_['column_price'] = 'Цена';
$_['column_action'] = '';

// Button
$_['button_continue'] = 'Продолжить покупки';
$_['button_checkout'] = 'Оформить заказ';
$_['button_plus'] = 'Увеличить количество товара';
$_['button_minus'] = 'Уменьшить количество товара';
$_['button_delete'] = 'Убрать товар из корзины';

$_['message_cart_single'] = 'Товар успешно добавлен в корзину! Вы можете или продолжить покупки, или перейти к оформлению заказа';
?>