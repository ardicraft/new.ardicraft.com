<?php

$_['button_bundle_buy'] = 'Купить комплект';
$_['text_economy'] = 'Вы экономите:';
$_['text_your_product'] = 'Ваш товар';
$_['text_select'] = 'Выберите';
$_['text_required_options_head'] = 'Для завершения покупки комплекта выберите опции';
$_['text_no_options'] = 'У данного товара нет опций';