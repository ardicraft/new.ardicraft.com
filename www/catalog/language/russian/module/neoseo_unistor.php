<?php
// Text
$_['text_characteristics_title']              = 'Характеристики';
$_['text_product_action']                     = 'Акция!Супер цены на акционный товар от нашего магазина!';
$_['text_follow_price']                       = 'Следить за ценой';
$_['text_all_characteristics']                = 'Смотреть все характеристики';
$_['text_see_all']                            = 'Смотреть все';
$_['text_collapse_all']                       = 'Свернуть';
$_['text_more']                               = 'Читать полностью';
$_['text_comment_answer']                     = 'Ответить';
$_['text_reviews_quantity']                   = 'Отзывы';
$_['text_give_feedback']                      = 'Оставить отзыв';
$_['text_see_all_reviews']                    = 'Смотреть все отзывы';
$_['text_shipping']                           = 'Доставка';
$_['text_payment']                            = 'Оплата';
$_['text_guarantee']                          = 'Гарантия';

$_['text_social']           = 'Авторизоваться с помощью социальной сети:';
$_['text_quickview']        = 'Быстрый просмотр';
$_['prev_text']             = 'Предыдущий';
$_['next_text']             = 'Следующий';
$_['text_read_more']        = 'Читать полностью';
$_['text_read_less']        = 'Скрыть';
$_['text_instock']          = 'В наличии';
$_['text_no_phone']         = 'Укажите номер телефона';
$_['text_dimension']        = 'Размеры (Д x Ш x В):';
$_['text_watched']          = 'Просмотренные товары';
$_['text_compare']          = 'сравнить';
$_['text_wishlist']         = 'в избранные';
$_['text_compare_menu']     = 'Сравнение товаров';
$_['text_wishlist_menu']    = 'Избранные товары';
$_['text_callback_menu']    = 'Перезвоните мне';
$_['text_all_categories']   = 'Все категории';
$_['text_callback']         = 'Заказать звонок';
$_['text_price']            = 'Цена: ';
$_['text_subscribe_title']  = 'Подпишись сейчас';
$_['text_subscribe_par']    = 'и получайте новости об акциях и специальных предложениях';
$_['text_payments_title']   = 'мы принимаем';
$_['text_tax']              = 'Без НДС:';
$_['text_work_hours']       = 'Мы работаем:';
$_['text_information']      = 'Полезная информация';
$_['text_callback']         = 'Заказать звонок';
$_['text_social']           = 'Войдите с помощью соц. сетей';
$_['text_need_account']     = 'У вас еще нет аккаунта?';
$_['text_or']               = '<span>или</span>';
$_['text_enter']            = 'Вход в магазин';
$_['text_forgotten']        = 'Я забыл(а)';
$_['text_password']         = 'Пароль';
$_['text_email']            = 'Электронная почта';
$_['text_track_order']      = 'Отследить заказ';
$_['text_category']         = 'Каталог';
$_['text_items']            = '<div class="cart__total-products"><div class="cart__total-items">%s</div>товар (ов)</div><br><div class="cart__total-cost">%s</div>';
$_['text_menu_name']        = 'Каталог товаров';
$_['text_sku']        		= 'Артикул:';
$_['text_weight']        	= 'Вес:';
$_['text_manufacturer']     = 'Производитель:';
$_['text_model']        	= 'Код товара:';
$_['text_read_more']        = 'Читать полностью';
$_['text_read_less']        = 'Скрыть';
$_['text_all_reviews']      = 'Последние отзывы';
$_['text_name_download']    = 'Название';
$_['text_size_download']    = 'Размер';
$_['text_date_added_download']    = 'Дата добавления';
$_['text_number_download']    = 'Номер';

// Button
$_['button_update']         = 'Сохранить';
$_['button_write']          = 'Написать';
$_['button_subscribe']      = '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>';
$_['button_table']          = 'Таблица';
$_['button_search']         = 'Поиск';

// Entry
$_['entry_email']           = 'E-mail';

// TimeFlicr
$_['text_days'] = 'Дней';
$_['text_hours'] = 'Часов';
$_['text_minutes'] = 'Минут';
$_['text_seconds'] = 'Секунд';

//Menu
$_['main_menu_category_quantity'] = 'Всего категорий: ';
