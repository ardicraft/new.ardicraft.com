<?php

$_['heading_title'] = 'Factura';

$_['button_neoseo_cash_memo'] = 'Factura';
$_['text_total_str'] = 'Cantidad en palabras';
$_['text_invoice'] = 'Factura';
$_['text_product_order'] = 'Ordenando';
$_['text_sku'] = 'Referencia';
$_['text_model'] = 'Modelo';
$_['text_final'] = 'Suelta';

$_['text_fecha'] = 'Fecha';
$_['text_cliente'] = 'Cliente';
$_['text_cif'] = 'Cif/Nif';
$_['text_address'] = 'Direccion';
$_['text_phone'] = 'Telefono';
$_['text_payment'] = 'Pago';
$_['text_shipment'] = 'Entrega';

$_['text_web'] = 'Web';

$_['column_id'] = '#';
$_['column_product'] = 'Nombre del Producto';
$_['column_sku'] = 'Referencia';
$_['column_model'] = 'Modelo';
$_['column_count'] = 'Cantidad';
$_['column_weight_class'] = 'It.';
$_['column_price'] = 'Precio';
$_['column_sum'] = 'Total';
$_['column_comment'] = 'Nota de orden';

$_['nul'] = 'cero';
$_['ten'] = array(
	array('', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve'),
	array('', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve'),
);
$_['a20'] = array('diez', 'once', 'doce', 'trece', 'catorce', 'quince', 'dieciseis', 'diecisiete', 'dieciocho', 'diecinueve');
$_['tens'] = array(2 => 'veinte', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa');
$_['hundred'] = array('', 'ciento', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos');
$_['unit'] = array(// Units
	array('EUR', 'EUR', 'EUR', 1),
	array('EUR', 'EUR', 'EUR', 0),
	array('EUR', 'EUR', 'EUR', 1),
	array('EUR', 'EUR', 'EUR', 0),
	array('EUR', 'EUR', 'EUR', 0),
);
