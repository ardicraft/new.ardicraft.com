<?php

// Heading
$_['heading_title']   = 'Solicitar una devolución de llamada';

// Text
$_['text_prompt'] = 'Introduce tus detalles';
$_['text_name'] = 'Tu nombre';
$_['text_phone'] = 'Número de contacto';
$_['text_email'] = 'Email';
$_['text_message'] = 'En que estas interesado';
$_['text_title_finish'] = 'Gracias!';
$_['text_time1'] = '';
$_['text_time2'] = '';
$_['text_time_from'] = '';
$_['text_time_to'] = '';

// Button
$_['button_continue'] = 'Seguir comprando';
$_['button_action'] = 'Esperando una llamada';
