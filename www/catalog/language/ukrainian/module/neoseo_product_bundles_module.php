<?php

$_['button_bundle_buy'] = 'Купити комплект';
$_['text_economy'] = 'Ви заощаджуєте:';
$_['text_your_product'] = 'Ваш товар';
$_['text_select'] = 'Оберіть';
$_['text_required_options_head'] = 'Для завершення покупки комплекту виберіть опції';
$_['text_no_options'] = 'У цього товару немає опцій';