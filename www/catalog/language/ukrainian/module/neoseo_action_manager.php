<?php

// Heading
$_['actions_heading'] = 'Акції';
$_['text_action_more'] = 'Дiзнатися бiльше';

$_['action_status_list'][0] = 'Подарунок';
$_['action_status_list'][1] = 'Знижка';
$_['action_status_list'][2] = '';

$_['action_status_data'][0] = 'Подарунок';
$_['action_status_data'][1] = 'Знижка';
$_['action_status_data'][2] = '';

$_['read_more'] = 'Детальніше';
$_['till_finish'] = 'До закінчення акції';
$_['action_finish'] = 'Акція закінчилася';


$_['days_left'] = 'Днів';
$_['hours_left'] = 'Годин';
$_['minutes_left'] = 'Хв.';
$_['seconds_left'] = 'Сек.';
$_['text_action_title'] = 'Товари, які беруть участь в акції';
