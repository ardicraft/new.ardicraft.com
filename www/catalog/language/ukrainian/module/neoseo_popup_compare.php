<?php

// Heading
$_['heading_title'] = 'Продукт доданий до порiвняння!';

// Column
$_['column_id'] = '#';
$_['column_name'] = 'Продукт';
$_['column_price'] = 'Цiна';
$_['column_action'] = '';

// Button
$_['button_continue'] = 'Продовжити покупки';
$_['button_action'] = 'Перейти до порiвняння';
$_['button_delete'] = 'Видалити iз списку';
