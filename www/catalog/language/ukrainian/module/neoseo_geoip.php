<?php
$_['text_select_region'] = "Оберіть зручні Вам налаштування";
$_['text_btn_cancel'] = 'Скасувати';
$_['text_not_detected'] = 'Не визначений';
$_['text_region_is'] = 'Ваш регіон: ';
$_['text_region_notice'] = 'Якщо ми визначили Ваш регіон невірно, впишіть його самостійно';
$_['text_select_correct'] = 'Якщо це не так, оберіть вірний';
$_['text_detect'] = 'Змінити';
$_['text_ok'] = 'Підтвердити';
$_['text_language_selector'] = 'Оберіть язик магазину';
$_['text_currency_selector'] = 'Оберіть валюту у магазині';
$_['text_region'] = '1. Ваш регіон: ';
$_['text_currency_notice'] = '2. Зручна валюта: ';
$_['text_language_notice'] = '3. Версія сайту: ';
$_['text_selected_region'] = '1. Обраний регіон:';