<?php
$_['text_customer_discount'] = 'Персональна знижка';
$_['text_group_discount'] = 'Групова знижка';
$_['text_total_discount'] = 'Знижка на суму замовлення';
$_['text_total_markup'] = 'Націнка на суму замовлення';
$_['text_cumulative_discount'] = 'Накопичувальна знижка';