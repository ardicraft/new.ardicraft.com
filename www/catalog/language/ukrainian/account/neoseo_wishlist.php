<?php

// Heading
$_['heading_title'] = 'Обрані товари';

// Text
$_['text_account'] = 'Особистий Кабінет';
$_['text_instock'] = 'В наявності';
$_['text_login'] = 'Ви повинні <a href="%s">виконати вхід</a> чи <a href="%s">створити аккаунт</a> щоб зберегти <a href="%s">%s</a> у свій <a href="%s">список обраних товарів</a>!';
$_['text_success'] = 'Ви додали <a href="%s">%s</a> у <a href="%s">обрані товари</a>!';
$_['text_exists'] = '<a href="%s">%s</a> вже знаходиться у Вашому <a href="%s">списку обраних товарів</a>!';
$_['text_remove'] = 'Список обраних товарів успішно змінений!';
$_['text_empty'] = 'У вас нема обраних товарів';

$_['text_quantity'] = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model'] = 'Модель:';
$_['text_points'] = 'Бонусні бали:';
$_['text_price'] = 'Ціна:';
$_['text_tax'] = 'Без ПДВ:';
$_['text_sort'] = 'Сортування:';
$_['text_default'] = 'За умовчанням';
$_['text_name_asc'] = 'Назва (А-Я)';
$_['text_name_desc'] = 'Назва (Я-А)';
$_['text_price_asc'] = 'Ціна (низька &gt; висока)';
$_['text_price_desc'] = 'Ціна (висока &gt; низька) ';
$_['text_rating_asc'] = 'Рейтинг (починаючи з низького)';
$_['text_rating_desc'] = 'Рейтинг (починаючи з високого)';
$_['text_model_asc'] = 'Модель (А - Я)';
$_['text_model_desc'] = 'Модель (Я - А)';
$_['text_limit'] = 'Показати:';
$_['text_remove_it'] = 'Видалити';

// Button
$_['button_table'] = 'Таблиця';
