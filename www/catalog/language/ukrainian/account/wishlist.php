<?php
// Heading
$_['heading_title'] = 'Обрані товари';

// Text
$_['text_account']  = 'Особистий Кабінет';
$_['text_instock']  = 'В наявності';
$_['text_wishlist'] = 'Обрані (%s)';
$_['text_login']    = 'Ви повинні <a href="%s">виконати вхід</a> чи <a href="%s">створити аккаунт</a> щоб зберегти <a href="%s">%s</a> у свій <a href="%s">список закладок</a>!';
$_['text_success']  = 'Ви додали <a href="%s">%s</a> у <a href="%s">обрані товари</a>!';
$_['text_exists']   = '<a href="%s">%s</a> вже знаходиться у Вашому <a href="%s">списку закладок</a>!';
$_['text_remove']   = 'Список закладок успішно змінений!';
$_['text_empty']    = 'У вас нема обраних товарів';


// Column
$_['column_image']  = 'Зображення';
$_['column_name']   = 'Назва товару';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'Наявність';
$_['column_price']  = 'Ціна за одиницю товару';
$_['column_action'] = 'Дія';

