<?php

// Heading
$_['blog_heading'] = 'Блог';
$_['heading_search'] = 'Пошук по блогу';

// Buttons
$_['button_continue'] = 'Читати';
$_['button_submit'] = 'Відправити';
$_['text_more'] = 'Детальніше';
$_['text_author'] = 'Автор: ';
$_['show_more_before'] = 'смотреть еще';
$_['show_more_after'] = 'свернуть';

// Text
$_['text_comments'] = ' ';
$_['text_comment'] = ' Коментар';
$_['text_tax'] = 'Податок:';
$_['text_sort'] = 'Сортування:';
$_['text_default'] = 'За замовчуванням';
$_['text_name_asc'] = 'Назва (А - Я)';
$_['text_name_desc'] = 'Назва (Я - А)';
$_['text_date_modified_desc'] = 'Останні';
$_['text_limit'] = 'Показати:';
$_['text_sub_categories'] = 'Підкатегорії:';
$_['text_search'] = 'Пошук';
$_['text_viewed'] = ' %s';
$_['text_read_more'] = 'Читати далi';
$_['text_read_less'] = 'Сховати';

// Error
$_['text_category_error'] = 'Категорія не знайдена!';
$_['text_author_error'] = 'Автор не знайдений!';
$_['text_article_error'] = 'Стаття не знайдена!';
$_['text_empty'] = 'Статті не знайдені!';

//Blogs
$_['blog_author_meta_h1'] = '%s';
$_['blog_author_meta_title'] = '%s. Сторінка №%d';
$_['blog_author_meta_keywords'] = '%s';
$_['blog_author_meta_description'] = '%s. Сторінка №%d';
$_['blog_author_description'] = '';