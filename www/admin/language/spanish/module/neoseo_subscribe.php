<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Subscribe</p>';
$_['heading_title_raw'] = 'NeoSeo Subscribe';

$_['tab_general'] = 'General';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';


// Text
$_['text_success'] = 'Module settings updated successfully!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'Log file successfully cleared!';
$_['text_clear_log'] = 'Clear log';
$_['text_title'] = 'Sign up for our newsletter and get discounts!';
$_['text_module_version'] = '';

$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear log';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_debug_desc'] = 'The module logs will write various information for the module developer';
$_['entry_title'] = 'Title';
$_['entry_message'] = 'New subscriber';
$_['entry_message_error'] = 'Error message on subscribe';
$_['entry_notify_list'] = 'Recipients of the subscription message, separated by commas';
$_['entry_notify_subject'] = 'Subject of a letter about a new subscription';
$_['entry_notify_message'] = 'New subscription message';
$_['entry_show_name'] = 'Show name field in submit form';

$_['param_message'] = "Thank you for subscribing to our news.";
$_['param_message_error'] = "You are already subscribed to our news.";
$_['param_notify_subject'] = "New subscribe";
$_['param_notify_message'] = "A visitor emailed {email} to our newsletter";

// Error
$_['error_permission'] = '';





