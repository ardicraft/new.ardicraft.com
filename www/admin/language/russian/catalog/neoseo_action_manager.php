<?php

// Heading
$_['heading_title'] = 'Акции';
$_['text_neoseo_action_manager'] = 'NeoSeo Менеджер Акций';

// Tab
$_['tab_other_data'] = 'Дополнительные';

// Text
$_['text_success'] = 'Список акций обновлен!';
$_['text_list'] = 'Список акций';
$_['text_add'] = 'Добавление акции';
$_['text_edit'] = 'Редактирование акции';
$_['text_plus'] = '+';
$_['text_minus'] = '-';
$_['text_default'] = 'Основной магазин';

$_['action_status'][0] = 'Подарок';
$_['action_status'][1] = 'Скидка';

// Column
$_['column_name'] = 'Название акции';
$_['column_image'] = 'Изображение';
$_['column_action_status'] = 'Статус';
$_['column_action'] = 'Действие';
$_['column_short_text'] = 'Краткий текст';
$_['column_date_end'] = 'Дата окончания акции';

// Entry
$_['entry_name'] = 'Название:';
$_['entry_short_text'] = 'Короткое описание:';
$_['entry_full_text'] = 'Полное описание:';
$_['entry_description'] = 'Описание:';
$_['entry_meta_title'] = 'HTML-тег Title';
$_['entry_meta_keyword'] = 'Мета-тег Keywords:';
$_['entry_meta_description'] = 'Мета-тег Description:';
$_['entry_keyword'] = 'SEO URL:';
$_['entry_image'] = 'Изображение для акции:';
$_['entry_date_end'] = 'Дата окончания акции:';
$_['entry_related_products'] = 'Привязка продуктов к акции:';
$_['entry_related_category'] = 'Привязка акций к категориям:';
$_['entry_related_brand'] = 'Привязка акций к производителям:';
$_['entry_action_status'] = 'Тип акции:';
$_['entry_main_page'] = 'Вывести в одиночный блок (на главную)? :';
$_['entry_all_category'] = 'Вывести на всех категориях? :';
$_['entry_image_width'] = 'Ширина изображения';
$_['entry_image_height'] = 'Высота изображения';

// Help
$_['help_keyword'] = 'Замените пробелы на тире. Должно быть уникальным на всю систему.';
$_['help_related_products'] = '(Автодополнение)';
$_['help_related_category'] = '(Автодополнение)';
$_['help_related_brand'] = '(Автодополнение)';
$_['help_action_status'] = 'Влияет на отображение иконки и название';

// Error
$_['error_warning'] = 'Внимательно проверьте форму на ошибки!';
$_['error_keyword_ext'] = 'Этот SEO keyword уже используется!';
$_['error_date_end'] = 'Укажите дату окончания акции!';
$_['error_permission'] = 'У Вас нет прав для изменения акций!';
$_['error_name'] = 'Название акции должно быть от 3 до 255 символов!';
$_['error_meta_title'] = 'Мета-тег Title должен быть от 3 до 255 символов!';
$_['error_model'] = 'Модель товара должна быть от 3 до 64 символов!';
$_['error_keyword'] = 'SEO keyword already in use!';
$_['error_action_status'] = 'Ошибка статуса';
