<?php
// Heading
$_['heading_title']    = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Оплата</p>';
$_['heading_title_raw']    = 'NeoSeo Оплата';

// Tabs
$_['tab_general'] = 'Параметры';
$_['tab_methods'] = 'Методы';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_payment']       = 'Оплата';
$_['text_success']       = 'Настройки модуля успешно обновлены!';
$_['text_edit']          = 'Редактирование модуля';
$_['text_edit_payment']	 = 'Редактировать метод оплаты+';
$_['text_add']           = 'Добавить метод оплаты+';
$_['text_all_zones']     = 'Все зоны';
$_['text_list']          = 'Список методов оплаты+';
$_['text_default']       = 'Основной магазин';


// Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_remove'] = 'Удалить';
$_['button_insert'] = 'Добавить';

// Entry
$_['entry_name']        = 'Название:';
$_['entry_description'] = 'Описание:';
$_['entry_geo_zone']    = 'Географическая зона:';
$_['entry_status']      = 'Статус:';
$_['entry_sort_order']  = 'Порядок сортировки:';
$_['entry_price_min']   = 'Минимальная сумма заказа:';
$_['entry_price_max']   = 'Максимальная сумма заказа:';
$_['entry_cities']      = 'Список городов:';
$_['entry_order_status']= 'Статус заказа после оплаты:';
$_['entry_debug']		= 'Отладочный режим';
$_['entry_debug_desc']	= 'В логи модуля будет писаться различная информация для разработчика модуля';
$_['entry_stores']		= 'Магазины';

$_['column_name']       = 'Название метода оплаты';
$_['column_status']     = 'Статус';
$_['column_action']     = 'Действие';
$_['column_sort_order'] = 'Порядок сортировки';

// Help
$_['help_max']    = 'Для того чтобы не ограничивать по максимальной сумме оставьте пустым';
$_['help_min']    = 'Для того чтобы не ограничивать по минимальной сумме оставьте пустым';
$_['help_cities'] = 'Введите города, через разделитель «;». Для того чтобы оплата действовала во всех городах, оставьте пустым';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_name'] = 'Не задано или неправильной длины название метода оплаты (Должно быть от 2 до 64 символов)';






