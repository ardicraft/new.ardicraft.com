<?php
// Text
$_['text_footer'] 	= '
<a class="--footer-link" target="_blank" href="https://neoseo.ru/razrabotka-internet-magazina/">Разработка</a>, 
<a class="--footer-link" target="_blank" href="https://neoseo.ru/internet-marketing">маркетинг</a>, 
<a class="--footer-link" target="_blank" href="https://neoseo.ru/tehnicheskaya-podderzhka-internet-magazinov-opencart">техническая поддержка</a> 
<a class="--footer-link" target="_blank" href="https://neoseo.ru/sozdanie-seo-magazina">SEO-магазина</a> 
с ❤ <br><a target="_blank" href="https://neoseo.com.ua">Веб-студия NeoSeo</a>';
$_['text_version'] 	= 'Версия %s';