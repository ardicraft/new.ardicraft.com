<?php
// Heading
$_['heading_title']        		= 'SEO Store';

// Text
$_['text_order']          		= 'Заказы';
$_['text_processing_status']    = 'В процессе';
$_['text_complete_status']      = 'Завершено';
$_['text_customer']          	= 'Покупатели';
$_['text_online']          		= 'Онлайн';
$_['text_approval']          	= 'В ожидании';
$_['text_product']          	= 'Товары';
$_['text_stock']          		= 'Нет в наличии';
$_['text_review']          		= 'Отзывы';
$_['text_return']          		= 'Возвраты';
$_['text_affiliate']          	= 'Партнерская программа';
$_['text_store']          		= 'Магазины';
$_['text_front']          		= 'Магазин';
$_['text_help']          		= 'Помощь';
$_['text_homepage']          	= 'Главная';
$_['text_support']          	= 'Форум';
$_['text_documentation']        = 'Документация';
$_['text_logout']          		= 'Выход';
$_['text_stores']               = 'Перейти на сайт';

$_['button_delete_demo_data']       ='Удалить демо данные';
$_['text_demo_data_delete']         ='Будут удалены все демо-данные из магазина, такие как товары, категории, опции фильтра, посадочные страницы. Даное действие невозможно отменить или вернуть';
$_['text_yes'] = 'Да';
$_['text_no'] = 'Нет';
$_['text_mute'] = 'Больше не напоминать';
$_['text_confirm_delete'] = 'Вы уверены, Данное действие не обратимо!!!';
