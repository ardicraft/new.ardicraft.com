<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Микроразметка Schema.org (JSON-LD)</span>';
$_['heading_title_raw'] = 'NeoSeo Микроразметка Schema.org (JSON-LD)';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';
$_['tab_product'] = 'Товары';
$_['tab_usefull'] = 'Полезные ссылки';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_image_manager'] = 'Менеджер изображений';
$_['text_browse'] = 'Обзор';
$_['text_error_1'] = 'Шаблоны не обнаружены!';
$_['text_error_2'] = 'Невозможно прочитать шаблон!';
$_['text_error_3'] = 'Невозможно сохранить файл модификации!';
$_['text_success_generate'] = 'Файл модификации успешно создан!';
$_['text_info'] = 'Для очистки микроразметки обновите модификатор в <a href="%s" class="alert-link">разделе</a> управления управления дополнениями!';
$_['text_breadcrum_emoji_first'] = '🐠';
$_['text_breadcrum_emoji_second'] = '🐟';
//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_debug'] = 'Отладочный режим:<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус:';
$_['entry_stock_status'] = 'Соотношение "Статуса склада" к Schema.org => ItemAvailability';
$_['entry_address_region'] = 'Страна:';
$_['entry_address_locality'] = 'Город:';
$_['entry_postal_code'] = 'Почтовый индекс:';
$_['entry_street_address'] = 'Улица, дом:';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastrojka-modulya-neoseo-mikrorazmetka-schema-org-json-ld-opencart">https://neoseo.com.ua/nastrojka-modulya-neoseo-mikrorazmetka-schema-org-json-ld-opencart</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/mikrorazmetka-opencart-v-2-1-2-3#module_history">https://neoseo.com.ua/mikrorazmetka-opencart-v-2-1-2-3#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/mikrorazmetka-opencart-v-2-1-2-3#faqBox">https://neoseo.com.ua/mikrorazmetka-opencart-v-2-1-2-3#faqBox</a>';