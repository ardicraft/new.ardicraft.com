<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Отслеживание несуществующих страниц</span>';
$_['heading_title_raw'] = 'NeoSeo Отслеживание несуществующих страниц';

//Tab
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

// Text
$_['text_module_version'] = '';
$_['text_description'] = 'Модуль предназначен для просмотра ошибок 404';
$_['text_clear_log'] = 'Очистить лог';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_success_options'] = 'Настройки модуля обновлены!';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_module'] = 'Модули';

//Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладка:';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';

$_['mail_support'] = '';
$_['module_licence'] = '';
