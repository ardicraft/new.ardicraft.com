<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Генератор robots.txt</p>';
$_['heading_title_raw'] = 'NeoSeo Генератор robots.txt';

//Tab
$_['tab_general'] = 'Общие';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Параметры';
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_default'] = 'По умолчанию';
$_['text_feed'] = 'Каналы продвижения';
$_['text_gzip_0'] = 'Отключено';
$_['text_gzip_1'] = 'Уровень 1';
$_['text_gzip_2'] = 'Уровень 2';
$_['text_gzip_3'] = 'Уровень 3';
$_['text_gzip_4'] = 'Уровень 4';
$_['text_gzip_5'] = 'Уровень 5';
$_['text_gzip_6'] = 'Уровень 6';
$_['text_gzip_7'] = 'Уровень 7';
$_['text_gzip_8'] = 'Уровень 8';
$_['text_gzip_9'] = 'Уровень 9';
$_['text_seo_0'] = 'Отключено';
$_['text_seo_1'] = 'SEO Pro';
$_['text_seo_2'] = 'SEO Url';
$_['text_blog_0'] = 'Отключено';
$_['text_blog_1'] = 'SEO CMS PRO';
$_['text_blog_2'] = 'PAV Blog';
$_['text_blog_3'] = 'Blog Manager';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_image_status'] = 'Выводить картинки:<br><br><small><i>Не рекомендуется, не все роботы понимают</i></small>';
$_['entry_status_seo'] = 'ЧПУ товаров:';
$_['entry_filterpro_status_seo'] = 'SEO FilterPro:';
$_['entry_category_brand_status'] = 'Категории с брендами:<br><br><small><i>Будут сгенерированы ссылки вида категория/бренд</i></small>';
$_['entry_status_addresses'] = 'Адресная информация:';
$_['entry_partition_status'] = 'Разбить карту на части:';
$_['entry_partition_volume'] = 'Размер части карты:';
$_['entry_status_multistore'] = 'Мультистор:';
$_['entry_status_gzip'] = 'Сжатие:';
$_['entry_status_blog'] = 'Ссылки для блога:';
$_['entry_url'] = 'Ссылка на sitemap.xml:';
$_['entry_use_url_date'] = 'Формировать дату изменения:';
$_['entry_use_url_frequency'] = 'Формировать частоту обновления';
$_['entry_use_url_priority'] = 'Формировать приоритет обновления';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Резервные копии"!';




