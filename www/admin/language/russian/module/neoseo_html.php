<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo HTML</span>';
$_['heading_title_raw'] = 'NeoSeo HTML';

$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_fields'] = 'Поля';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_template'] = 'Шаблон';
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_default_title'] = 'Главная';
$_['text_default_html'] = '';
$_['text_module'] = 'Модули';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать лог';

// Entry
$_['entry_name'] = 'Название модуля';
$_['entry_template'] = 'Выберите шаблон отображения';
$_['entry_height'] = 'Высота блока';
$_['entry_html_content'] = 'Содержимое HTML Блока';
$_['entry_title'] = 'Заголовок блока';
$_['entry_label'] = 'Название: ';


$_['entry_supplier_info'] = 'Поставщик:';
$_['entry_text'] = 'Дополнительный текст:';
$_['entry_debug'] = 'Отладочный режим:<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_supplier_info'] = 'Это поле обязательно для заполнения!';



// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';


