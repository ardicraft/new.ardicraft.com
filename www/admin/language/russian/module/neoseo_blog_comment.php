<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Коментарии';
$_['heading_title_raw'] = 'NeoSeo Блог Коментарии';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Модуль Блог Коментарии удачно изменен!';
$_['text_edit']        = 'Редактирование модуля Блог Коментарии';
$_['text_latest']      = 'Последние коментарии';
$_['text_popular']     = 'Популярные коментарии';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_category']   = 'Категория';
$_['entry_root_category'] = 'Ограничить категорией:';
$_['entry_type']       = 'Тип';
$_['entry_limit']      = 'Лимит';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';
$_['entry_template']   = 'Шаблон';

// Error
$_['error_permission'] = 'У вас нет прав изменять модуль Блог Статьи!';
$_['error_name']       = 'Название модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина обязательна!';
$_['error_height']     = 'Высота обязательна!';