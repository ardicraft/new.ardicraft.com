<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Быстрые настройки магазина</span>';
$_['heading_title_raw'] = 'Быстрые настройки магазина';
$_['heading_title_topmenu'] = 'Быстрые настройки магазина';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';
$_['tab_usefull'] = 'Полезные ссылки';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_image_manager'] = 'Менеджер изображений';
$_['text_browse'] = 'Обзор';
$_['text_step0'] = 'Укажите необходимые языки и валюту';
$_['text_step0_desc'] = 'Ненужные данные будут удалены, но их заново можно будет создать в настройках';
$_['text_step1'] = 'Шаг 1 из 4';
$_['text_step2'] = 'Шаг 2 из 4';
$_['text_step3'] = 'Шаг 3 из 4';
$_['text_step4'] = 'Шаг 4 из 4';
$_['text_next'] = 'Следующий шаг';
$_['text_prev'] = 'Предыдущий шаг';
$_['text_confirm_cleanup_lc'] = 'Все данные об отключенных языках и валютах будут удалены. Подтвердить?';
//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_debug'] = 'Отладочный режим:<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус:';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

$_['entry_logo'] = 'Логотип Вашего магазина';
$_['entry_logo_desc'] = 'Логотип магазина будет находиться в шапке сайта.Рекомендуемый размер 150px x 250px. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/logo.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_unistor_phone1'] = 'Телефон 1 в шапке';
$_['entry_neoseo_unistor_phone1_desc'] = 'Номер телефона, который будет отображаться первым в списке в шапке и в футере магазина. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/phones.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_unistor_phone2'] = 'Телефон 2 в шапке';
$_['entry_neoseo_unistor_phone2_desc'] = 'Номер телефона, который будет отображаться вторым в списке в шапке и в футере магазина (можно оставить пустым). <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/phones.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_unistor_phone3'] = 'Телефон 3 в шапке';
$_['entry_neoseo_unistor_phone3_desc'] = 'Номер телефона, который будет отображаться третьим в списке в шапке и футере магазина (можно оставить пустым). <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/phones.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_unistor_scheme_style'] = 'Цветовая схема';
$_['entry_neoseo_unistor_scheme_style_desc'] = 'Цветовая схема Вашего сайта доступные варианты: 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/yellow.jpg\' class=\'tooltip-image\'>">Желтый</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/red.jpg\' class=\'tooltip-image\'>">Красный</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/orange.jpg\' class=\'tooltip-image\'>">Оранжевый</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/green.jpg\' class=\'tooltip-image\'>">Зеленый</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/violet.jpg\' class=\'tooltip-image\'>">Фиолетовый</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/blue.jpg\' class=\'tooltip-image\'>">Синий</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/lblue.jpg\' class=\'tooltip-image\'>">Голубой</span>, 
<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/black.jpg\' class=\'tooltip-image\'>">Черный</span>
';

$_['entry_neoseo_unistor_work_time'] = 'Время работы магазина';
$_['entry_neoseo_unistor_work_time_desc'] = 'Укажите время работы магазина, информация будет отображаться на странице "О магазине" и в шапке сайта. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/work_time.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_config_name'] = 'Название магазина';
$_['entry_config_name_desc'] = 'Название Вашего магазина будет отображаться в заголовке страниц. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/title.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_config_meta_title'] = 'Тэг Мета тайтл магазина (Может совпадать с названием)';
$_['entry_config_meta_title_desc'] = 'Мета тег Title — это заголовок страницы в браузере, который отображается в выдаче поисковых систем. Мета тег Тайтл учитывается поисковой системой при ранжирование сайтов, считается основным показателем определения релевантности страницы к поисковым запросам.';

$_['entry_config_meta_description'] = 'Тэг Дескрипшн - описание магазина';
$_['entry_config_meta_description_desc'] = 'Содержимое meta name description content используется при формировании сниппета для описания сайта в поиске.';

$_['entry_config_meta_keyword'] = 'Ключевые слова к магазину';
$_['entry_config_meta_keyword_desc'] = 'Meta keywords — список ключевых слов (key words), соответствующих содержимому страницы сайта. Поисковые системы могут использовать ключевые слова тега meta name keywords content при индексации.';

$_['entry_config_email'] = 'E-mail администратора';
$_['entry_config_email_desc'] = 'Почта, на которую будут приходить уведомления о заказах';

$_['entry_config_owner'] = 'Владелец магазина';
$_['entry_config_owner_desc'] = 'Имя владельца магазина';

$_['entry_config_currency'] = 'Валюта магазина';
$_['entry_config_currency_desc'] = 'Валюта, которая будет использоваться по умолчанию в магазине';

$_['entry_config_language'] = 'Язык магазина';
$_['entry_config_language_desc'] = 'Язык магазина по умолчанию для покупателей';

$_['entry_config_admin_language'] = 'Язык админ. панели';
$_['entry_config_admin_language'] = 'Язык который будет использоваться в панели администратора по умолчанию';

$_['entry_neoseo_unistor_contact_map'] = 'Показывать карту';
$_['entry_neoseo_unistor_contact_map_desc'] = 'Карта будет показываться на странице "Контакты"';

$_['entry_neoseo_unistor_contact_google_api_key'] = 'Google api key';
$_['entry_neoseo_unistor_contact_google_api_key_desc'] = 'Создать ключ можно по ссылке <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key">https://developers.google.com/maps/documentation/javascript/get-api-key</a>';

$_['entry_big_slides'] = "Выберите большие слайды для слайдера";
$_['entry_big_slides_desc'] = "Можно будет потом изменить в настройках слайдшоу. <span data-toggle=\"tooltip\" class='h-tool-tip' data-html=\"true\" data-title=\"<img src='view/image/neoseo_quick_setup/slides.png' class='tooltip-image'>\">Пример</span> Размер банера 1200 * 400 для горизонтального меню, 800 * 400 для вертикального меню.";

$_['entry_small_slides'] = "Выберите дополнительные маленькие слайды для слайдера";
$_['entry_small_slides_desc'] = "Можно будет потом изсенить в настройках слайдшоу";

$_['entry_neoseo_unistor_menu_main_type'] = "Выберите тип главного меню";
$_['entry_neoseo_unistor_menu_main_type_desc'] = "Главное меню может быть <span data-toggle=\"tooltip\" class='h-tool-tip' data-html=\"true\" data-title=\"<img src='view/image/neoseo_quick_setup/horizontal-menu.png' class='tooltip-image'>\">горизонтальным</span> или <span data-toggle=\"tooltip\" class='h-tool-tip' data-html=\"true\" data-title=\"<img src='view/image/neoseo_quick_setup/vertical-menu.png' class='tooltip-image'>\">вертикальным</span>";

$_['entry_neoseo_unistor_contact_latitude'] = "Укажите широту";
$_['entry_neoseo_unistor_contact_latitude_desc'] = "Укажите GPS координату 'шарота' для отображения точки на карте, указывающей где находится Ваш магазин, если это необходимо. Без скобок и кавычек, например 54.718681";

$_['entry_neoseo_unistor_contact_longitude'] = "Укажите долготу";
$_['entry_neoseo_unistor_contact_longitude_desc'] = "Укажите GPS координату 'долгота' для отображения точки на карте, указывающей где находится Ваш магазин, если это необходимо. Без скобок и кавычек, например 20.499113";

$_['entry_config_icon'] = 'Favicon магазина';
$_['entry_config_icon_desc'] = 'Favicon будет отображаться в заголовке окна в браузере. Иконка должна быть в PNG 16px на 16px. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/favicon.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_config_address'] = 'Адрес магазина';
$_['entry_config_address_desc'] = 'Адрес магазина будет отображаться на странице "Контакты"';

$_['entry_config_open'] = 'Время работы магазина';
$_['entry_config_open_desc'] = 'Укажите время работы магазина, информация будет отображаться на странице "О магазине" и в шапке сайта. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/work_time.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_config_country_id'] = 'Укажите страну';
$_['entry_config_country_id_desc'] = 'Укажите страну, где находится Ваш магазин. При оформлении заказа будут выбраны регионы из этой страны по умолчанию. Также будет отодбражаться на странице "Контакты"';

$_['entry_config_zone_id'] = 'Укажите регион';
$_['entry_config_zone_id_desc'] = 'Укажите регион в котором расположен Ваш магазин. Будет отодбражаться на странице "Контакты"';

$_['entry_neoseo_unistor_general_style'] = 'Общий стиль';
$_['entry_neoseo_unistor_general_style_desc'] = '<span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/flat.png\' class=\'tooltip-image\'>">Объемный</span> или <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/material.png\' class=\'tooltip-image\'>">плоский</span>';

$_['entry_neoseo_unistor_use_wide_style'] = 'Использовать широкоформатные стили';
$_['entry_neoseo_unistor_use_wide_style_desc'] = 'Будут ли у сайт на фирокофрматных мониторах <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/nowide.png\' class=\'tooltip-image\'>">поля по бокам</span>, либо <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/wide.png\' class=\'tooltip-image\'>">растягиваться на весь экран</span>.';

$_['entry_neoseo_unistor_delivery'] = 'Информация о доставке';
$_['entry_neoseo_unistor_delivery_desc'] = 'Информационный блок в карточке товара. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/shipping-info.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_unistor_payment'] = 'Информация об оплате';
$_['entry_neoseo_unistor_payment_desc'] = 'Информационный блок в карточке товара. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/payment-info.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_unistor_guarantee'] = 'Информация о гарантиях';
$_['entry_neoseo_unistor_guarantee_desc'] = 'Информационный блок в карточке товара. <span data-toggle="tooltip" class=\'h-tool-tip\' data-html="true" data-placement="right" data-title="<img src=\'view/image/neoseo_quick_setup/warranty-info.png\' class=\'tooltip-image\'>">Пример</span>';

$_['entry_neoseo_jivosite_code'] = 'Код Jivosite, если используете';
$_['entry_neoseo_jivosite_code_desc'] = 'Например JH-54598';

$_['entry_neoseo_google_analytics_code'] = 'Код Google Аналитики, если используете';
$_['entry_neoseo_google_analytics_code_desc'] = 'Например GA-55489';

$_['entry_need_languages'] = 'Выберите языки';
$_['entry_need_languages_desc'] = 'Выберите языки из предустановленных, с которыми планируете работать. Не отмеченные языки будут удалены из системы. В дальнейшем  их можно будет добавить в админ. панели.';

$_['entry_need_currencies'] = 'Выберите валюты';
$_['entry_need_currencies_desc'] = 'Выберите валюты из предустановленных, которые планируете отображать в магазине. Не отмеченные валюты будут удалены из системы. В дальнейшем их можно будет добавить в админ. части.';
//$_['entry_'] = '';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['error_all_empty'] = 'Необходимо оставить хотя-бы по одному значению';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-module-template">https://neoseo.com.ua/nastroyka-modulya-neoseo-module-template</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/module-template#module_history">https://neoseo.com.ua/module-template#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/module-template#faqBox">https://neoseo.com.ua/module-template#faqBox</a>';