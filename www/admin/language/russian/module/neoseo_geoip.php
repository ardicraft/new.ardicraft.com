<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Автоопределение валюты, языка и местоположения покупателя по GeoIp</span>';
$_['heading_title_raw'] = 'NeoSeo Автоопределение валюты, языка и местоположения покупателя по GeoIp';
$_['heading_title_edit'] = 'Сопоставление зон';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';
$_['tab_mapping'] = 'Сопоставление зон';
$_['tab_customer_groups'] = 'Группы покупателей';
$_['tab_localization'] = 'Локализация';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_image_manager'] = 'Менеджер изображений';
$_['text_browse'] = 'Обзор';
$_['text_geoip_update'] = 'База GeoIp обновлена';
$_['text_geoip_update_help'] = 'Обновление базы может занять до 30 секунд. Не закрывайте окно после нажатия на кнопку и дождитесь завершения операции';
$_['text_no_api_key'] = 'Укажите API ключ и сохраните настройки модуля';
//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';
$_['button_save_mapping'] = 'Сохранить соответствия';
$_['button_update'] = 'Обновить базу GeoIp';

// Entry
$_['entry_debug'] = 'Отладочный режим:<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус:';
$_['entry_api_key'] = "API ключ";
$_['entry_country'] = 'Страна по Geoip';
$_['entry_country_oc'] = 'Страна в магазине';
$_['entry_zone'] = 'Регион по Geoip';
$_['entry_zone_oc'] = 'Регион в магазине';
$_['entry_customer_group'] = 'Группа покупателей';
$_['entry_map'] = 'Сопоставить';
$_['entry_language'] = 'Язык магазина';
$_['entry_currency'] = 'Валюта магазина';
$_['entry_allow_change_language'] = 'Позволить покупателю изменить язык';
$_['entry_allow_change_language_desc'] = 'Позволить покупателю изменить язык установленный в результате автоматического применения настроек локализации';
$_['entry_allow_change_currency'] = 'Позволить покупателю изменить валюту';
$_['entry_allow_change_currency_desc'] = 'Позволить покупателю изменить валюту установленную в результате автоматического применения настроек локализации';


// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['error_download'] = 'Ошибка загрузки обновлений. Попробуйте чуть позже';
$_['error_unpack'] = 'Ошибка распаковки файла обновлений. Попробуйте чуть позже';
$_['error_key'] = 'API ключ неверный. Укажите корректный API ключ и сохраните настройки модуля';
$_['error_move'] = 'Ошибка перемещения файла обновлений. Проверьте права на папку';

$_['mail_support'] = '';
$_['module_licence'] = '';
