<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Виджет заметок  на панели управления</span>';
$_['heading_title_raw'] = 'NeoSeo Виджет заметок';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_notes'] = 'Заметки';
$_['tab_header'] = 'Шапка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_title'] = 'Заметки';
$_['text_add'] = 'Добавить заметку';
$_['text_edit'] = 'Редактировать заметку';
$_['text_date_notification'] = 'Уведомить:';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';
$_['button_add_note'] = 'Добавить заметку';

// Entry
$_['entry_debug'] = 'Отладочный режим<br /><span class="help">В логи модуля будет писаться различная информация для разработчика модуля.</span>';
$_['entry_status'] = 'Статус';
$_['entry_title'] = 'Заголовок блока';
$_['entry_name'] = 'Название заметки';
$_['entry_text'] = 'Текст заметки';
$_['entry_use_notification'] = 'Уведомить';
$_['entry_date_notification'] = 'Дата уведомления';
$_['entry_text_notification'] = 'Текст уведомления';
$_['entry_text_notification_desc'] = 'В указанную дату на панели управления будет выведено оповещение';
$_['entry_show_dashboard'] = 'Выводить в виджет на панель управления';
$_['entry_sort_order'] = 'Порядок сортировки';
$_['entry_color'] = 'Цвет заметки';
$_['entry_font_color'] = 'Цвет шрифта';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_empty'] = 'Поле обязательно для заполнения!';



