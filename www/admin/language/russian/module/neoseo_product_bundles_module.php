<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Вместе дешевле Блок</span>';
$_['heading_title_raw'] = 'NeoSeo Вместе дешевле Блок';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать лог';

// Entry
$_['entry_name'] = 'Название модуля';
$_['entry_title'] = 'Заголовок модуля';
$_['entry_status'] = 'Статус';
$_['entry_template'] = 'Шаблон отображения';
$_['entry_debug'] = 'Отладочный режим';

//Text
$_['text_default_title'] = 'NeoSeo Комплект товаров';
$_['text_template'] = 'Шаблон';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';