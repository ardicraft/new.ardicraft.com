<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Рекомендуемые товары PRO';
$_['heading_title_raw'] = 'NeoSeo Рекомендуемые товары PRO';

//Tabs
$_['tab_general'] = 'Общие';
$_['tab_module_tabs'] = 'Табы';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';
$_['button_download_log'] = 'Скачать файл логов';

// Text
$_['text_module_version'] = '';
$_['text_module'] = 'Модули';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_edit'] = 'Редактирование модуля';
$_['text_default_title'] = 'Рекомендуемые товары';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_clear_log'] = 'Очистить лог';
$_['text_clear'] = 'Очистить';
$_['text_success_delete'] = 'Таб успешно удален!';
$_['text_template'] = 'Шаблон';

// Entry
$_['entry_name'] = 'Название:';
$_['entry_product'] = 'Товары:';
$_['entry_limit'] = 'Лимит:';
$_['entry_width'] = 'Ширина:';
$_['entry_height'] = 'Высота:';
$_['entry_status'] = 'Статус:';
$_['entry_url'] = 'Ссылка:';
$_['entry_url_text'] = 'Текст ссылки:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_title'] = 'Заголовок:';
$_['entry_title_tab'] = 'Название таба:';
$_['entry_products'] = 'Товары:';
$_['entry_order'] = 'Порядок:';
$_['entry_order_desc'] = 'Порядок вывода таба';
$_['entry_use_related'] = 'Заполнить по месту использования:';
$_['entry_template'] = 'Шаблон:';

// Help
$_['help_product'] = '(Автодополнение)';

//Params
$_['param_in_product'] = 'В товаре';
$_['param_in_category'] = 'В категории';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Рекомендуемые продукты"!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = ' ';
$_['module_licence'] = '';
