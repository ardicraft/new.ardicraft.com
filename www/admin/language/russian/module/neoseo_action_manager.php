<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Менеджер Акций</p>';
$_['heading_title_raw'] = 'NeoSeo Менеджер Акций';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_usefull'] = 'Полезные ссылки';

// Text
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_description'] = '<p>Таблица со списком акций находится в меню Каталог \ Акции. Но увидеть ее сможет только тот, у кого есть права на просмотр \ удаление для этого модуля</p><p>Соответственно, сразу после установки вы должны зайти в Система \ Пользователи \ Группы пользователей и добавить права на просмотр \ модификацию нужным группам пользователей</p>';
$_['clear'] = 'Очистить логи';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить лог';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладка:';
$_['entry_meta_title_ml'] = 'Метатег Title:';
$_['entry_meta_description_ml'] = 'Метатег Description:';
$_['entry_meta_keyword_ml'] = 'Метатег Keywords:';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';
$_['entry_brand_to_related'] = 'Добавлять бренды в автокомплит привязки продуктов:';
$_['entry_category_to_related'] = 'Добавлять категории в автокомплит привязки продуктов:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_other_errors'] = 'Нет нужных компонентов!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = "";
$_['error_license_missing'] = "";
$_['mail_support'] = "";
$_['module_licence'] = "";
$_['text_module_version'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-menedzher-akciy">https://neoseo.com.ua/nastroyka-modulya-neoseo-menedzher-akciy</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/menedzher-akcij#module_history">https://neoseo.com.ua/menedzher-akcij#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/menedzher-akcij#faqBox">https://neoseo.com.ua/menedzher-akcij#faqBox</a>';