<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Карта Сайта</span>';
$_['heading_title_raw'] = 'NeoSeo Карта сайта';

//Tabs
$_['tab_general'] = 'Параметры';
$_['tab_store'] = 'Магазин';
$_['tab_store_information'] = 'Информация';
$_['tab_store_category'] = 'Категория';
$_['tab_store_manufacturer'] = 'Производитель';
$_['tab_store_product'] = 'Товар';
$_['tab_blog'] = 'Блог';
$_['tab_blog_module'] = 'Модули';
$_['tab_blog_category'] = 'Категории';
$_['tab_blog_author'] = 'Авторы';
$_['tab_blog_article'] = 'Статьи';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';

// Text
$_['text_edit'] = 'Параметры';
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_default'] = 'По умолчанию';
$_['text_feed'] = 'Каналы продвижения';
$_['text_gzip_0'] = 'Отключено';
$_['text_gzip_1'] = 'Уровень 1';
$_['text_gzip_2'] = 'Уровень 2';
$_['text_gzip_3'] = 'Уровень 3';
$_['text_gzip_4'] = 'Уровень 4';
$_['text_gzip_5'] = 'Уровень 5';
$_['text_gzip_6'] = 'Уровень 6';
$_['text_gzip_7'] = 'Уровень 7';
$_['text_gzip_8'] = 'Уровень 8';
$_['text_gzip_9'] = 'Уровень 9';

$_['text_seo_0'] = 'Отключено';
$_['text_seo_1'] = 'SEO Pro';
$_['text_seo_2'] = 'SEO Url';


// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_image_status'] = 'Выводить картинки:';
$_['entry_image_status_desc'] = 'Не рекомендуется, не все роботы понимают';
$_['entry_seo_status'] = 'Система формирования ЧПУ:';
$_['entry_seo_url_include_path'] = 'Скрывать полный путь в ЧПУ категорий и товаров:';
$_['entry_seo_lang_status'] = 'Мультиязычный ЧПУ:';
$_['entry_filterpro_seo_status'] = 'SEO FilterPro:';
$_['entry_filterpro_seo_status_desc'] = '';
$_['entry_filtervier_seo_status'] = 'SEO FilterVier:';
$_['entry_filtervier_seo_status_desc'] = '';
$_['entry_ocfilter_seo_status'] = 'SEO OcFilter:';
$_['entry_ocfilter_seo_status_desc'] = '';
$_['entry_mfilter_seo_status'] = 'SEO MegaFilter:';
$_['entry_mfilter_seo_status_desc'] = '';
$_['entry_filter_seo_status'] = 'NeoSeo Посадочные страницы:';
$_['entry_filter_seo_status_desc'] = '';


$_['entry_category_brand_status'] = 'Категории с брендами:';
$_['entry_category_brand_status_desc'] = 'Будут сгенерированы ссылки вида категория/бренд';
$_['entry_status_addresses'] = 'Адресная информация:';
$_['entry_partition_status'] = 'Разбить карту на части:';
$_['entry_partition_volume'] = 'Размер части карты:';
$_['entry_multistore_status'] = 'Мультимагазин:';
$_['entry_gzip_status'] = 'Сжатие:';
$_['entry_url'] = 'Ссылка на sitemap.xml:';


$_['entry_category_status'] = 'Выводить в карте сайта';
$_['entry_category_url_date'] = 'Дата изменения';
$_['entry_category_url_frequency'] = 'Частота обновления';
$_['entry_category_url_priority'] = 'Приоритет обновления';

$_['entry_manufacturer_status'] = 'Выводить в карте сайта';
$_['entry_manufacturer_line_by_tima'] = 'Производители - Линии by T1ma (доп. модуль)';
$_['entry_manufacturer_url_date'] = 'Дата изменения';
$_['entry_manufacturer_url_frequency'] = 'Частота обновления';
$_['entry_manufacturer_url_priority'] = 'Приоритет обновления';

$_['entry_product_status'] = 'Выводить в карте сайта';
$_['entry_product_url_date'] = 'Дата изменения';
$_['entry_product_url_frequency'] = 'Частота обновления';
$_['entry_product_url_priority'] = 'Приоритет обновления';

$_['entry_information_status'] = 'Выводить в карте сайта';
$_['entry_information_url_date'] = 'Дата изменения';
$_['entry_information_url_frequency'] = 'Частота обновления';
$_['entry_information_url_priority'] = 'Приоритет обновления';


$_['entry_blog_freecart_status'] = 'Выводить NeoSeo Blog';
$_['entry_blog_pavo_status'] = 'Выводить Pavo Blog';
$_['entry_blog_seocms_status'] = 'Выводить SEO CMS Blog';
$_['entry_blog_blogmanager_status'] = 'Выводить Blog Manager';


$_['entry_blog_category_status'] = 'Выводить в карте сайта';
$_['entry_blog_category_url_date'] = 'Дата изменения';
$_['entry_blog_category_url_frequency'] = 'Частота обновления';
$_['entry_blog_category_url_priority'] = 'Приоритет обновления';

$_['entry_blog_author_status'] = 'Выводить в карте сайта';
$_['entry_blog_author_url_date'] = 'Дата изменения';
$_['entry_blog_author_url_frequency'] = 'Частота обновления';
$_['entry_blog_author_url_priority'] = 'Приоритет обновления';

$_['entry_blog_article_status'] = 'Выводить в карте сайта';
$_['entry_blog_article_url_date'] = 'Дата изменения';
$_['entry_blog_article_url_frequency'] = 'Частота обновления';
$_['entry_blog_article_url_priority'] = 'Приоритет обновления';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Резервные копии"!';


$_['text_module_version '] = '';


