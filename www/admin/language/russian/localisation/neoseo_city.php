<?php

// Heading
$_['heading_title'] = 'NeoSeo Города';

// Button
$_['button_refresh'] = 'Обновить список городов';

// Text
$_['text_success'] = 'Cписок городов обновлен!';
$_['text_list'] = 'Список городов';
$_['text_add'] = 'Добавление города';
$_['text_edit'] = 'Редактирование города';

// Column
$_['column_name'] = 'Название города';
$_['column_zone'] = 'Регион';
$_['column_country'] = 'Страна';
$_['column_action'] = 'Действие';

// Entry
$_['entry_name'] = 'Название города';
$_['entry_zone'] = 'Регион';
$_['entry_country'] = 'Страна';
$_['entry_status'] = 'Статус';
$_['button_filter'] = 'Отфильтровать';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения списка городов!';
$_['error_name'] = 'Название региона должно быть от 2 до 128 символов!';
$_['error_default'] = 'Город не может быть удален, поскольку она назначена по умолчанию!';
$_['error_store'] = 'Город нельзя удалить, поскольку он используется в %s магазине(ах)!';
$_['error_address'] = 'Город нельзя удалить, поскольку он прикреплен к %s адресу(ам)!';
$_['error_affiliate'] = 'Город нельзя удалить, поскольку он прикреплен к %s партнёру(ам)!';
