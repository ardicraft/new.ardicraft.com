<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Вместе дешевле</span>';
$_['heading_title_raw'] = 'NeoSeo Вместе Дешевле';

//Tab
$_['tab_general'] = 'Общие';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';
$_['button_download_log'] = 'Скачать файл логов';

// Text
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_default'] = 'По умолчанию';
$_['text_module'] = 'Статистика';
$_['text_price'] = 'Цена с учетом скидок';
$_['text_clear_price'] = 'Цена без учета скидок';

// Entry
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отладочный режим';
$_['entry_sort_order'] = 'Порядок сортировки';
$_['entry_price_type'] = 'Используемая цена';
$_['entry_price_type_desc'] = 'Укажите используемый тип цены: Цена без учета скидок - будет использована цена из карточки товара поля "цена". Цена с учетом скидок - будет использована цена товара с учетом действущих скидок.';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Программа лояльности"!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';
$_['text_module_version'] = '';
