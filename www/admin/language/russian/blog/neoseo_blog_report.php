<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Статистика просмотров страниц</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Статистика просмотров страниц';

// Column
$_['column_article_name'] = 'Название статьи';
$_['column_author_name']  = 'Имя автора';
$_['column_viewed']       = 'Просмотров';
$_['column_percent']      = 'В процентах';

$_['text_module_version'] = '';