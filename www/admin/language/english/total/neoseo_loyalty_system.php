<?php

// Heading
$_['heading_title']     = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Loyalty program</p>';
$_['heading_title_raw'] = 'NeoSeo Loyalty program';

//Tabs
$_['tab_general'] = 'General';
$_['tab_support'] = 'Support';
$_['tab_logs']    = 'Logs';
$_['tab_license'] = 'License';
$_['tab_discount_order'] = 'Discount order';

//Buttons
$_['button_save']           = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close']          = 'Close';
$_['button_recheck']        = 'Check again';
$_['button_clear_log']      = 'Clear Logs';
$_['button_download_log']   = 'Download log file';

// Text
$_['text_module_version']   = '';
$_['text_edit']             = 'Options';
$_['text_success']          = 'Module settings have been successfully updated!';
$_['text_success_clear']    = 'Logs successfully deleted';
$_['text_default']          = 'Default';
$_['text_module']           = 'Statistics';
$_['incart_text'] = 'Order more on {estsumm}, to get a discount{discount}%';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_customer_discount_status'] = 'Use personal discounts:';
$_['entry_customer_discount_status_desc'] = 'A personal discount is indicated in the customer`s card in the "Personal Discount" field.';
$_['entry_group_discount_status'] = 'Use group discounts:';
$_['entry_group_discount_status_desc'] = 'Group discount is indicated in the card of a group of buyers in the field "Personal discount for this group of buyers."';
$_['entry_cumulative_discount_status'] = 'Use cumulative discounts:';
$_['entry_total_discount_status'] = 'Use discounts for the amount of the order:';
$_['entry_total_discount_gradation'] = 'Graduation of discounts for the amount of the order:';
$_['entry_total_discount_gradation_desc'] = 'For example: 1000: 2.5, 2000: 10. This means that when the amount of the order is reached from 1,000 EUR, the discount will be 2.5%, and if you order from 2,000 EUR, the discount will be 10%.';
$_['entry_cumulative_discount_gradation'] = 'Graduation of cumulative discounts:';
$_['entry_cumulative_discount_gradation_desc'] = 'For example: 1000: 2.5, 2000: 10. This means that when the amount of orders fulfilled is from EUR 1,000, the discount will be 2.5%, and if you order from 2,000 EUR, the discount will be 10%.';
$_['entry_sort_order'] = 'Sorting order:';
$_['entry_excluded_categories'] = 'Disable discount for categories:';
$_['entry_excluded_manufacturers'] = 'Disable discount for manufacturers:';
$_['entry_excluded_products'] = 'Disable discount for products:';
$_['entry_incart_text_status'] = 'Display tooltip in the cart';
$_['entry_incart_text_status_desc'] = 'Showing a hint for a discount on the order amount';
$_['entry_incart_text'] = 'Text in cart';
$_['entry_incart_text_desc'] = 'Templates allowed: {estsumm} - Remaining amount for reorder, {discount} - size of the discount received';
$_['entry_personal_sort_order'] = 'Personal discount order';
$_['entry_group_sort_order'] = 'Group discount order';
$_['entry_accumulative_sort_order'] = 'Accumulative discount order';
$_['entry_sum_sort_order'] = 'Discount on order amount order';
$_['entry_in_neoseo_checkout_text_status'] = 'Display tooltip on checkout page';
$_['entry_in_neoseo_checkout_text_status_desc'] = 'Displaying a prompt for a discount on the order amount on the checkout page, provided that the "NeoSeo Checkout" module is used';
$_['entry_in_neoseo_checkout_text'] = 'Text on checkout page';
$_['entry_in_neoseo_checkout_text_desc'] = 'The text on the checkout page, subject to the use of the "NeoSeo Checkout" module. The following templates are allowed: {estsumm} - Remaining amount for backorder, {discount} - amount of received discount';

// Error
$_['error_permission']      = 'You do not have sufficient rights to edit the "NeoSeo Loyalty Program"!';
$_['error_post_size']       = "Increase the values of the post_max_size and upload_max_filesize parameters to exceed the archive size";
$_['error_download_logs']   = 'The log file is empty or missing!';
$_['error_ioncube_missing'] = '<h3 style = "color: red"> IonCube Loader is missing! </h3><p> To use our module, you need to install the IonCube Loader. Here are the instructions for installing the IonCube Loader for different cases: </p><ul><li> If you have shared hosting - <a href = "http://neoseo.com.ua/articles/ioncube-loader-shared "> http://neoseo.com.ua/articles/ioncube-loader-shared </a></li><li> If you have VPS on ubuntu - <a href =" http://neoseo.com. ua / articles / ioncube-loader-ubuntu "> http://neoseo.com.ua/articles/ioncube-loader-ubuntu </a></li><li> If you have a VPS on centos - <a href = "http://neoseo.com.ua/articles/ioncube-loader-centros">http://neoseo.com.ua/articles/ioncube-loader-centos </a></li></ul><p>If you do not get to install IonCube Loader yourself, you can also ask for help from our specialists at <a href="mailto:alex.sorokin@neoseo.com.ua"> alex.sorokin@neoseo.com.ua </a>, indicating exactly where you bought fashion l, your nickname on this resource and the order number. </p>';
$_['error_license_missing'] = '<h3 style = "color: red"> There is no license file! </h3><p> To obtain the license file, contact the module developer at <a href="mailto:alex.sorokin@neoseo.com.ua"> alex.sorokin @ neoseo.com.ua </a>, specifying exactly where you purchased the module, your nickname on this resource and the order number. </p><p> Put the license file in the root of the site, i.e. next to the robots.txt file and click "Check again." </p><p> You can not worry that someone will steal your license file! Your license file is made for you personally and will not work on another domain </p>';

$_['mail_support'] = '';
$_['module_licence'] = '';

$_['mail_support1'] = '<h3 style="color:red">Thank you for choosing our product!</h3><p><a href="http://neoseo.com.ua">Web Studio NeoSeo </a> makes every effort to ensure that its products are installed as quickly and easily as possible, without creating conflicts with other modules and themes, and delivering customers only the joy of using products. We will be glad if you <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank"> buy NeoSeo module Yandex Metrica </a> more once or any other module in the store seomag.com.ua. <b> Cumulative discounts apply! </b></p><p>However, this is not always possible, considering that opencart has very weak technical capabilities for this, so please take these nuances with understanding.</p><p><b> What we guarantee </b>, and we provide for free:</p><ul><li>the work of our modules on the standard issue of opencart</li><li>the work of our modules on the standard admin opencart</li></ul><p>If you have a problem with the module in this context, you can always request free technical support at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a>.</p><p><b>What we try to provide, but do not guarantee</b>:</p><ul><li>the work of our modules on a NON-standard theme opencart</li><li>the work of our modules on the NON-standard admin opencart</li></ul><p>As already mentioned, it is impossible to envisage in advance all the nuances of foreign themes, so in case of problems in this vein, we provide paid support for symbolic value. You can request it, as well as comprehensive maintenance of your store, at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p> <p><b> ATTENTION !!! </b> If you are having trouble installing the modules, then you do not need to spend your precious time on this routine process. Let our technical experts do this instead of you for a nominal fee, and you can spend the saved time on the development of your business, family and hobbies. You can order installation and maintenance at <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></p>';
$_['module_licence1'] = ' <h3 style="color:red">Thank you for choosing our product!</h3><p>All rights to the software product, then the module, belong to <a href="http://neoseo.com.ua"> NeoSeo Web Studio </a>. You can <a href="http://seomag.com.ua/moduli/moduli-obrabotki-zakazov/soforp-cash-memo" target="_blank"> buy NeoSeo Yandex Metrica </a> once again in the store seomag.com.ua. <b> Cumulative discounts apply! </b></p><p><b>The license for this module entitles you to:</b><ul><li>activation on <b> ONE domain </b>. Not on the site, not at the person, not at the studio. At you some domains are connected to one site - means you need several licenses.</li><li>to use at your store or customer store.</li><li>free updates to store owners within a year of purchase, regardless of who was the installer of the module</li></ul></p><p><b>Strictly forbidden:</b><ul><li>Publish the module on other sites without notifying the author</li><li>Transfer the module to third parties</li><li>Sell on its own behalf without prior agreement with the author</li><li>Use unlicensed versions of modules (warez). In case of violation, all purchases on the domain without refunds will be canceled.</li></ul></p><p><b>Denial of responsibility:</b><ul><li>The author of the module does not bear any responsibility for material and non-material damage caused by the module. You use the module at your own risk.</li><li>In order to significantly minimize the risks, you can <a href="http://seomag.com.ua/moduli/moduli-prochie/soforp-backup" target="_blank"> buy the NeoSeo backup module </a>, which is reliable protect your store from data loss, or order a comprehensive service for your store from the author <a href="mailto:alex.sorokin@neoseo.com.ua">alex.sorokin@neoseo.com.ua</a></li><li>The author reserves the right at any time to change the terms of the license agreement, without agreement with the end users of his products.</li></ul></p>';
