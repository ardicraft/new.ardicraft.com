<?php

$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Generator of Human-Readable URLs and metadata</span>';
$_['heading_title_raw'] = 'NeoSeo Generator of Human-Readable URLs and metadata';

// Column
$_['column_status_name'] = 'Status Name';
$_['column_template_subject'] = 'Message';

// Text
$_['text_success'] = 'Module settings updated!';
$_['text_success_seogen'] = 'Generation was successful!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';
$_['text_select_template'] = 'Specify a template';
$_["text_new_order"] = 'New order received';
$_['text_customer_templates'] = 'Messages to buyers';
$_['text_admin_templates'] = 'Messages to admins';
$_['text_generate'] = 'Generate';

// Button
$_['button_clear_log'] = 'Clear Log';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_insert'] = 'Create';
$_['button_delete'] = 'Remove';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_rewrite'] = 'Overwriting:';
$_['entry_debug'] = 'Debugging:';
$_['entry_limit_title'] = 'Length limit title:';
$_['entry_limit_description'] = 'Length limit description:'; 
$_['entry_option_name_value_separator'] = 'Separator between option name and value:'; 
$_['entry_option_values_separator'] = 'Separator between option values:'; 
$_['entry_options_separator'] = 'Separator between options:'; 
$_['entry_attribute_name_value_separator'] = 'Separator between attribute name and value:'; 
$_['entry_attribute_values_separator'] = 'Separator between attribute values:';
$_['entry_attributes_separator'] = 'Separator between attributes:';
$_['entry_language'] = 'The language for forming Human-Readable URLs:';
$_['entry_category'] = 'Categories:';
$_['entry_manufacturer'] = 'Manufacturer:';

$_['entry_products_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_h1'] = 'HTML tag h1:<br><span class="help">Available tags: [product_id], [product_h1], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_title'] = 'HTML tag title:<br><span class="help">Available tags: [product_id], [product_name], [product_title], [sku], [price], [model_name], [manufacturer_name], [category_name]</span>';
$_['entry_products_description'] = 'HTML tag description:<br><span class="help">Available tags: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes], [product_description]</span>';
$_['entry_products_product_description'] = 'Product description:<br><span class="help"> Available tags: [product_id], [product_name], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes], [product_description]</span>';
$_['entry_products_keywords'] = 'HTML tag keywords:<br><span class="help">Available tags: [product_id], [product_name], [product_keyword], [sku], [price], [model_name], [manufacturer_name], [category_name], [product_options], [product_attributes]</span>';

$_['entry_categories_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [category_name],[parent_category_name]</span>';
$_['entry_categories_h1'] = 'HTML tag h1:<br><span class="help">Available tags: [category_name], [category_h1],[parent_category_name]</span>';
$_['entry_categories_title'] = 'HTML tag title:<br><span class="help">Available tags: [category_name], [category_title],[parent_category_name]</span>';
$_['entry_categories_description'] = 'HTML tag description:<br><span class="help">Available tags: [category_name], [category_description],[parent_category_name]</span>';
$_['entry_categories_keywords'] = 'HTML tag keywords:<br><span class="help">Available tags: [category_name], [category_keyword],[parent_category_name]</span>';

$_['entry_manufacturers_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [manufacturer_name]</span>';
$_['entry_manufacturers_h1'] = 'HTML tag h1:<br><span class="help">Available tags: [manufacturer_name], [manufacturer_h1]</span>';
$_['entry_manufacturers_title'] = 'HTML tag title:<br><span class="help">Available tags: [manufacturer_name], [manufacturer_title]</span>';
$_['entry_manufacturers_description'] = 'HTML tag description:<br><span class="help">Available tags: [manufacturer_name], [manufacturer_description]</span>';
$_['entry_manufacturers_keywords'] = 'HTML tag keywords:<br><span class="help">Available tags: [manufacturer_name], [manufacturer_keyword]</span>';

$_['entry_articles_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [information_name]</span>';
$_['entry_articles_h1'] = 'HTML tag h1:<br><span class="help">Available tags: [information_title], [information_h1]</span>';
$_['entry_articles_title'] = 'HTML tag title:<br><span class="help">Available tags: [information_title], [information_title]</span>';
$_['entry_articles_description'] = 'HTML tag description:<br><span class="help">Available tags: [information_title], [information_description]</span>';
$_['entry_articles_keywords'] = 'HTML tag keywords:<br><span class="help">Available tags: [information_title], [information_keyword]</span>';

$_['entry_blogs_articles_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [blog_article_name]</span>';
$_['entry_blogs_articles_h1'] = 'HTML-tag h1:<br><span class="help">Available tags: [blog_article_name], [blog_article_h1]</span>';
$_['entry_blogs_articles_title'] = 'HTML-tag title:<br><span class="help">Available tags: [blog_article_name], [blog_article_title]</span>';
$_['entry_blogs_articles_description'] = 'HTML-tag description:<br><span class="help">Available tags: [blog_article_name], [blog_article_description]</span>';
$_['entry_blogs_articles_keywords'] = 'HTML-tag keywords:<br><span class="help">Available tags: [blog_article_name], [blog_article_keyword]</span>';

$_['entry_blogs_authors_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [blog_author_name]</span>';
$_['entry_blogs_authors_h1'] = 'HTML-tag h1:<br><span class="help">Available tags: [blog_author_name], [blog_author_h1]</span>';
$_['entry_blogs_authors_title'] = 'HTML-tag title:<br><span class="help">????????? ????: [blog_author_name], [blog_author_title]</span>';
$_['entry_blogs_authors_description'] = 'HTML-tag description:<br><span class="help">Available tags: [blog_author_name], [blog_author_description]</span>';
$_['entry_blogs_authors_keywords'] = 'HTML-tag keywords:<br><span class="help">Available tags: [blog_author_name], [blog_author_keyword]</span>';

$_['entry_blogs_categories_seo_url'] = 'SEO URL:<br><span class="help">Available tags: [blog_category_name]</span>';
$_['entry_blogs_categories_h1'] = 'HTML-tag h1:<br><span class="help">Available tags: [blog_category_name], [blog_category_h1]</span>';
$_['entry_blogs_categories_title'] = 'HTML-tag title:<br><span class="help">????????? ????: [blog_category_name], [blog_category_title]</span>';
$_['entry_blogs_categories_description'] = 'HTML-tag description:<br><span class="help">Available tags: [blog_category_name], [blog_category_description]</span>';
$_['entry_blogs_categories_keywords'] = 'HTML-tag keywords:<br><span class="help">Available tags: [blog_category_name], [blog_category_keyword]</span>';

$_['entry_filter_page_manufacturer'] = 'Name for the manufacturer:<br><span class="help">Specify the name for the manufacturer, if it is used in the landing</span>';
$_['entry_filter_page_h1'] = 'H1 :<br><span class="help">Available tags: [filter_page_name], [filter_page_h1], [filter_page_title], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_title'] = 'Document Title :<br><span class="help">Available tags: [filter_page_h1], [filter_page_title], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_description'] = 'Meta Description:<br><span class="help">Available tags: [filter_page_h1], [filter_page_description], [filter_page_category], [filter_page_filters]</span>';
$_['entry_filter_page_keywords'] = 'Meta Keywords :<br><span class="help">Available tags: [filter_page_h1], [filter_page_keyword], [filter_page_category], [filter_page_filters]</span>';
$_['entry_instruction'] = 'Read the module instruction:';
$_['entry_history'] = 'Changes history:';
$_['entry_faq'] = 'Frequency Asked Questions:';

$_['entry_cron'] = 'CRON';

// Tabs
$_['tab_general'] = 'General';
$_['tab_magazine'] = 'Store';
$_['tab_blogs'] = 'Blogs';
$_['tab_filters'] = 'Filters';
$_['tab_logs'] = 'Logs';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';
$_['tab_usefull'] = 'Usefull links';

// Sub tabs
$_['subtab_magazine_categories'] = 'Categories';
$_['subtab_magazine_products'] = 'Goods';
$_['subtab_magazine_manufacturers'] = 'Manufacturers';
$_['subtab_magazine_articles'] = 'Articles';

$_['subtab_blogs_categories'] = 'Categories';
$_['subtab_blogs_authors'] = 'Authors';
$_['subtab_blogs_articles'] = 'Article';

$_['subtab_filter_pages'] = 'NeoSeo Filter pages';

// Params
$_['params_products_seo_url'] = '[product_name]';
$_['params_products_h1'] = '[product_h1]';
$_['params_products_title'] = '[product_title]';
$_['params_products_keywords'] = '[product_keyword]';
$_['params_products_description'] = '[product_description]';
$_['params_products_product_description'] = '[category_name] [product_name] [model_name] [manufacturer_name]';

$_['params_categories_seo_url'] = '[category_name]';
$_['params_categories_h1'] = '[category_h1]';
$_['params_categories_title'] = '[category_title]';
$_['params_categories_description'] = '[category_description]';
$_['params_categories_keywords'] = '[category_keyword]';
$_['params_categories_parent_name'] = '[parent_category_name]';

$_['params_manufacturers_seo_url'] = '[manufacturer_name]';
$_['params_manufacturers_h1'] = '[manufacturer_h1]';
$_['params_manufacturers_title'] = '[manufacturer_title]';
$_['params_manufacturers_description'] = '[manufacturer_description]';
$_['params_manufacturers_keywords'] = '[manufacturer_keyword]';

$_['params_articles_seo_url'] = '[information_name]';
$_['params_articles_h1'] = '[information_h1]';
$_['params_articles_title'] = '[information_title]';
$_['params_articles_description'] = '[information_description]';
$_['params_articles_keywords'] = '[information_keyword]';

$_['params_blogs_articles_seo_url'] = '[blog_article_name]';
$_['params_blogs_articles_h1'] = '[blog_article_h1]';
$_['params_blogs_articles_title'] = '[blog_article_title]';
$_['params_blogs_articles_description'] = '[blog_article_description]';
$_['params_blogs_articles_keywords'] = '[blog_article_keyword]';

$_['params_blogs_authors_seo_url'] = '[blog_author_name]';
$_['params_blogs_authors_h1'] = '[blog_author_h1]';
$_['params_blogs_authors_title'] = '[blog_author_title]';
$_['params_blogs_authors_description'] = '[blog_author_description]';
$_['params_blogs_authors_keywords'] = '[blog_author_keyword]';

$_['params_blogs_categories_seo_url'] = '[blog_category_name]';
$_['params_blogs_categories_h1'] = '[blog_category_h1]';
$_['params_blogs_categories_title'] = '[blog_category_title]';
$_['params_blogs_categories_description'] = '[blog_category_description]';
$_['params_blogs_categories_keywords'] = '[blog_category_keyword]';

$_['params_filter_page_manufacturer'] = 'Manufacturer';
$_['params_filter_page_title'] = '[filter_page_h1]';
$_['params_filter_page_title'] = '[filter_page_title]';
$_['params_filter_page_description'] = '[filter_page_description]';
$_['params_filter_page_keywords'] = '[filter_page_keyword]';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';





$_['text_module_version'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-generator-chpu-i-metadannyh">https://neoseo.com.ua/nastroyka-modulya-generator-chpu-i-metadannyh</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/generator-chpu-i-metadannyh#module_history">https://neoseo.com.ua/generator-chpu-i-metadannyh#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/generator-chpu-i-metadannyh#faqBox">https://neoseo.com.ua/generator-chpu-i-metadannyh#faqBox</a>';