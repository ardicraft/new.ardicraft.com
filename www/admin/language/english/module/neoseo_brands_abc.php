<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Brands in alphabetical</p>';
$_['heading_title_raw'] = 'NeoSeo Brands in alphabetical';

$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';


// Text
$_['text_success'] = 'Module settings updated!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';

$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';

$_['entry_debug'] = 'Debug Mode';
$_['entry_debug_desc'] = 'The module logs will write various information for the module developer';
$_['entry_status'] = 'Status:';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';



