<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Менеджер Акций - Список Акций';
$_['heading_title_raw'] = 'NeoSeo Менеджер Акций - Список Акций';
$_['new_title'] = 'Похожие товары';

$_['tab_general'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';
$_['tab_usefull'] = 'Полезные ссылки';

// Text
$_['text_module'] = 'Модули';
$_['button_clear_log'] = 'Очистить лог';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_download_log'] = 'Скачать файл логов';

// Entry
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отладка';
$_['entry_name'] = 'Название';
$_['entry_block'] = 'Способ отображения';
$_['entry_title'] = 'Заголовок';
$_['entry_limit'] = 'Количество акций в отображении';
$_['entry_description_limit'] = 'Количество символов описания';
$_['entry_view'] = 'Схема';
$_['entry_image_width'] = 'Ширина изображения';
$_['entry_image_height'] = 'Высота изображения';
$_['entry_sort_order'] = 'Порядок:';
$_['entry_template'] = 'Шаблон:';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

// Text
$_['text_clear_log'] = 'Очистить лог';
$_['text_module_version'] = '';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_default_title'] = 'Просмотренные продукты';
$_['text_template'] = 'Шаблон';
$_['text_alone_block'] = 'Блок одиночный';
$_['text_categories'] = 'Блок категорий';


// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_image'] = 'Image width &amp; height dimensions required!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-menedzher-akciy">https://neoseo.com.ua/nastroyka-modulya-neoseo-menedzher-akciy</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/menedzher-akcij#module_history">https://neoseo.com.ua/menedzher-akcij#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/menedzher-akcij#faqBox">https://neoseo.com.ua/menedzher-akcij#faqBox</a>';