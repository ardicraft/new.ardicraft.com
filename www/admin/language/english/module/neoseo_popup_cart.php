<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="margin-right: 10px;float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Pop-up basket</p>';
$_['heading_title_raw'] = 'NeoSeo Pop-up basket';

// Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Module settings updated!';
$_['text_module'] = 'Modules';
$_['text_order_date_created'] = 'Date of creation';
$_['text_order_date_modified'] = 'Date of change';
$_['text_order_date_current'] = 'The current date';
$_['text_separate_column'] = 'In a separate column';
$_['text_product_column'] = 'In the product column';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';

// Button
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';

// Entry
$_['entry_supplier_info'] = 'Provider:';
$_['entry_text'] = 'Additional text:';
$_['entry_debug'] = 'Debug mode:';
$_['entry_debug_desc'] = 'The module logs will write various information for the module developer';
$_['entry_status'] = 'Status:';
$_['entry_replace_status'] = 'Remove the old button:';
$_['entry_customer_info_format'] = 'Customer Info:<br><br>Supported tags:<br><small>{country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}</small>';
$_['entry_payment_info_format'] = 'Payment Information:<br><br>Supported tags:<br><small>{method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}</small>';
$_['entry_shipping_info_format'] = 'Information about delivery:<br><br>Supported tags:<br><small>{method}, {country},  {zone}, {zone_code}, {city}, {postcode}, {address_1}, {address_2}, {firstname}, {lastname}, {company}</small>';
$_['entry_store_name'] = 'Name of shop:<br><small>If not filled, it will be taken from the store settings</small>';
$_['entry_store_url'] = 'Store URL:<br><small>If it is not filled, it will not be displayed</small>';
$_['entry_store_phone'] = 'Store Phone:<br><small>If not filled, it will be taken from the store settings</small>';
$_['entry_store_email'] = 'Store mail:<br><small>If not filled, it will be taken from the store settings</small>';
$_['entry_store_logo'] = 'Shop logo:<br><small>If not selected, it will be taken from the store settings</small>';
$_['entry_store_logo_width'] = 'Width of store logo';
$_['entry_store_logo_height'] = 'Height of store logo';
$_['entry_order_date'] = 'Order date:<br><small>The date that will be displayed in the check</small>';
$_['entry_column_sku_status'] = 'Show article:<br><small>Show or not article in the list of goods</small>';
$_['entry_column_image_status'] = 'Show image:<br><small>Show or not picture of goods  </small>';
$_['entry_column_model_status'] = 'Show model:<br><small>Show or not model in the list of goods</small>';
$_['entry_field_list_name'] = 'Template';
$_['entry_field_list_desc'] = 'Description';
$_['entry_show_comment'] = 'Show note to the order';
$_['entry_show_last_product'] = 'Display only the last added item';

$_['field_desc_order_id'] = 'Order number';
$_['field_desc_invoice_no'] = 'Account number';
$_['field_desc_date_added'] = 'Order creation date';
$_['field_desc_date_modified'] = 'Order change date';
$_['field_desc_date_current'] = 'The current date';
$_['field_desc_store_name'] = 'Name of shop';
$_['field_desc_store_url'] = 'Link to the store';
$_['field_desc_store_address'] = 'Store Address';
$_['field_desc_store_email'] = 'Store Email';
$_['field_desc_store_phone'] = 'Store phone number';
$_['field_desc_store_fax'] = 'Store Fax';
$_['field_desc_store_owner'] = 'Shopkeeper';
$_['field_desc_text'] = 'Additional text from module settings';
$_['field_desc_email'] = 'Buyer Email';
$_['field_desc_customer_info'] = 'Information about the buyer according to the settings of the module';
$_['field_desc_firstname'] = 'Buyer`s name';
$_['field_desc_lastname'] = 'Buyer`s surname';
$_['field_desc_telephone'] = 'Buyer phone number';
$_['field_shipping_firstname'] = 'Receiver name ';
$_['field_shipping_lastname'] = 'Receiver surname';
$_['field_desc_shipping_company'] = 'Company of the recipient';
$_['field_desc_shipping_address_1'] = 'Address of 1 recipient';
$_['field_desc_shipping_address_2'] = 'Address of 2 recipient';
$_['field_desc_shipping_city'] = 'City of the recipient';
$_['field_desc_shipping_postcode'] = 'Postal code of the recipient';
$_['field_desc_shipping_zone'] = 'Recipient Region';
$_['field_desc_shipping_zone_code'] = 'Region code of the recipient';
$_['field_desc_shipping_country'] = 'Recipien`s country';
$_['field_desc_shipping_info'] = 'Delivery information according to module settings';
$_['field_desc_shipping_method'] = 'Shipping method name';
$_['field_desc_payment_firstname'] = 'Payor name';
$_['field_desc_payment_lastname'] = 'Payor surname';
$_['field_desc_payment_company'] = 'Company payer';
$_['field_desc_payment_address_1'] = 'Address of 1 payer';
$_['field_desc_payment_address_2'] = 'Address of 2 payer';
$_['field_desc_payment_city'] = 'City of the payer';
$_['field_desc_payment_postcode'] = 'Postal code of the payer';
$_['field_desc_payment_zone'] = 'Region of the payer';
$_['field_desc_payment_zone_code'] = 'Region code of the payer';
$_['field_desc_payment_country'] = 'Country of payer';
$_['field_desc_payment_info'] = 'Delivery information according to module settings';
$_['field_desc_payment_method'] = 'Shipping method name';
$_['field_desc_product'] = 'List of products ( name, model, option, quantity, price, total)';
$_['field_desc_voucher'] = 'List of vouchers ( description, amount )';
$_['field_desc_total'] = 'Order summary ( code, title, text, value )';
$_['field_desc_total_str'] = 'Suma in cuirsive';
$_['field_desc_comment'] = 'Order note';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_supplier_info'] = 'This field is required!!';




$_['text_module_version'] = '';
