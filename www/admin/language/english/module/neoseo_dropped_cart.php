<?php

// Column
$_['column_name'] = 'Name';
$_['column_quantity'] = 'Number';
$_['column_price'] = 'Price';

// Text
$_['text_top'] = 'You have left your cart #%d of %s';
$_['text_total'] = 'Total:';
$_['text_bottom'] = 'Click the link to confirm the order or continue shopping.';

// Error
$_['error_missing_cart_id'] = 'Missing cart ID.';
$_['error_cart_not_found'] = 'Cart isn\'t found';
$_['error_template_not_found'] = 'Litter template isn\'t found';
$_['error_empty_email'] = 'Empty email';
