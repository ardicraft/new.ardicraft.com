<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Category';
$_['heading_title_raw'] = 'NeoSeo Blog Category';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Blog Category module!';
$_['text_edit']        = 'Edit Blog Category';
$_['text_module_version'] = '';

// Entry
$_['entry_status']     = 'Status';
$_['entry_title']      = 'Module Title';
$_['entry_root_category'] = 'Root category';
$_['entry_template']   = 'Template';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Blog Category module!';