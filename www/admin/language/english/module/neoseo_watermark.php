<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Watermark</span>';
$_['heading_title_raw'] = 'NeoSeo Watermark';
$_['new_title'] = 'Watermark';

$_['tab_general'] = 'General';
$_['tab_params'] = 'Params';
$_['tab_logs'] = 'Logs';
$_['tab_license'] = 'License';
$_['tab_support'] = 'Support';
$_['tab_addition_params'] = 'Addition params';
$_['tab_usefull'] = 'Usefull links';

// Button
$_['button_clear_log'] = 'Clear log';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and close';
$_['button_recheck'] = 'Recheck';
$_['button_clean'] = 'Clean cache';
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and close';
$_['button_close'] = 'Close';

// Text
$_['text_module'] = 'Modules';
$_['text_edit'] = 'Adjusting watermark settings';
$_['text_success_options'] = 'Module settings updated!';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear log';
$_['text_success_clean'] = 'Image cache cleared!';
$_['text_browse'] = 'Browse';
$_['text_clear'] = 'Clear';
$_['text_image_manager'] = 'Image manager';

// Entry
$_['entry_transparent'] = 'Transparent background of PNG images:';
$_['entry_status'] = '<label class="control-label">Status:</label>';
$_['entry_hide_real_path'] = '<label class="control-label">Hide the path to the image in the folder image:</label>';
$_['entry_image'] = '<label class="control-label">Watermark:</label>';
$_['entry_exclude'] = '<label class="control-label">Exclude folders:</label><br><span class="help">Do not apply a watermark to images that are in folders. </span>';
$_['entry_min_size'] = '<label class="control-label">Minimum image size:</label><br><span class="help">Min image size for watermark application. </span>';
$_['entry_max_size'] = '<label class="control-label">Maximum image size:</label><br><span class="help">Max image size for watermark application. </span>';
$_['entry_debug'] = '<label class="control-label">Debug:</label>';
$_['entry_header_clear_cache'] = '<label class="control-label">Header cache clear button</label>';
$_['entry_instruction'] = 'Read the module instruction:';
$_['entry_history'] = 'Changes history:';
$_['entry_faq'] = 'Frequency Asked Questions:';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_image'] = 'Image width &amp; height dimensions required!';
$_['error_ioncube_missing'] = "";
$_['error_license_missing'] = "";
$_['mail_support'] = "";
$_['module_licence'] = "";
$_['text_module_version'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-vodyanoy-znak">https://neoseo.com.ua/nastroyka-modulya-neoseo-vodyanoy-znak</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/vodyanoj-znak#module_history">https://neoseo.com.ua/vodyanoj-znak#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/vodyanoj-znak#faqBox">https://neoseo.com.ua/vodyanoj-znak#faqBox</a>';