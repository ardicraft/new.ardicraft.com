<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Informative message</span>';
$_['heading_title_raw'] = 'NeoSeo Informative message';

// Tab
$_['tab_general'] = 'Options';
$_['tab_logs'] = 'Logs';
$_['tab_fields'] = 'Fields';
$_['tab_support'] = 'Support';
$_['tab_license'] = 'License';

// Text
$_['text_module_version'] = '';
$_['text_success'] = 'Module settings have been successfully updated!';
$_['text_module'] = 'Modules';
$_['text_success_clear'] = 'The log file has been successfully cleared!';
$_['text_clear_log'] = 'Clear Log';
$_['text_clear'] = 'Clear';

//Buttons
$_['button_save'] = 'Save';
$_['button_save_and_close'] = 'Save and Close';
$_['button_close'] = 'Close';
$_['button_recheck'] = 'Check again';
$_['button_clear_log'] = 'Clear Log';
$_['button_download_log'] = 'Download log file';

// Entry
$_['entry_status'] = 'Status:';
$_['entry_debug'] = 'Debug mode:<br /><span class="help">The module logs will write various information for the module developer.</span>';
$_['entry_text'] = 'Message';
$_['entry_message_desc'] = "Enter the message that will be displayed in the site header";
$_['entry_show_close_button'] = 'Display the close button';
$_['entry_close_button_text'] = 'Close button text';
$_['entry_color_background'] = 'Background color';
$_['entry_color_close_button'] = 'Close button color';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_supplier_info'] = 'This field is required!';
$_['error_download_logs'] = 'The log file is empty or missing!';
$_['error_ioncube_missing'] = '<h3 style="color:red">IonCube Loader is missing!</h3><p>To use our module, you need to install the IonCube Loader. Here are the instructions for installing the IonCube Loader for different cases:</p><ul><li>If you have shared hosting - <a href="http://soforp.com/faq/ioncube-loader-shared">http://soforp.com/faq/ioncube-loader-shared</a></li><li>If you have VPS on ubuntu - <a href="http://soforp.com/faq/ioncube-loader-ubuntu">http://soforp.com/faq/ioncube-loader-ubuntu</a></li><li>If you have a VPS on centos - <a href="http://soforp.com/faq/ioncube-loader-centos">http://soforp.com/faq/ioncube-loader-centos</a></li></ul><p>If you do not get to install IonCube Loader yourself, you can also ask for help from our specialists at <a href="mailto:alex@soforp.com">alex@soforp.com</a>, indicating exactly where you purchased the module, your nickname on this resource and the order number.</p>';
$_['error_license_missing'] = '<h3 style="color:red">There is no license file!</h3><p>To obtain the license file, contact the module developer at <a href="mailto:alex@soforp.com">alex@soforp.com</a>, specifying exactly where you purchased the module, your nickname on this resource and the order number.</p><p>Put the license file in the root of the site, i.e. next to the robots.txt file and click "Check again.".</p><p>You can not worry that someone will steal your license file! Your license file is made for you personally and will not work on another domain</p>';

// Error
$_['error_permission'] = 'You do not have the rights to manage this module!';
$_['error_download_logs'] = 'The log file is empty or missing!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['mail_support'] = '';
$_['module_licence'] = '';