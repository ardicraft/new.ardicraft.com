<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Related Products';
$_['heading_title_raw'] = 'NeoSeo Blog Related Products';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Module Blog Articles successfully changed!';
$_['text_edit']        = 'Editing the module Blog Related products';
$_['text_module_version'] = '';
$_['text_column']      = 'Column';
$_['text_grid']     = 'Grid';

// Buttons
$_['button_add_articles'] = 'Add article';

// Entry
$_['entry_name']       = 'Module name';
$_['entry_title']      = 'Module header';
$_['entry_category']   = 'Category';
$_['entry_root_category'] = 'Restricted by category:';
$_['entry_type']       = 'Type';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['entry_template']   = 'Template';
$_['entry_selected_articles'] = 'Selected articles';
$_['entry_article_name'] = 'Article title:';
$_['entry_sort_order']   = 'Sorting:';
$_['entry_status']       = 'Status:';

// Error
$_['error_permission'] = 'You do not have the right to modify the module Blog Related products!';
$_['error_name']       = 'The module name must be between 3 and 64 characters!';
$_['error_width']      = 'The width is mandatory!';
$_['error_height']     = 'The height is mandatory!';