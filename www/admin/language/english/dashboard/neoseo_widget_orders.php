<?php

//Columns
$_['column_order_number'] = '№';
$_['column_customer'] = 'Покупатель';
$_['column_order_status'] = 'Статус';
$_['column_date_added'] = 'Дата создания';
$_['column_order_total'] = 'Сумма';
$_['column_payment'] = 'Метод оплаты';
$_['column_shipping'] = 'Метод доставки';
$_['column_comment'] = 'Комментарий';
$_['column_order_referrer'] = 'Источник заказа';

//Text
$_['text_customer_email'] = '';
$_['text_customer_telephone'] = '';
