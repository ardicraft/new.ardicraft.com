<?php
$_['heading_title'] = 'Брошенная корзина';

//Columns
$_['column_dropped_cart_number'] = '№';
$_['column_email'] = 'Email';
$_['column_customer'] = 'Покупатель';
$_['column_telephone'] = 'Телефон';
$_['column_total'] = 'Сумма';
$_['column_date'] = 'Дата изменения:';
$_['column_action'] = 'Действие';
$_['column_notification'] = 'Оповещений:';


//Entry
$_['entry_domain'] = 'Домен';
$_['entry_email'] = 'Email';

//Text
$_['text_no_information'] = 'Информация отсутствует. Требуется включить модуль "NeoSeo Оформление заказа"';
$_['text_no_install'] = 'Информация отсутствует. Требуется установить модуль "NeoSeo Оформление заказа"';
$_['text_buy'] = 'Купить модуль';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';
$_['text_isset_redirect'] = 'Редирект добавлен';

//Buttons
$_['button_request'] = 'Оставить заявку';
$_['button_notify'] = 'Отправить сообщение';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Оформление заказа"';
$_['params_message'] = 'Домен: %s. Почта: %s.. Модуль: "NeoSeo Оформление заказа"';
$_['params_link'] = 'https://neoseo.com.ua/en/oformlenie-zakaza';