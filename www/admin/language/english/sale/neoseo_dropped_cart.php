<?php

// Heading
$_['heading_title'] = 'Dropped Cart';
$_['text_dropped_cart'] = 'Dropped Carts';

$_['column_email'] = 'Email';
$_['column_name'] = 'Name';
$_['column_total'] = 'Total';
$_['column_subtotal'] = 'Subtotal';
$_['column_modified'] = 'Changed';
$_['column_phone'] = 'Phone';
$_['column_action'] = 'Action';
$_['column_quantity'] = 'Quantity';
$_['column_options'] = 'Options';
$_['column_price'] = 'Price';
$_['column_notification_count'] = 'Notifications sent';

$_['entry_email'] = 'E-mail box';
$_['entry_name'] = 'Name';
$_['entry_phone'] = 'Phone';
$_['entry_total'] = 'Total';
$_['entry_modified'] = 'Changed';
$_['entry_notification_count'] = 'Notifications sent';

$_['error_missing_cart_id'] = 'Missing cart ID';
$_['error_cart_not_found'] = 'Cart has not been found';
$_['error_template_not_found'] = 'Template "%s" has not been found';

$_['email_send_successfully'] = 'Notification was sent successfully';
$_['email_sending_error'] = 'Notification sending error';

$_['button_send_notification'] = 'Send Notification';
$_['button_view'] = 'View';
