<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Authors</p>';
$_['heading_title_raw'] = 'NeoSeo Blog Authors';

// Columns
$_['column_author_name'] = 'Author Name';
$_['column_status'] = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action'] = 'Action';

// Text
$_['text_success'] = 'Success: You have modified the blog Authors!';
$_['text_edit'] = 'Edit article';
$_['text_add'] = 'Add article';
$_['text_module_version'] = '';

// Help
$_['help_name'] = 'Author name must be unique.';
$_['help_seo_url'] = 'Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.';

// Entry
$_['entry_name'] = 'Author Name:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Avatar:';
$_['entry_type'] = 'Type:';
$_['entry_teaser'] = 'Teaser:';
$_['entry_description'] = 'Description:';
$_['entry_meta_title'] = 'HTML Tag Title';
$_['entry_meta_h1'] = 'HTML Tag H1';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_meta_keyword'] = 'Meta Tag Keywords:';
$_['entry_status'] = 'Status:';


// Errors
$_['error_warning'] = 'Warning: Please check form carefully for errors!';
$_['error_permission'] = 'Warning: You do not have permission to modify the authors!';
$_['error_name'] = 'Author Name must be between 3 and 255 characters!';
$_['error_author_found'] = 'Author Name already exists, author name must unique!';
$_['error_seo_url'] = 'SEO keyword already in use!';
$_['error_article'] = 'Warning: This Author cannot be deleted as it is currently assigned to %s articles!';
