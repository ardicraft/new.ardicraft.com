<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Blog Article</p>';
$_['heading_title_raw'] = 'NeoSeo Blog Article';

// Text
$_['text_success'] = 'Success: You have modified articles!';
$_['text_edit'] = 'Edit Article';
$_['text_add'] = 'Add Article';
$_['text_module_version'] = '';

// Buttons
$_['button_add_articles'] = 'Add Article';

// Columns
$_['column_article_name'] = 'Article Title';
$_['column_author_name'] = 'Author Name';
$_['column_category'] = 'Category';
$_['column_sort_order'] = 'Sort Order';
$_['column_status'] = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action'] = 'Action';

// Tab
$_['tab_related'] = 'Related';

// Help
$_['help_title'] = 'Article Title must be unique.';
$_['help_teaser'] = 'Shows on the article list';
$_['help_seo_url'] = 'Do not use spaces instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_image'] = 'This contain main article image.';
$_['help_related_article_name'] = 'Article name must be select on autocomplete.';


// Entry
$_['entry_title'] = 'Article Title:';
$_['entry_description'] = 'Description:';
$_['entry_teaser'] = 'Teaser:';
$_['entry_author_name'] = 'Author Name:';
$_['entry_meta_title'] = 'HTML Tag Title';
$_['entry_meta_h1'] = 'HTML Tag H1';
$_['entry_meta_description'] = 'Meta Description:';
$_['entry_meta_keyword'] = 'Meta keyword:';
$_['entry_allow_comment'] = 'Allow comments for this article?:';
$_['entry_seo_url'] = 'SEO URL:';
$_['entry_image'] = 'Image:';
$_['entry_images'] = 'Images:';
$_['entry_sort_order'] = 'Sort Order:';
$_['entry_status'] = 'Status:';
$_['entry_category'] = 'Category:';
$_['entry_main_category'] = 'Main category';
$_['entry_store'] = 'Store:';
$_['entry_layout'] = 'Layout Override:';
$_['entry_related_products'] = 'Related Products:';
$_['entry_related_products_product'] = 'Product:';
$_['entry_related_products_category'] = 'Category:';
$_['entry_related_products_manufacturer'] = 'Manufacturer:';
$_['entry_related_articles'] = 'Related Articles';
$_['entry_related_article_name'] = 'Article Name:';
$_['entry_date_added'] = 'Date added:';
$_['entry_date_modified'] = 'Date modified:';

// Error
$_['error_warning'] = 'Warning: Please check the form carefully for errors!';
$_['error_permission'] = 'Warning: You do not have permission to modify article!';
$_['error_title'] = 'Article Title must be greater than 3 and less than 100 characters!';
$_['error_title_found'] = 'Article Title already exists, article title must be unique!';
$_['error_description'] = 'Description must be greater than 3 characters!';
$_['error_author_name'] = 'Author name can\'t be blank!';
$_['error_author_not_found'] = 'Author not found!';
$_['error_seo_url'] = 'SEO keyword already in use!';
$_['error_article_related'] = 'Warning: You can\'t delete article because currently it relate %s articles!';
