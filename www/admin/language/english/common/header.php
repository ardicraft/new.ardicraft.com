<?php
// Heading
$_['heading_title']        = 'SEO Store';

// Text
$_['text_order']             = 'Orders';
$_['text_processing_status'] = 'Processing';
$_['text_complete_status']   = 'Completed';
$_['text_customer']          = 'Customers';
$_['text_online']            = 'Customers Online';
$_['text_approval']          = 'Pending approval';
$_['text_product']           = 'Products';
$_['text_stock']             = 'Out of stock';
$_['text_review']            = 'Reviews';
$_['text_return']            = 'Returns';
$_['text_affiliate']         = 'Affiliates';
$_['text_store']             = 'Stores';
$_['text_front']             = 'Store Front';
$_['text_help']              = 'Help';
$_['text_homepage']          = 'Homepage';
$_['text_support']           = 'Support Forum';
$_['text_documentation']     = 'Documentation';
$_['text_logout']            = 'Logout';
$_['text_stores']            = 'Open site';

$_['button_delete_demo_data']       ='Удалить демо данные';
$_['text_demo_data_delete']         ='Будут удалены все демо-данные из магазина, такие как товары, категории, опции фильтра, посадочные страницы. Даное действие невозможно отменить или вернуть';
$_['text_yes'] = 'Да';
$_['text_no'] = 'Нет';
$_['text_mute'] = 'Больше не напоминать';
$_['text_confirm_delete'] = 'Вы уверены, Данное действие не обратимо!!!';
