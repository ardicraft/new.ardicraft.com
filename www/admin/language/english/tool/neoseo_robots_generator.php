<?php

// Heading
$_['heading_title'] = 'NeoSeo Generator robots.txt';

//Button
$_['button_close'] = 'Close';
$_['button_refresh'] = 'Update';
$_['button_save'] = 'Write down';
$_['button_ganerate'] = 'Generate';

// Text
$_['text_module_version'] = '';
$_['text_neoseo_robots_generator'] = 'Robots.txt generator';
$_['text_confirm_save'] = 'Attention! This will overwrite the current robots.txt file! Continue?';
$_['text_description'] = '<p>Below is the generated robots.txt content that corresponds to the robots.txt generation standard and the current Human-Readable URLs for the technical sections of your site. </p> <p> You can add missing rules and click the Save button, or simply the Save button if you are satisfied with the current contents of the file</p>';
$_['text_save_success'] = 'The robots.txt file was successfully updated!';
$_['text_generate_success'] = 'The robots.txt file was successfully generated!';

//Errors
$_['error_no_content'] = 'Could not write robots.txt. Check the write permissions for this file and try again.';
$_['error_save'] = 'Could not write robots.txt. Check the write permissions for this file and try again.';
$_['error_permission'] = 'You do not have permission to generate a file!';
