<?php

// Heading
$_['heading_title'] = 'Несуществующие страницы';

// Text
$_['text_module_version'] = '';
$_['text_neoseo_broken_links'] = 'Несуществующие страницы';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_setting_success'] = 'Настройки модуля обновлены!';
$_['text_module'] = 'Модули';
$_['text_list'] = 'Список ссылок';
$_['text_none'] = '---';
$_['text_toggle_columns'] = 'Колонки';
$_['text_table_search'] = 'Фильтр:';
$_['text_success_clear_list'] = 'Журнал 404 ошибок очищен!';
$_['text_table_results_per_page'] = 'Записей на страницу: _MENU_';
$_['text_table_info'] = 'Записи с _START_ по _END_ из _TOTAL_';
$_['text_table_info_empty'] = 'Записи с 0 по 0';
$_['text_table_info_filtered'] = 'выбрано из _MAX_ записей';
$_['text_table_zero_records'] = 'Данные по запросу не найдены';
$_['text_table_empty'] = 'Нет данных';
$_['text_table_processing'] = 'Выполняется обработка...';
$_['text_table_loading'] = 'Выполняется загрузка данных...';
$_['text_table_first'] = '|<';
$_['text_table_prev'] = '<';
$_['text_table_next'] = '>';
$_['text_table_last'] = '>|';
$_['text_indicator_saving'] = "<img src='view/image/loading.gif' />";
$_['text_isset_redirect'] = 'Редирект добавлен ';

// Entry
$_['entry_date'] = 'Дата';
$_['entry_ip'] = 'IP';
$_['entry_browser'] = 'Browser';
$_['entry_referer'] = 'Referer';
$_['entry_request_uri'] = 'Запрашиваемая страница';

// Button
$_['button_clear'] = 'Сброс';
$_['button_filter'] = 'Фильтр';
$_['button_add_redirect'] = 'Добавить редирект';

// Column
$_['column_date'] = 'Дата';
$_['column_ip'] = 'IP';
$_['column_browser'] = 'Browser';
$_['column_referer'] = 'Referer';
$_['column_request_uri'] = 'Запрашиваемая страница';

// Error
$_['error_permission'] = 'Ошибка: У вас нет прав на измененеи данных модуля!';
