<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Коментарі</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Коментарі';

// Text
$_['text_success']        = 'Коментар успішно оновлений!';
$_['text_edit']           = 'Редагування коментарів';
$_['text_add']            = 'Створення коментаря';
$_['text_module_version'] = '';
$_['text_parent_comment'] = 'Батьківський коментар';
$_['text_child_comments'] = 'Відповіді на коментар';
$_['text_goto_comment']   = 'перейти';

// Columns
$_['column_article_name'] = 'Назва статті';
$_['column_author_name']  = 'Ім\'я автора';
$_['column_status']       = 'Статус';
$_['column_date_added']   = 'Дата додавання';
$_['column_action']       = 'Дія';
$_['column_comment']      = 'Коментар';

// Buttons
$_['button_add_reply']    = 'Додати відповідь';

// Help
$_['help_article']        = 'Пошук через автодоповнення.';

// Tab
$_['tab_comment']         = 'Коментар';

// Entry
$_['entry_author']        = 'Ім\'я автора:';
$_['entry_article']       = 'Назва статті:';
$_['entry_rating']        = 'Рейтинг:';
$_['entry_status']        = 'Статус:';
$_['entry_comment']       = 'Коментар:';
$_['entry_reply_comment'] = 'Відповідь';

// Error
$_['error_warning']       = 'Перевірте форму на наявність помилок!';
$_['error_permission']    = 'У вас немає прав змінювати коментарі!';
$_['error_author']        = 'Ім\'я автора повинно бути від 3 до 63 символів!';
$_['error_comment']       = 'Текст коментаря повинен бути від 3 до 1000 символів!';
$_['error_article_name']  = 'Назва статті обов\'язкова!';
$_['error_article_not_found'] = 'Стаття не знайдена!';