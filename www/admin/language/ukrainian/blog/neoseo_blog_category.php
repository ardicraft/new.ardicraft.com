<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Категорії</p>';
$_['heading_title_raw'] = 'NeoSeo Блог Категорії';
	
// Text
$_['text_success']          = 'Категорія успішно змінена!';
$_['text_edit']             = 'Редагування категорії';
$_['text_add']              = 'Створення категорії';
$_['text_module_version'] = '';

// Column
$_['column_name']           = 'Назва категорії';
$_['column_sort_order']     = 'Сортування';
$_['column_status']         = 'Статус';
$_['column_action']         = 'Дія';

// Help
$_['help_seo_url']          = 'Повинно бути унікальним, не використовуйте пробіли, заміняйте їх на "-".';
$_['help_top']              = 'Відображається в меню, працює лише від батьківських категорій.';

// Entry
$_['entry_name']             = 'Назва категорії:';
$_['entry_meta_title']       = 'HTML-тег Title';
$_['entry_meta_h1']          = 'HTML-тег H1';
$_['entry_meta_keyword']     = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_teaser']           = 'Короткий опис:';
$_['entry_description']      = 'Опис:';
$_['entry_parent']           = 'Батьківська категорія:';
$_['entry_store']            = 'Магазин:';
$_['entry_seo_url']          = 'SEO URL:';
$_['entry_image']            = 'Зображення:';
$_['entry_sort_order']       = 'Сортування:';
$_['entry_status']           = 'Статус:';
$_['entry_top']              = 'Додати в меню:';
$_['entry_layout']           = 'Шаблон:';

// Error 
$_['error_warning']          = 'Перевірте форму на наявність помилок!';
$_['error_permission']       = 'У вас немає прав змінювати категорії!';
$_['error_name']             = 'Назва категорії повинна бути більше 2 і менше 255 символів!';
$_['error_seo_url']          = 'Цей SEO URL вже використовується!';
$_['error_article']          = 'Ця категорія не може бути видалена, так як в ній є статті %s!';