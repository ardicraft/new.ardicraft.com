<?php

// Text
$_['text_main_page'] = 'Головна сторінка';
$_['text_category_page'] = 'Сторінка категорії';
$_['text_product_page'] = 'Карточка товару';
$_['text_checkout_page'] = 'Оформлення замовлення';
$_['text_account_page'] = 'Особистий кабінет';
$_['text_admin_home_page'] = 'Адміністративна панель';
$_['text_integration_services'] = 'Інтеграція з зовнішніми сервісами';
$_['text_adding_products'] = 'Наповнення магазину товарами';
$_['text_technical_modules'] = 'Технічні модулі';
$_['text_advancement'] = 'Для просування';
$_['text_improvement'] = 'Поліпшення';
