<?php
$_['heading_title'] = 'Когда сделан последний бэкап?';

//Text
$_['text_no_information'] = 'Информация отсутствует. Требуется включить модуль <a href="https://neoseo.com.ua/uk/rezervnye-kopii" target="_blank">"NeoSeo Резервные копии"</a>';
$_['text_no_install'] = 'Информация отсутствует. Требуется установить модуль <a href="https://neoseo.com.ua/uk/rezervnye-kopii" target="_blank">"NeoSeo Резервные копии"</a>';
$_['text_buy'] = 'Купить модуль';
$_['text_error'] = 'Домен и почта обязательны для заполнения';
$_['text_warning'] = 'С момента последнего создания резервной копии прошло %s дней.';
$_['text_success'] = 'Ваша заявка на info@neoseo.com.ua успешно отправлена.';
$_['text_now'] = 'Сегодня';

//Buttons
$_['button_request'] = 'Оставить заявку';
$_['button_notify'] = 'Отправить сообщение';

//Params
$_['params_email'] = 'info@neoseo.com.ua';
$_['params_subject'] = 'Новая заявка на покупку модуля "NeoSeo Резервные копии"';
$_['params_message'] = 'Домен: %s. Почта: %s.. Модуль: "NeoSeo Резервные копии"';
$_['params_monday'] = 'Понедельник';
$_['params_tuesday'] = 'Вторник';
$_['params_wednesday'] = 'Среда';
$_['params_thursday'] = 'Четверг';
$_['params_friday'] = 'Пятница';
$_['params_saturday'] = 'Суббота';
$_['params_sunday'] = 'Воскресенье';
$_['params_link'] = 'https://neoseo.com.ua/uk/rezervnye-kopii';