<?php
// Heading
$_['heading_title']        = 'Адміністрування';

// Text
$_['text_order']           = 'Замовлення';
$_['text_order_status']    = 'Статуси замовлень';
$_['text_complete_status'] = 'Завершено';
$_['text_customer']        = 'Клієнти';
$_['text_online']          = 'Клієнти Online';
$_['text_approval']        = 'Чекають схвалення';
$_['text_product']         = 'Товари';
$_['text_stock']           = 'Немає в наявності';
$_['text_review']          = 'Відгуки';
$_['text_return']          = 'Повернення';
$_['text_store']           = 'Магазин';
$_['text_front']           = 'Магазин';
$_['text_help']            = 'Допомога';
$_['text_homepage']        = 'Сайт ';
$_['text_support']         = 'Форум';
$_['text_documentation']   = 'Документація';
$_['text_logout']          = 'Вихід';

// Text mobile
$_['text_currency_mobile']      = 'Валюта:';
$_['text_wishlist_mobile']      = 'Обране';
$_['text_compare_mobile']       = 'Порівняння';
$_['text_cart_mobile']          = 'Кошик';

$_['button_delete_demo_data']       ='Удалить демо данные';
$_['text_demo_data_delete']         ='Будут удалены все демо-данные из магазина, такие как товары, категории, опции фильтра, посадочные страницы. Даное действие невозможно отменить или вернуть';
$_['text_yes'] = 'Да';
$_['text_no'] = 'Нет';
$_['text_mute'] = 'Больше не напоминать';
$_['text_confirm_delete'] = 'Вы уверены, Данное действие не обратимо!!!';


