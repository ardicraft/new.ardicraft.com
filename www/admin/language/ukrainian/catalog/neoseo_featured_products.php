<?php

//Tabs
$_['tab_module_related_tabs'] = 'Рекомендуемые товары';

// Text
$_['text_success_delete'] = 'Таб успешно удален!';
$_['text_related_confirm'] = 'Вы уверены?';
$_['text_template'] = 'Шаблон';
$_['text_warning'] = 'Необходимо добавить модуль в Дизайн -> Схемы -> Продукт!';

// Entry
$_['entry_name'] = 'Название';
$_['entry_product'] = 'Сопутствующие товары';
$_['entry_limit'] = 'Лимит';
$_['entry_width'] = 'Ширина';
$_['entry_height'] = 'Высота';
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_title'] = 'Заголовок';
$_['entry_title_tab'] = 'Название таба';
$_['entry_products'] = 'Товары';
$_['entry_order'] = 'Порядок';
$_['entry_template'] = 'Шаблон';
