<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Архивный товар</p>';
$_['heading_title_raw'] = 'NeoSeo Архивный товар';

//Tab
$_['tab_general'] = 'Общие';
$_['tab_support'] = 'Поддержка';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';

//Button
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';

// Text
$_['text_module_version'] = '';
$_['text_edit'] = 'Параметры';
$_['text_success'] = 'Настройки модуля успешно обновлены!';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_module'] = 'Модули';

// Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Отладочный режим:';
$_['entry_stock_statuses'] = 'Отображаемые статусы';
$_['entry_stock_statuses_desc'] = 'Архивными будут считаться те товары, у которых статус отмечен галочкой в этом списке.';
$_['entry_image_width'] = 'Ширина изображения';
$_['entry_image_height'] = 'Высота изображения';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Резервные копии"!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';

$_['mail_support'] = '';
$_['module_licence'] = '';
