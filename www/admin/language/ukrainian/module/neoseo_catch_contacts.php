<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Захват контактов';
$_['heading_title_raw'] = 'NeoSeo Захват контактов';

//Tabs
$_['tab_general'] = 'Общие';
$_['tab_subscribe'] = 'Подписка';
$_['tab_logs'] = 'Логи';
$_['tab_support'] = 'Поддержка';
$_['tab_license'] = 'Лицензия';
$_['tab_usefull'] = 'Полезные ссылки';

//Buttons
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';
$_['button_clear_log'] = 'Очистить логи';
$_['button_download_log'] = 'Скачать файл логов';

// Text
$_['text_module_version'] = '';
$_['text_module'] = 'Модули';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_edit'] = 'Редактирование модуля';
$_['text_success_clear'] = 'Логи успешно удалены';
$_['text_success_delete'] = 'Таб успешно удален!';
$_['text_title'] = 'ЗАБРОНИРУЙТЕ СКИДКУ ПРЯМО СЕЙЧАС!';
$_['text_title_form'] = 'Оставьте заявку и мы свяжемся с Вами.';
$_['text_type_subscription'] = 'Заказать скидку';
$_['text_position_top'] = 'Вверху';
$_['text_position_bottom'] = 'Внизу';
$_['text_position_left'] = 'Слева';
$_['text_position_right'] = 'Справа';

// Entry
$_['entry_name'] = 'Название';
$_['entry_status'] = 'Статус';
$_['entry_debug'] = 'Отладочный режим';
$_['entry_title'] = 'Заголовок';
$_['entry_title_form'] = 'Заголовок формы';
$_['entry_background'] = 'Фон';
$_['entry_type_subscription'] = 'Тип заявки';
$_['entry_type_subscription_desc'] = 'Укажите тип заявки, например: "Заказ скидки"';
$_['entry_admin_subscribe_notify'] = 'Получатели сообщения о заявке, через запятую';
$_['entry_position_form'] = 'Положение формы';
$_['entry_phone_mask'] = 'Маска ввода для телефона';
$_['entry_phone_mask_desc'] = 'Укажите маску ввода чтобы минимизировать ошибки';
$_['entry_subscribe_text'] = 'Текст сообщения, в случае успешной подписки';
$_['entry_subscribe_text_desc'] = 'Допускается использование {id}, {name}, {email}, {phone}, {type_subscribe}';
$_['entry_subscribe_subject'] = 'Тема письма';
$_['entry_subscribe_subject_desc'] = 'Допускается использование {id}, {name}, {email}, {phone}, {type_subscribe}';
$_['entry_subscribe_message'] = 'Текст сообщения';
$_['entry_subscribe_message_desc'] = 'Допускается использование {id}, {name}, {email}, {phone}, {type_subscribe}';
$_['entry_admin_subscribe'] = 'Получатели сообщения о заявке, через запятую';
$_['entry_admin_subscribe_subject'] = 'Тема письма админу';
$_['entry_admin_subscribe_subject_desc'] = 'Допускается использование {id}, {name}, {email}, {phone}, {type_subscribe}';
$_['entry_admin_subscribe_message'] = 'Текст сообщения админу';
$_['entry_admin_subscribe_message_desc'] = 'Допускается использование {id}, {name}, {email}, {phone}, {type_subscribe}';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

//Params
$_['param_subscribe_text'] = "<p>Уважаемый, {name}!</p><p>Ваша заявка №{id} принята.</p><p>В ближайшее время наш менеджер свяжется с Вами!</p>";
$_['param_subscribe_subject'] = "Заявка №{id} принята";
$_['param_subscribe_message'] = "Здравствуйте, {name}!<br>В ближайшее время наш менеджер свяжется с Вами!";
$_['param_admin_subscribe_subject'] = "Поступила заявка №{id} на {type_subscribe}";
$_['param_admin_subscribe_message'] = "Посетитель <a href='mailto:{email}'>{name}</a> оставил заявку №{id} на {type_subscribe}";

// Help
$_['help_product'] = '(Автодополнение)';

// Error
$_['error_permission'] = 'У Вас недостаточно прав для изменения "NeoSeo Рекомендуемые продукты"!';
$_['error_download_logs'] = 'Файл логов пустой или отсутствует!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';

$_['mail_support'] = '';
$_['module_licence'] = '';

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-zahvat-kontaktov">https://neoseo.com.ua/nastroyka-modulya-neoseo-zahvat-kontaktov</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/sbor-dannyh-klientov#module_history">https://neoseo.com.ua/sbor-dannyh-klientov#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/sbor-dannyh-klientov#faqBox">https://neoseo.com.ua/sbor-dannyh-klientov#faqBox</a>';