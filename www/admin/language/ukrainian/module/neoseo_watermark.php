<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Водяной знак</span>';
$_['heading_title_raw'] = 'NeoSeo Водяной знак';
$_['new_title'] = 'Водяной знак';
$_['text_module_version'] = '';

$_['tab_general'] = 'Общее';
$_['tab_params'] = 'Параметры';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Лицензия';
$_['tab_support'] = 'Поддержка';
$_['tab_addition_params'] = 'Дополнительные параметры';
$_['tab_usefull'] = 'Полезные ссылки';

// Button
$_['button_clear_log'] = 'Очистить лог';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';
$_['button_recheck'] = 'Проверить еще раз';

// Text
$_['text_module'] = 'Модули';
$_['text_edit'] = 'Настройка параметров водного знака';
$_['text_success_options'] = 'Настройки модуля обновлены!';
$_['text_success_clear'] = 'Лог файл успешно очищен!';
$_['text_clear_log'] = 'Очистить лог';
$_['text_success_clean'] = 'Кеш изображений очищен!';
$_['text_browse'] = 'Выбрать';
$_['text_clear'] = 'Очистить';
$_['text_image_manager'] = 'Менеджер изображений';
$_['button_clean'] = 'Очистить кеш изображений';
$_['button_save'] = 'Сохранить';
$_['button_save_and_close'] = 'Сохранить и Закрыть';
$_['button_close'] = 'Закрыть';


// Entry
$_['entry_transparent'] = 'Прозрачный фон PNG изображений:';
$_['entry_status'] = '<label class="control-label">Статус:</label>';
$_['entry_hide_real_path'] = '<label class="control-label">Скрывать путь к изображению в папке image:</label>';
$_['entry_image'] = '<label class="control-label">Водяной знак:</label>';
$_['entry_exclude'] = '<label class="control-label">Исключить папки:</label><br><span class="help">Не применять водяной знак для изображений, которые находятся в папках. </span>';
$_['entry_min_size'] = '<label class="control-label">Минимальный размер изображения:</label><br><span class="help">Мин. размер изображения для применения водяного знака. </span>';
$_['entry_max_size'] = '<label class="control-label">Максимальный размер изображения:</label><br><span class="help">Макс. размер изображения для применения водяного знака. </span>';
$_['entry_debug'] = '<label class="control-label">Отладка:</label>';
$_['entry_header_clear_cache'] = '<label class="control-label">Кнопка очистки кеша в хедере</label>';
$_['entry_instruction'] = 'Инструкция к модулю:';
$_['entry_history'] = 'История изменений:';
$_['entry_faq'] = 'Часто задаваемые вопросы:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_image'] = 'Image width &amp; height dimensions required!';
$_['error_ioncube_missing'] = "";
$_['error_license_missing'] = "";

$_['mail_support'] = "";
$_['module_licence'] = "";

//links
$_['instruction_link'] = '<a target="_blank" href="https://neoseo.com.ua/nastroyka-modulya-neoseo-vodyanoy-znak">https://neoseo.com.ua/nastroyka-modulya-neoseo-vodyanoy-znak</a>';
$_['history_link'] = '<a target="_blank" href="https://neoseo.com.ua/vodyanoj-znak#module_history">https://neoseo.com.ua/vodyanoj-znak#module_history</a>';
$_['faq_link'] = '<a target="_blank" href="https://neoseo.com.ua/vodyanoj-znak#faqBox">https://neoseo.com.ua/vodyanoj-znak#faqBox</a>';
