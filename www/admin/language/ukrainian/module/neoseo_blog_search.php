<?php
// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><p style="margin:0;line-height: 24px;">NeoSeo Блог Пошук';
$_['heading_title_raw'] = 'NeoSeo Блог Пошук';

// Text
$_['text_module']      = 'Модулі';
$_['text_success']     = 'Модуль Блог Пошук успішно змінений!';
$_['text_edit']        = 'Редагування модуля Блог Пошук';
$_['text_module_version'] = '';

// Entry
$_['entry_name']       = 'Назва модуля';
$_['entry_title']      = 'Заголовок модуля';
$_['entry_blog_category'] = 'Кореневий каталог';
$_['entry_template']   = 'Шаблон';
$_['entry_root_category'] = 'Обмежити категорією:';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас немає прав змінювати модуль Блог Пощук!';