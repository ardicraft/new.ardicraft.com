﻿<?php

// Heading
$_['heading_title'] = '<img width="24" height="24" src="view/image/neoseo.png" style="float: left;"><span style="margin:0;line-height: 24px;">NeoSeo Биті посилання ( HTTP 404 )</span>';
$_['heading_title_raw'] = 'NeoSeo Биті посилання ( HTTP 404 )';

//Tab
$_['tab_general'] = 'Параметри';
$_['tab_logs'] = 'Логи';
$_['tab_license'] = 'Ліцензія';
$_['tab_support'] = 'Підтримка';

// Text
$_['text_module_version'] = '';
$_['text_description'] = 'Модуль призначений для перегляду помилок 404';
$_['text_clear_log'] = 'Очистити лог';
$_['text_success'] = 'Налаштування модуля оновлені!';
$_['text_success_options'] = 'Налаштування модуля оновлені!';
$_['text_success_clear'] = 'Лог файл успішно очищений!';
$_['text_module'] = 'Модулі';

//Entry
$_['entry_status'] = 'Статус:';
$_['entry_debug'] = 'Налагодження:';

//Button
$_['button_save'] = 'Зберегти';
$_['button_save_and_close'] = 'Зберегти і Закрити';
$_['button_close'] = 'Закрити';
$_['button_recheck'] = 'Перевірити ще раз';
$_['button_clear_log'] = 'Очистити лог';

// Error
$_['error_permission'] = 'У Вас немає прав для управління цим модулем!';
$_['error_ioncube_missing'] = '';
$_['error_license_missing'] = '';
$_['error_permission'] = 'У Вас немає прав для управління цим модулем!';


$_['mail_support'] = '';
$_['module_licence'] = '';
