﻿<?php

// Heading
$_['heading_title'] = 'NeoSeo Биті посилання ( HTTP 404 ) ';

// Text
$_['text_module_version'] = '';
$_['text_neoseo_broken_links'] = 'Биті посилання';
$_['text_success'] = 'Налаштування модуля оновлені!';
$_['text_setting_success'] = 'Налаштування модуля оновлені!';
$_['text_module'] = 'Модулі';
$_['text_list'] = 'Список посилань';
$_['text_none'] = '---';
$_['text_toggle_columns'] = 'Колонки';
$_['text_table_search'] = 'Фільтр:';
$_['text_table_results_per_page'] = 'Записів на сторінку: _MENU_';
$_['text_table_info'] = 'Записи з _START_ по _END_ із _TOTAL_';
$_['text_table_info_empty'] = 'Записи з 0 по 0';
$_['text_table_info_filtered'] = 'вибрано із _MAX_ записів';
$_['text_table_zero_records'] = 'Дані по запите не знайдені';
$_['text_table_empty'] = 'Немає даних';
$_['text_table_processing'] = 'Виконується обробка...';
$_['text_table_loading'] = 'Виконується загрузка даних...';
$_['text_table_first'] = '|<';
$_['text_table_prev'] = '<';
$_['text_table_next'] = '>';
$_['text_table_last'] = '>|';
$_['text_indicator_saving'] = "<img src='view/image/loading.gif' />";


// Entry
$_['entry_date'] = 'Дата';
$_['entry_ip'] = 'IP';
$_['entry_browser'] = 'Browser';
$_['entry_referer'] = 'Referer';
$_['entry_request_uri'] = 'Запитувана сторінка';

// Button
$_['button_clear'] = 'Скидання';
$_['button_filter'] = 'Фільтр';
// Column
$_['column_date'] = 'Дата';
$_['column_ip'] = 'IP';
$_['column_browser'] = 'Browser';
$_['column_referer'] = 'Referer';
$_['column_request_uri'] = 'Запитувана сторінка';

// Error
$_['error_permission'] = 'Помилка: У вас немає прав на зміну даних модуля!';
