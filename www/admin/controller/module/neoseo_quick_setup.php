<?php

require_once( DIR_SYSTEM . '/engine/neoseo_controller.php');
require_once( DIR_SYSTEM . '/engine/neoseo_view.php' );

class ControllerModuleNeoSeoQuickSetup extends NeoSeoController
{

	private $error = array();
	public function __construct($registry)
	{
		parent::__construct($registry);
		$this->_moduleSysName = "neoseo_quick_setup";
		$this->_modulePostfix = ""; // Постфикс для разных типов модуля, поэтому переходим на испольлзование $this->_moduleSysName()
		$this->_logFile = $this->_moduleSysName() . ".log";
		$this->debug = $this->config->get($this->_moduleSysName() . "_debug") == 1;
	}

	public function index()
	{
		$this->checkLicense();
		$this->upgrade();
		$this->load->model('tool/'.$this->_moduleSysName());

		$data = $this->language->load($this->_route . '/' . $this->_moduleSysName());

		$this->document->setTitle($this->language->get('heading_title_raw'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {

			$this->model_setting_setting->editSetting($this->_moduleSysName(), $this->request->post);
			$slides = $this->{'model_tool_'.$this->_moduleSysName()}->saveVars($this->request->post);
			$this->{'model_tool_'.$this->_moduleSysName()}->setParam('neoseo_quick_setup_complete',1);
			$this->{'model_tool_'.$this->_moduleSysName()}->saveUnistor();

			$this->session->data['success'] = $this->language->get('text_success');

			if ($this->request->post['action'] == "save") {
				$this->response->redirect($this->url->link($this->_route . '/' . $this->_moduleSysName(), 'token=' . $this->session->data['token'], 'SSL'));
			} else if($this->request->post['action'] == "close") {
				$this->response->redirect($this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'));
			} else {
				$this->response->redirect($this->url->link('extension/' . $this->_route, 'token=' . $this->session->data['token'], 'SSL'));
			}
		}
		if(isset($this->request->get['slider'] )){
			$useSlider = true;
		} else {
			$useSlider = false;
		}
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else if (isset($this->session->data['error_warning'])) {
			$data['error_warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}

		$data = $this->initBreadcrumbs(array(
			array('extension/' . $this->_route, 'text_module'),
			array($this->_route . '/' . $this->_moduleSysName(), "heading_title_raw")
			), $data);


		$data = $this->initButtons($data);

		if(isset($this->request->get['store_id'])){
			$store_id = $this->request->get['store_id'];
		} else {
			$store_id = 0;
		}
		$multistore_options = $this->{'model_module_'.$this->_moduleSysName()}->getMultistoreOptions();
		$this->load->model($this->_route . "/" . $this->_moduleSysName());
		$data = $this->initParamsListEx($this->{"model_" . $this->_route . "_" . $this->_moduleSysName()}->params, $data);
		$data['params_arr'] = $this->{"model_" . $this->_route . "_" . $this->_moduleSysName()}->params;
		foreach ($data['params_arr'] as $a_name => $attr){
			if(in_array($a_name,array('status','debug'))) continue;
			//echo "$a_name <hr>".print_r($this->{'model_tool_'.$this->_moduleSysName()}->getParam($a_name),true)."<hr>";
			$data[$this->_moduleSysName.'_'.$a_name] = $this->{'model_tool_'.$this->_moduleSysName()}->getParam($a_name);
			if(in_array($a_name, $multistore_options )){
				$data[$this->_moduleSysName.'_'.$a_name] = $data[$this->_moduleSysName.'_'.$a_name][$store_id];
			}
		}

		$config_langdata_db = $this->{'model_tool_'.$this->_moduleSysName()}->getParam('config_langdata');
		foreach ($config_langdata_db as $l => $l_data){
			foreach ($l_data as $p_name => $p_val){
				$data[$this->_moduleSysName().'_config_'.$p_name][$l] = $p_val;
			}
		}
		//echo "<pre>";print_r($data[$this->_moduleSysName."_".'config_meta_title']);exit;
		//echo "<pre>";print_r($data);exit;

		$data["token"] = $this->session->data['token'];
		$data['config_language_id'] = $this->config->get('config_language_id');
		$data['params'] = $data;

		$data["logs"] = $this->getLogs();

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$languages = $data['languages'] ;
		// Языки и валюты для 0 шага
		$data[$this->_moduleSysName()."_need_languages"] = array();
		$data[$this->_moduleSysName()."_need_currencies"] = array();

		$this->language->load('module/neoseo_unistor');
		$data['schemes'] = array (
			'default' => $this->language->get('text_default'),
			'yellow' => $this->language->get('text_scheme_1'),
			'red' => $this->language->get('text_scheme_2'),
			'orange' => $this->language->get('text_scheme_3'),
			'green' => $this->language->get('text_scheme_4'),
			'purple' => $this->language->get('text_scheme_5'),
			'indigo' => $this->language->get('text_scheme_6'),
			'blue' => $this->language->get('text_scheme_7'),
			'black' => $this->language->get('text_scheme_8'),
		);

		$data['maps'] = array(
			'none' => $this->language->get('text_disabled'),
			'google' => $this->language->get('text_google'),
			'yandex' => $this->language->get('text_yandex'),
		);

		$data['menu_types'] = array( 0 => $this->language->get('text_horizontal'), 1 => $this->language->get('text_vertical'));

		$this->load->model('tool/image');
		foreach ($languages as $language) {
			$data['languages_c'][$language['code']] = $language['name'];
			$data[$this->_moduleSysName()."_need_languages"][] = $language['code'];
			if (file_exists(DIR_IMAGE . $data[$this->_moduleSysName . '_neoseo_unistor_logo'][$language['language_id']]) && is_file(DIR_IMAGE . $data[$this->_moduleSysName . '_neoseo_unistor_logo'][$language['language_id']])) {
				$data[$this->_moduleSysName . '_neoseo_unistor_logo_img'][$language['language_id']] = $this->model_tool_image->resize($data[$this->_moduleSysName . '_neoseo_unistor_logo'][$language['language_id']], 100, 100);
			} else {
				$data[$this->_moduleSysName . '_neoseo_unistor_logo_img'][$language['language_id']] = $this->model_tool_image->resize('no_image.png', 100, 100);
			}
		}

		if (file_exists(DIR_IMAGE . $data[$this->_moduleSysName . '_config_icon']) && is_file(DIR_IMAGE . $data[$this->_moduleSysName . '_config_icon'])) {
			$data[$this->_moduleSysName . '_config_icon_img'] = $this->model_tool_image->resize($data[$this->_moduleSysName . '_config_icon'], 100, 100);
		} else {
			$data[$this->_moduleSysName . '_config_icon_img'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		// Slides
		$slides = $this->{'model_tool_'.$this->_moduleSysName()}->getSlides();
		$data['big_slides'] = array();//$slides['big_banners'];
		$data['small_slides'] = array();//$slides['small_banners'];
		foreach ($slides['big_banners'] as $bid){
			$bi = array_shift($bid['banner_image_description']);
			//echo "<pre>";print_r($bi);exit;
			if (file_exists(DIR_IMAGE .  $bi['image']) && is_file(DIR_IMAGE .  $bi['image'])) {
				$th = $this->model_tool_image->resize( $bi['image'], 150, 100);
			} else {
				$th = $this->model_tool_image->resize('no_image.png', 100, 100);
			}
			$data['big_slides'][] = array(
				'image' =>  $bi['image'],
				'thumb' => $th,
			);
		}

		foreach ($slides['small_banners'] as $bid){
			$bi = array_shift($bid['banner_image_description']);
			if (file_exists(DIR_IMAGE .  $bi['image']) && is_file(DIR_IMAGE .  $bi['image'])) {
				$th = $this->model_tool_image->resize( $bi['image'], 150, 100);
			} else {
				$th = $this->model_tool_image->resize('no_image.png', 100, 100);
			}
			$data['small_slides'][] = array(
				'image' =>  $bi['image'],
				'thumb' => $th,
			);
		}
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		$data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		//echo "<pre>";print_r($data[$this->_moduleSysName().'_big_slides']);exit;
		$this->load->model('localisation/currency');
		foreach ($this->model_localisation_currency->getCurrencies() as $c){
			$data['currencies'][$c['code']] = $c['title'];
			$data[$this->_moduleSysName()."_need_currencies"][] = $c['code'];
		}
		$data['ml_update_link'] = html_entity_decode($this->url->link($this->_route . '/' . $this->_moduleSysName().'/updateMl', 'token=' . $this->session->data['token'], 'SSL'));

		$this->load->model('localisation/country');
		$data['countries'] = array(0 => $this->language->get('text_select'));
		$countries = $this->model_localisation_country->getCountries();
		foreach ($countries as $country) {
			$data['countries'][$country['country_id']] = $country['name'];
		}
		$this->load->model('localisation/zone');
		$data['zones'] = array(0 => $this->language->get('text_select'));
		foreach ($this->model_localisation_zone->getZones() as $zone) {
			$data['zones'][$zone['zone_id']] = $zone['name'];
		}

		$data['general_styles'] = array( 0 => $this->language->get('text_style_flat'), 1 => $this->language->get('text_style_3d'));

		$data['ckeditor'] = $this->config->get('config_editor_default');
		if ($this->config->get('config_editor_default')) {
			$this->document->addScript('view/javascript/ckeditor/ckeditor.js');
			$this->document->addScript('view/javascript/ckeditor/ckeditor_init_qs.js');
			$this->document->addScript('view/javascript/ckeditor/ckeditor_init_qs.js');
		}
		//echo "<pre>";print_r($data);exit;
		$widgets = new NeoSeoWidgets($this->_moduleSysName() . '_', $data);
		$widgets->text_select_all = $this->language->get('text_select_all');
		$widgets->text_unselect_all = $this->language->get('text_unselect_all');
		$data['widgets'] = $widgets;

		$data['start_page'] = 0;
		if(isset($this->session->data['MlUpdated']) && $this->session->data['MlUpdated'] === true ){
			$data['start_page'] = 1;
			$this->session->data['MlUpdated'] = false;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($this->_route . '/' . $this->_moduleSysName() .($useSlider?'_slider':''). '.tpl', $data));
	}

	private function validate()
	{
		if (!$this->user->hasPermission('modify', $this->_route . '/' . $this->_moduleSysName())) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function updateMl()
	{
		$this->session->data['MlUpdated'] = true;
		$data = $this->request->post;
		//echo "<pre>";print_r($data);exit;
		$this->load->model('tool/'.$this->_moduleSysName());
		$this->{'model_tool_'.$this->_moduleSysName()}->clearNeedlessData($data);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode(array('msg' => 'ok')));
	}
}
