<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <?php if( $category_id ) { ?>
        <a href="<?php echo HTTP_CATALOG; ?>index.php?route=product/category&path=<?php echo $category_id; ?>" target="_new"  data-toggle="tooltip" title="" class="btn btn-success" data-original-title="Перейти к категории на витрине"><i class="fa fa-eye"></i></a>
        <?php } ?>
          <button type="button" onclick="$('#form-category').attr('action', $('#form-category').attr('action') + '&action=save_and_stay');$('#form-category').submit();" data-toggle="tooltip" title="<?php echo $button_save_and_stay; ?>" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;<i class="fa fa-refresh"></i></button>
          <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
	    <!-- NeoSeo Featured Products - begin -->
        <li><a href="#tab-module-related-tabs" data-toggle="tab"><?php echo $tab_module_related_tabs; ?></a></li>
        <!-- NeoSeo Featured Products - end -->
            <li><a href="#tab-design" data-toggle="tab"><?php echo $tab_design; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="category_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="category_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="category_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-h1<?php echo $language['language_id']; ?>"><?php echo $entry_meta_h1; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="category_description[<?php echo $language['language_id']; ?>][meta_h1]" value="<?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_h1'] : ''; ?>" placeholder="<?php echo $entry_meta_h1; ?>" id="input-meta-h1<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="category_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="category_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($category_description[$language['language_id']]) ? $category_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="tab-pane" id="tab-data">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-parent"><?php echo $entry_parent; ?></label>
                <div class="col-sm-10">
                  <select name="parent_id">
                    <option value="0" selected="selected"><?php echo $text_none; ?></option>
                    <?php foreach ($categories as $category) { ?>
                    <?php if ($category['category_id'] == $parent_id) { ?>
                    <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                  <div id="category-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($category_filters as $category_filter) { ?>
                    <div id="category-filter<?php echo $category_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $category_filter['name']; ?>
                      <input type="hidden" name="category_filter[]" value="<?php echo $category_filter['filter_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $category_store)) { ?>
                        <input type="checkbox" name="category_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="category_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $category_store)) { ?>
                        <input type="checkbox" name="category_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="category_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
              <!-- NeoSeo Blog - begin -->
				<label class="col-sm-2 control-label" for="input-category-to-blog-article"><?php echo $entry_category_to_blog_article; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="blog_article" value="" placeholder="<?php echo $text_article; ?>" id="input-category-to-blog-article" class="form-control" />
				  <div id="category-to-blog-article" class="well well-sm" style="height: 150px; overflow: auto;">
					<?php foreach ($category_to_blog_article as $article) { ?>
					<div id="category-to-blog-article<?php echo $article['article_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $article['name']; ?>
					  <input type="hidden" name="articles_to_blog[]" value="<?php echo $article['article_id']; ?>" />
					</div>
					<?php } ?>
				  </div>
				</div>
			  </div>
			  <!-- NeoSeo Blog - end -->
			  <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>                
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_image; ?></label>
                <div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-top"><span data-toggle="tooltip" title="<?php echo $help_top; ?>"><?php echo $entry_top; ?></span></label>
                <div class="col-sm-10">
                  <div class="checkbox">
                    <label>
                      <?php if ($top) { ?>
                      <input type="checkbox" name="top" value="1" checked="checked" id="input-top" />
                      <?php } else { ?>
                      <input type="checkbox" name="top" value="1" id="input-top" />
                      <?php } ?>
                      &nbsp; </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-column"><span data-toggle="tooltip" title="<?php echo $help_column; ?>"><?php echo $entry_column; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="column" value="<?php echo $column; ?>" placeholder="<?php echo $entry_column; ?>" id="input-column" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <!-- NeoSeo Product Archive - begin -->
                <?php if ($status_archive_product) { ?>
                <label class="col-sm-2 control-label"><?php echo $text_product_similar_category; ?></label>
                <div class="col-sm-10">
                  <select name="product_similar_category" class="form-control">
                    <option value="0" selected="selected"><?php echo $text_none; ?></option>
                    <?php foreach ($categories as $category) { ?>
                    <?php if ($category['category_id'] == $product_similar_category) { ?>
                    <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
          </div>
        <div class="form-group">
        <?php } ?>
                <!-- NeoSeo Product Archive  - end -->
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
                    <?php if ($status) { ?>
                    <!-- NeoSeo Product Archive - begin -->
                    <?php if ($status==1) { ?>
                    <!-- NeoSeo Product Archive  - end -->
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>

                    <!-- NeoSeo Product Archive - begin -->
                    <?php if ($status_archive_product) { ?>
                    <option value="2"><?php echo $text_archive; ?></option>
                    <?php } ?>
                    <?php } ?>
                    <?php if ((int)$status==2) { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <?php if ($status_archive_product) { ?>
                    <option value="2"  selected="selected"><?php echo $text_archive; ?></option>
                    <?php } ?>
                    <?php } ?>
                    <!-- NeoSeo Product Archive  - end -->
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <!-- NeoSeo Product Archive - begin -->
                    <option value="2"><?php echo $text_archive; ?></option>
                      <!-- NeoSeo Product Archive  - end -->
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
	    <!-- NeoSeo Featured Products - begin -->
        <div class="tab-pane" id="tab-module-related-tabs">
        <?php if(count($info_modules)>0) { ?>
            <ul class="nav nav-tabs">
            <?php $first = true; foreach ($info_modules as $module) { ?>
                <li <?php if($first) { $first = false; echo 'class="active"'; }?>>
                    <a data-toggle="tab" href="#tab-layout-module-<?php echo $module['module_id']; ?>">
                    <?php echo $module['name']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
            <div class="tab-content">
            <?php $first = true; foreach ($info_modules as $module) { ?>
                <div class="tab-pane <?php if($first) { $first = false; echo 'active'; }?>" id="tab-layout-module-<?php echo $module['module_id'];?>">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <td class="text-center"><?php echo $entry_related_title;?></td>
                            <td class="text-center"><?php echo $entry_related_template;?></td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr id="attribute-row0">
                        <td class="text-center" >
                        <?php foreach ($languages as $language) {  ?>
                        <div class="input-group">
                            <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                            <input class="form-control" name="setting_module[<?php echo $module['module_id'] ?>][name][<?php echo $language['language_id']; ?>]" value="<?php echo (isset($product_module_setting[$module['module_id']]['name'][$language['language_id']])) ? $product_module_setting[$module['module_id']]['name'][$language['language_id']] : ''; ?>"/>
                        </div>
                        <?php } ?>
                        </td>
                        <td class="text-left">
                            <select class="form-control" name="setting_module[<?php echo $module['module_id'] ?>][template]">
                            <?php foreach ($related_templates as $key_template => $template) { ?>
                            <?php if (isset($product_module_setting[$module['module_id']]['template']) && $key_template == $product_module_setting[$module['module_id']]['template']) { ?>
                            <option value="<?php echo $key_template; ?>" selected="selected"><?php echo $template; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $key_template; ?>"><?php echo $template; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                        </td>
                    </table>

                    <div class="col-xs-4 col-md-3 col-lg-3 related_tabs-left">
                    <ul class="nav nav-pills nav-stacked" id="custom-related_tabs-header">
                        <?php if(isset($related_tabs[$module['module_id']])){ ?>
                        <?php $first = true; foreach ($related_tabs[$module['module_id']] as $tab) { ?>
                        <li <?php if($first) { $first = false; echo 'class="active"'; }?>>
                            <a data-toggle="tab" href="#tab-label-<?php echo $tab['tab_id']; ?>">
                            <?php echo $tab['name'][$language_id]; ?>
                                <button type="button" class="btn btn-xs btn-link delete-tab" rel="tooltip"
                                    data-original-title="Delete this label" data-container="body"
                                    onclick="deleteTab( <?php echo $tab['tab_id'] ?> )">
                                    <i class="fa fa-trash text-danger"></i>
                                </button>
                            </a>
                        </li>
                        <?php } ?>
                        <?php } ?>
                        <li>
                            <div class="input-group">
                                <input type="text" class="form-control" id="new-tab-<?php echo $module['module_id'];?>" placeholder="New tab name" data-bind="value: newTabName">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-default add-new-label" rel="tooltip"
                                        data-original-title="Add a new label" data-container="body" data-module="<?php echo $module['module_id']; ?>">
                                        <i class="fa fa-plus-circle text-success"></i>
                                    </button>
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col-xs-8 col-md-9 col-lg-9">
                    <div class="tab-content">
                        <?php if(isset($related_tabs[$module['module_id']])){ ?>
                        <?php $first = true; foreach ($related_tabs[$module['module_id']] as $tab) { ?>
                        <?php if (!isset($related_related_max_id)) $related_related_max_id = $tab['tab_id']; ?>
                        <div class="tab-pane <?php if($first) { $first = false; echo 'active'; }?>" id="tab-label-<?php echo $tab['tab_id']; ?>">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-status"><?php echo $entry_related_status; ?></label>
                                <div class="col-sm-9">
                                    <select name="related_tabs[<?php echo $tab['tab_id'];?>][status]" id="input-status" class="form-control">
                                    <?php if ($tab['status']) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                    <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                    <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" ><?php echo $entry_related_title_tab;?></label>
                                <div class="col-sm-9">
                                    <?php foreach ($languages as $language) {  ?>
                                    <div class="input-group">
                                        <span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                        <input class="form-control" name="related_tabs[<?php echo $tab['tab_id']; ?>][name][<?php echo $language['language_id']; ?>]" value="<?php echo (isset($related_tabs[$module['module_id']][$tab['tab_id']]['name'][$language['language_id']])) ? $related_tabs[$module['module_id']][$tab['tab_id']]['name'][$language['language_id']] : ''; ?>"/>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="col-sm-3 control-label" for="input-status"><?php echo $entry_related_limit; ?></label>
                                <div class="col-sm-9">
                                    <input type="text" name="related_tabs[<?php echo $tab['tab_id'];?>][limit]" value="<?php echo (isset($tab['limit'])) ? $tab['limit'] : ''; ?>" class="form-control" />
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-status"><?php echo $entry_related_width; ?></label>
                                <div class="col-sm-9">
                                <input type="text" name="related_tabs[<?php echo $tab['tab_id'];?>][width]" value="<?php echo (isset($tab['width'])) ? $tab['width'] : ''; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-status"><?php echo $entry_related_height; ?></label>
                                <div class="col-sm-9">
                                    <input type="text" name="related_tabs[<?php echo $tab['tab_id'];?>][height]" value="<?php echo (isset($tab['height'])) ? $tab['height'] : ''; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-status"><?php echo $entry_related_order; ?></label>
                                <div class="col-sm-9">
                                    <input type="text" name="related_tabs[<?php echo $tab['tab_id'];?>][order]" value="<?php echo (isset($tab['order'])) ? $tab['order'] : ''; ?>" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group" id="show_products">
                                <label class="col-sm-3 control-label" for="input-product"><?php echo $entry_related_products; ?></label>
                                <div class="col-sm-9">
                                    <input type="text" name="tab_product" value="" placeholder="<?php echo $entry_related_products; ?>" id="<?php echo $tab['tab_id'];?>" class="form-control" />
                                    <div id="label-hand-product-<?php echo $tab['tab_id']?>" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php if(isset($related_tabs[$module['module_id']][$tab['tab_id']]['prod'])){ ?>
                                        <?php foreach ($related_tabs[$module['module_id']][$tab['tab_id']]['prod'] as $id_product => $name_product) {  ?>
                                        <div id="label-hand-product<?php echo $id_product; ?>"><i class="fa fa-minus-circle"></i> <?php echo $name_product; ?>
                                        <input type="hidden" name="related_tabs[<?php echo $tab['tab_id']?>][products][]" value="<?php echo $id_product; ?>" />
                                        </div>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="related_tabs[<?php echo $tab['tab_id']; ?>][module_id]" value="<?php echo $module['module_id']; ?>" />
                            <input type="hidden" name="related_tabs[<?php echo $tab['tab_id']; ?>][tab_id]" value="<?php echo $tab['tab_id']; ?>" />
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php } else { ?>
        <p><b><?php echo $text_warning_category;?></b></p>
        <?php } ?>
        </div>
        <!-- NeoSeo Featured Products - end -->
            <div class="tab-pane" id="tab-design">
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_store; ?></td>
                      <td class="text-left"><?php echo $entry_layout; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-left"><?php echo $text_default; ?></td>
                      <td class="text-left"><select name="category_layout[0]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($category_layout[0]) && $category_layout[0] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php foreach ($stores as $store) { ?>
                    <tr>
                      <td class="text-left"><?php echo $store['name']; ?></td>
                      <td class="text-left"><select name="category_layout[<?php echo $store['store_id']; ?>]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($category_layout[$store['store_id']]) && $category_layout[$store['store_id']] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
<?php if ($ckeditor) { ?>
ckeditorInit('input-description<?php echo $language['language_id']; ?>', '<?php echo $token; ?>');
<?php } else { ?>
$('#input-description<?php echo $language['language_id']; ?>').summernote({
	height: 300,
    lang:'<?php echo $lang; ?>'
});
<?php } ?>
<?php } ?>
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');

		$('#category-filter' + item['value']).remove();

		$('#category-filter').append('<div id="category-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="category_filter[]" value="' + item['value'] + '" /></div>');
	}
});

$('#category-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script> 
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script></div>
<!-- NeoSeo Blog - begin -->
<script type="text/javascript"><!--
$('input[name=\'blog_article\']').autocomplete({
	'source': function (request, response) {
		$.ajax({
			url: 'index.php?route=blog/neoseo_blog_article/autocomplete&token=<?php echo $token; ?>&article_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function (json) {
				response($.map(json, function (item) {
					return {
						label: item['name'],
						value: item['article_id']
					}
				}));
			}
		});
	},
	'select': function (item) {
		$('input[name=\'blog_article\']').val('');

		$('#category-to-blog-article' + item['value']).remove();

		$('#category-to-blog-article').append('<div id="category-to-blog-article' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="articles_to_blog[]" value="' + item['value'] + '" /></div>');
	}
});

$('#category-to-blog-article').delegate('.fa-minus-circle', 'click', function () {
	$(this).parent().remove();
});
//--></script> 
<!-- NeoSeo Blog - end -->
 <!-- NeoSeo Featured Products - begin -->
<script type="text/javascript"><!--
function deleteTab(id) {
            if (!confirm('<?php echo $text_related_confirm; ?>'))
                return false;
                $('#form-category').attr('action', '<?php echo str_replace("&amp;","&",$delete_related_tab); ?>' + '&related_tab_id=' + id);
                $('#form-category').attr('target', '_self');
                $('#form-category').submit();
                return true;
            }

        var one_touch = false;
            window.token = '<?php echo $token; ?>';
            var template = '<?php echo $tab_related_form; ?>';
            var count_label = <?php echo (isset($tab['tab_id']) ? $tab['tab_id'] : 0) ?> ;
            if (!count_label)
            count = $("#tab-pills a").length; //заменить на максимальный айди фида

            $(".add-new-label").click(function() {
            console.log($(this).parents('.tab-pane:first'));
        if (one_touch)
            return false;
            one_touch = true;
            var name = $("#new-tab-"+$(this).data('module')).val();
            var tmp_template = template.replace("new-tab-name", name);
            tmp_template = tmp_template.replace("module-id", $(this).data('module'));
            $(this).parents('.tab-pane:first').find(".nav-pills li").removeClass('active');
            $(this).parents('.tab-pane:first').find('.tab-content').find('.tab-pane').removeClass('active');
            $(this).parents('.tab-pane:first').find(".nav-pills").prepend("<li class='active'><a data-toggle='tab' href='#tab-label-" + count_label + "' >" + name + "</a></li>");
            $(this).parents('.tab-pane:first').find('.tab-content').append("<div class='tab-pane active' id='tab-label-" + count_label + "'>" + tmp_template + "</div>");
            $(this).parents('.tab-pane:first').find(".nav-pills li:last a").trigger('click');
        });
//--></script>
<script type="text/javascript"><!--
    //Поле Товары для меток
        $('#content').delegate('input[name="tab_product"]', 'focus', function() {
        $('input[name="tab_product"]').autocomplete({
        source: function(request, response) {
        $.ajax({
        url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
            dataType: 'json',
            success: function(json) {
            response($.map(json, function(item) {
            return {
            label: item['name'],
            	value: item['product_id']
            }
            }));
            }
        });
        },
            select: function(item) {
            $('input[name=\'tab_product\']').val('');
                var id_elem = this.id;
                $(this).parent().find('#label-hand-product' + item['value']).remove();
                $(this).parent().find('#label-hand-product-' + id_elem).append('<div id="label-hand-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="related_tabs[' + id_elem + '][products][]" value="' + item['value'] + '" /></div>');
            }
        });
            $('div[id^="label-hand-product"').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
        });
        });
        	$('div[id^="label-hand-product-"').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
        });
//--></script>
 <!-- NeoSeo Featured Products - end -->
<?php echo $footer; ?>