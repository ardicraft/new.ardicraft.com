<?php echo $header; ?>
<div class="container">
    <?php if (file_exists(DIR_MODIFICATION . '/catalog/view/theme/neoseo_unistor/template/common/breadcrumbs.tpl')) { ?>
    <?php require_once(DIR_MODIFICATION . '/catalog/view/theme/neoseo_unistor/template/common/breadcrumbs.tpl'); ?>
    <?php } else { ?>
    <?php  require_once(DIR_TEMPLATE . 'neoseo_unistor/template/common/breadcrumbs.tpl'); ?>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <aside class="search-box box-shadow box-corner">
            <h1><?php echo $heading_title; ?></h1>
            <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control"/>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class='checkbox'>
                        <?php if ($description) { ?>
                        <input type="checkbox" name="description" value="1" id="description" checked="checked"/>
                        <?php } else { ?>
                        <input type="checkbox" name="description" value="1" id="description"/>
                        <?php } ?>
                        <label for='description'><?php echo $entry_description; ?></label>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <select name="category_id" class="form-control">
                        <option value="0"><?php echo $text_category; ?></option>
                        <?php foreach ($categories as $category_1) { ?>
                        <?php if ($category_1['category_id'] == $category_id) { ?>
                        <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
                        <?php } ?>
                        <?php foreach ($category_1['children'] as $category_2) { ?>
                        <?php if ($category_2['category_id'] == $category_id) { ?>
                        <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $category_2['category_id']; ?>">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                        <?php } ?>
                        <?php foreach ($category_2['children'] as $category_3) { ?>
                        <?php if ($category_3['category_id'] == $category_id) { ?>
                        <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class='checkbox'>
                        <?php if ($sub_category) { ?>
                        <input type="checkbox" name="sub_category" id='sub_category' value="1" checked="checked"/>
                        <?php } else { ?>
                        <input type="checkbox" name="sub_category" id='sub_category' value="1"/>
                        <?php } ?>
                        <label for="sub_category">
                            <?php echo $text_sub_category; ?>
                        </label>
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="col-sm-12">
                    <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary"/>
                </div>
            </div>
            </aside>
            <?php if ($products) { ?>
            <div class="filters-box box-shadow box-corner">
                <div class="sort-list col-sm-5">
                    <div class="col-md-5 text-right sort-div">
                        <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                    </div>
                    <div class="col-md-7 text-left">
                        <select id="input-sort" class="selectpicker" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="show-list col-sm-4">
                    <div class="col-md-6 text-right limit-div">
                        <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                    </div>
                    <div class="col-md-6 text-right">
                        <select id="input-limit" class="selectpicker" onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                            <?php if ($limits['value'] == $limit) { ?>
                            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="style-list col-sm-3 text-center ">
                    <div class="btn-group">
                        <button type="button" id="list-view" class="btn btn-view-type btn-default<?php if ($category_view_type == 'list') { echo ' active'; } ?>" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
                        <button type="button" id="grid-view" class="btn btn-view-type btn-default<?php if ($category_view_type == 'grid') { echo ' active'; } ?>" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th-large" aria-hidden="true"></i></button>
                        <button type="button" id="table-view" class="btn btn-view-type btn-default<?php if ($category_view_type == 'table') { echo ' active'; } ?>" data-toggle="tooltip" title="<?php echo $button_table; ?>"><i class="fa fa-list"></i></button>
                    </div>
                </div>

            </div>
            <script type="text/javascript" src="catalog/view/theme/neoseo_unistor/javascript/jquery.formstyler.js"></script>
            <script>
                (function($) {
                    $(function() {
                        $('.filters-box .selectpicker').styler({
                            selectSearch: true
                        });
                    });
                })(jQuery);
            </script>

            <div class="row  products-content">
                <?php foreach ($products as $product) { ?>
                <div class="product-layout product-<?php echo $category_view_type; ?> col-xs-12">
                    <div itemscope="" class="product-thumb box-shadow box-corner clearfix">
                        <div class="product-thumb_top">
                            <?php if( isset($product['labels']) && count($product['labels'])>0 ) { ?>
                            <!-- NeoSeo Product Labels - begin -->
                            <?php foreach($product['labels'] as $label_wrap => $group_label) { ?>
                                <?php foreach($group_label as $label) { ?>
                                    <div class="product-preview-label <?php echo $label['label_type']; ?> <?php echo $label['position']; ?> <?php echo $label['class']; ?>">
                                        <span style="<?php echo $label['style']; ?>">
                                            <?php echo $label['text']; ?>
                                        </span>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <!-- NeoSeo Product Labels - end -->
                            <?php } ?>
                            <div class="image">
                                <a href="<?php echo $product['href']; ?>">
                                    <?php if ($product['thumb']) { ?>
                                    <img  src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="hoverable img-responsive" data-over="<?php echo $product['thumb1']; ?>" data-out="<?php echo $product['thumb']; ?>" />
                                    <?php } else { ?>
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                        <div class="product-thumb_middle">
                            <div class="rating-container">
                                <div class="caption">
                                    <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                </div>
                                <span class="rating">
                                <?php if($product['rating']){ ?>
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($product['rating'] < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php }?>
                                <?php } ?>
                                <?php } ?>
                                </span>
                            </div>
                            <div class="caption">
                                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
							</div>
                            <?php if ($product['price']) { ?>
                            <div class="price-and-cart-add">
                                <div class="price-wrapper">
                                    <p class="price">
                                        <?php if (!$product['special']) { ?>
                                        <?php echo $product['price']; ?>
                                        <?php } else { ?>
                                        <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span>
                                        <?php } ?>
                                        <?php if ($product['tax']) { ?>
                                        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                        <?php } ?>
                                    </p>
                                </div>

                                <div class="input-group input-quantity-group" data-min-quantity="<?php echo $product['minimum']; ?>">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-default" data-type="minus" data-field="input-quantity">
                                      <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                  </span>
                                    <input type="text" name="quantity" value="<?php echo $product['minimum']; ?>" size="2" class="form-control quantity">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-type="plus" data-field="input-quantity">
                                      <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                    </span>
                                </div>

                                <div class="button-group-cart">
                                    <span class="text-right stock-status-text-<?php echo $product['stock_status_id']; ?>" style="color:<?php echo $product['stock_status_color'] ?>;"><?php echo $product['stock_status']; ?></span>
                                    <button class="cart-add-button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs"><?php echo $button_cart; ?></span></button>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="labels-wrap-table">
    <!-- NeoSeo Product Labels - begin -->
                        <?php if( isset($product['labels']) && count($product['labels'])>0 ) { ?>
                            <?php foreach($product['labels'] as $label_wrap => $group_label) { ?>
                                    <div class="wr-tag <?php echo $label_wrap; ?>">
                                       <?php foreach($group_label as $label) { ?>
                                       <div class="tag <?php echo $label['label_type']; ?> <?php echo $label['position']; ?> <?php echo $label['class']; ?>"><span style="<?php echo $label['style']; ?>"><?php echo $label['text']; ?></span></div>
                                       <?php } ?>
                                    </div>
                            <?php } ?>
                        <?php } ?>
                        <!-- NeoSeo Product Labels - end -->
</div>
                        <div class="description">
                            <div class="description-top">
                                <?php if (isset($product['additional_attributes']) && $product['additional_attributes']) { ?>
                                <div class="attributes-top">
                                    <?php $counter = 1; ?>
                                    <?php foreach ($product['additional_attributes'] as $key => $attribute) { ?>
                                    <span><b><?php echo $attribute['name']; ?></b> <?php echo $attribute['text']; ?></span><?php if ($counter < $product['total_attributes']) { echo $divider ? $divider : ''; } ?>
                                    <?php $counter++;
										} ?>
                                </div>
                                <?php } ?>
                                <div class="description-text"><?php echo $product['short_description']; ?></div>
                            </div>
                            <div class="description-bottom">
                                <div class="button-group">
                                    <a class="wishlist-button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                        <i class="fa fa-heart"></i>
                                        <span><?php echo $text_wishlist; ?></span>
                                    </a>
                                    <a class="compare-button" onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                        <i class="ns-clone"></i>
                                        <span><?php echo $text_compare; ?></span>
                                    </a>
                                    <?php if($neoseo_quick_order_status) { ?>
                                    <a class="buy-one-click" onclick="showQuickOrder('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                                        <i class="ns-mouse"></i>
                                        <span><?php echo $text_one_click_buy; ?></span>
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row paginator">

            <!-- NeoSeo Display More BEGIN -->
            <?php if ($status_more_btn_enable) { ?>
            <div class="row">
                <div class="text-center">
                    <button type="button" id="button_show_more" onclick="showMore(this);" class="btn btn-primary" style="margin-bottom: 25px;"><?php echo $button_show_more; ?></button>
                </div>
            </div>

            <script type="text/javascript">
                document.addEventListener("DOMContentLoaded", function(){
                    if (!$('.pagination').length) {
                        $('#button_show_more').hide();
                    } else {
                        <?php if ($status_pagination) { ?>
                        $('.pagination').css('display', 'none');
                        <?php } ?>
                    }
                	let next = $('.pagination li.active').next('li').find('a');
                    if (next.length == 0) {
						$('#button_show_more').hide();
						return;
					}
                });
                function showMore() {
                    let next = $('.pagination li.active').next('li').find('a');

                    if (next.length == 0) {
                    return;
                }

                $.get(next.attr('href'), function (data) {
                    $(data).filter('script').each(function () {
                        if ((this.text || this.textContent || this.innerHTML).indexOf("document.write") >= 0) {
                            return;
                        }
                        $.globalEval(this.text || this.textContent || this.innerHTML || '');
                    });

                    let products = $(data).find('.product-layout').parent().html();

                    $('.product-layout:last-child').after(products);

                    $('.pagination').html($(data).find('.pagination > *'));
                    $('.pagination').parent().next('.text-right').html($(data).find('.pagination').parent().next('.text-right').html());
                    if ($('.pagination li.active').next('li').length == 0) {
                        $('#button_show_more').hide();
                    }
                    var page = parseInt($('.pagination li.active').find('span').html());
                    $('.pagination > li').each(function (){

                        var page2 = parseInt($(this).find('a').html());

                        if(page2 != undefined  && page != undefined ){
                            if(page2 >= <?php echo $page_pagination; ?> && page > page2 ){
                                $(this).html('<span class="box-shadow">'+page2+'</span>');
                                $(this).addClass('active');
                            }
                        }
                    });

                    if (localStorage.getItem('display') == 'list') {
                        $('#list-view').trigger('click');
                        $('#list-view').addClass('active');
                    } else if(localStorage.getItem('display') == 'table') {
                        $('#table-view').trigger('click');
                        $('#table-view').addClass('active');
                    } else {
                        $('#grid-view').trigger('click');
                        $('#grid-view').addClass('active');
                    }
                }, "html");
                return false;
            }
        </script>
      <?php } ?>
      <!-- NeoSeo Display More END -->
    
                <div class="col-sm-7 col-md-12 col-lg-7 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-5 col-md-12 col-lg-5 text-right"><?php echo $results; ?></div>
            </div>

            <?php } else { ?>
                <div class="empty-box box-shadow box-corner">
                    <p class="empty-title"><?php echo $text_empty; ?></p>
                </div>
            <?php } ?>
        </div>
        <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
</div>
<script><!--
    $('#button-search').bind('click', function () {
        //var url = 'index.php?route=product/search';

        let current_language_code =  $('.language__compact-wrap li.active').attr('data-code');

        if (current_language_code === undefined || current_language_code === null) {
            var url = 'index.php?route=product/search';
        } else {
            var url = current_language_code + '/index.php?route=product/search';
        }

        var search = $('#content input[name=\'search\']').prop('value');
        if (search) {
            url += '&search=' + encodeURIComponent(search);
        }

        var category_id = $('#content select[name=\'category_id\']').prop('value');
        if (category_id > 0) {
            url += '&category_id=' + encodeURIComponent(category_id);
        }

        var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');
        if (sub_category) {
            url += '&sub_category=true';
        }

        var filter_description = $('#content input[name=\'description\']:checked').prop('value');
        if (filter_description) {
            url += '&description=true';
        }

        location = url;
    });
    $('#content input[name=\'search\']').bind('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#button-search').trigger('click');
        }
    });
    $('select[name=\'category_id\']').on('change', function () {
        if (this.value == '0') {
            $('input[name=\'sub_category\']').prop('disabled', true);
        } else {
            $('input[name=\'sub_category\']').prop('disabled', false);
        }
    });
    $('select[name=\'category_id\']').trigger('change');
    --></script>

<script>// input-quantity
    $('.input-quantity-group .input-group-btn button').on('click', function () {
        var field = $(this).attr('data-field');
        if( !field ) return;
        var type = $(this).attr('data-type');
        if( !type ) return;

        var I = $(this).parent().siblings('input:first');
        var o = $(this).parents('.input-quantity-group');
        var min = $(this).parents('.input-quantity-group').attr('data-min-quantity');
        var value = Number(I.val());
        if( type == "minus") {
            value -= 1;
        } else {
            value += 1;
        }
        if( value < min ) { setInvalid(I); value = min; }
        I.val(value);
        setQuantity(value,o);
    });
    $('.input-quantity-group input.quantity').keydown(function(e) {
        if(e.which == 38){ // plus
            var value = Number($(this).val());
            var o = $(this).parents('.input-quantity-group');
            value++;
            if(value > 100) value = 100;
            $(this).val(value);
            setQuantity(value, o);
        }
        if(e.which == 40){ // minus
            var value = Number($(this).val());
            var o = $(this).parents('.input-quantity-group');
            var min = $(this).parents('.input-quantity-group').attr('data-min-quantity');
            value--;
            if(value < min) { setInvalid($(this)); value = min; }
            $(this).val(value);
            setQuantity(value, o);
        }
    });
    function setQuantity(v,o){
        o.siblings('.button-group').find('button').each(function(){
            var t = $(this).attr('onclick');
            if( t.indexOf('cart.add') > -1 ){
                var i = t.slice(9,-2).split(',')[0].slice(1,-1);
				var q = "c = cart;c.add('" + i + "', '" + v + "');";
                $(this).attr("onclick",q);
            }
        })
    }
    function setInvalid(o){
        o.addClass('invalid');
        setTimeout(function(){
            o.removeClass('invalid');
        },100);
    }
</script>

<?php echo $footer; ?>