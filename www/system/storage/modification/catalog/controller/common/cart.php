<?php
class ControllerCommonCart extends Controller {

	/* NeoSeo QuickOrder - begin */
	protected function renderTemplate($template, $data)
	{
		extract($data);

		ob_start();
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $template)) {
			$template_file = $this->config->get('config_template') . '/template/' . $template;
		} else {
			$template_file = 'default/template/' . $template;
		}
		require(DIR_TEMPLATE . $template_file);
		$result = ob_get_contents();
		ob_end_clean();

		return $result;
	}
	/* NeoSeo QuickOrder - end */
            
	public function index() {
		$this->load->language('common/cart');

		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_remove'] = $this->language->get('button_remove');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();



        /* NeoSeo QuickOrder - begin */
        $this->language->load("module/neoseo_quick_order");
		$data_quick_order['neoseo_quick_order_status'] = $this->config->get('neoseo_quick_order_status');
		$data_quick_order['neoseo_quick_order_status_popup_cart'] = $this->config->get('neoseo_quick_order_status_popup_cart');
		$data_quick_order['button_quick_order'] = $this->language->get('button_quick_order');
		$data_quick_order['text_quick_order'] = $this->language->get('text_quick_order');
		$data_quick_order['text_no_phone'] = $this->language->get('text_no_phone');
		$data_quick_order['quick_order_phone_mask'] = $this->config->get("neoseo_quick_order_phone_mask");
		$template = $this->config->get('neoseo_quick_order_popup_cart_template');
		$sum_quantity = 0;
		$quick_products = array();
		/* NeoSeo QuickOrder - end */

            
		foreach ($this->cart->getProducts() as $product) {
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			} else {
				$total = false;
			}



        /* NeoSeo QuickOrder - begin */
        if($data_quick_order['neoseo_quick_order_status_popup_cart'] ) {
			$sum_quantity = $sum_quantity + $product['quantity'];
			$quick_products[$product['product_id']] = $product['quantity'] ;
        }
        /* NeoSeo QuickOrder - end */

            
			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'price'     => $price,
				'total'     => $total,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}

		// Gift Voucher


        /* NeoSeo QuickOrder - begin */
        if($data_quick_order['neoseo_quick_order_status_popup_cart'] ) {
			$data_quick_order['sum_quantity'] = $sum_quantity;
			$data_quick_order['products'] =  serialize($quick_products);
			$data_quick_order['products'] = urlencode($data_quick_order['products']);
			$data['neoseo_quick_order_popup_cart_template'] = $this->renderTemplate('module/neoseo_quick_order_' . $template . '.tpl', $data_quick_order);
		}
        /* NeoSeo QuickOrder - end */

            
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'])
				);
			}
		}

		$data['totals'] = array();

		foreach ($total_data as $result) {
			$data['totals'][] = array(
				'title' => $result['title'],
				'text'  => $this->currency->format($result['value']),
			);
		}

		$data['cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

			/* NeoSeo Loyalty System - begin */
			if($this->config->get('neoseo_loyalty_system_status')){
				$data['loyality_data'] = $this->model_total_neoseo_loyalty_system->getTextDiscountInCart();
			} else {
				$data['loyality_data'] = "";
			}
			/* NeoSeo Loyalty System - End */
			

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/cart.tpl', $data);
		} else {
			return $this->load->view('default/template/common/cart.tpl', $data);
		}
	}

	public function info() {
		$this->response->setOutput($this->index());
	}
}
