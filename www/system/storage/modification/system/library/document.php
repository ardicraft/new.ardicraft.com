<?php
class Document {
	private $title;
	private $description;
	private $keywords;
    /* NeoSeo Filter - start */
    private $noindex = false;
    /* NeoSeo Filter - end */
	private $links = array();
	private $styles = array();
	private $scripts = array();
	private $og_image;

    /* NeoSeo Filter - start */
    public function nsf_setLinks($links) {
  	    $this->links = $links;
    }

    public function nsf_setNoindex($state = false) {
  	    $this->noindex = $state;
    }

	public function nsf_isNoindex() {
		return $this->noindex;
	}
    /* NeoSeo Filter - end */

	/* NeoSeo MicroData - begin */
	private $microdata = array();

	public function setMicroData($microdata) {
		$this->microdata[] = $microdata;
	}

	public function getMicroData() {
		return $this->microdata;
	}
	/* NeoSeo MicroData - end */
	public function setTitle($title) {
		$this->title = $title;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	public function getKeywords() {
		return $this->keywords;
	}

	public function addLink($href, $rel) {
		$this->links[$href] = array(
			'href' => $href,
			'rel'  => $rel
		);
	}

	public function getLinks() {
		return $this->links;
	}

	public function addStyle($href, $rel = 'stylesheet', $media = 'screen') {
		global $config;
		if($config->get('neoseo_optimizer_status')) {
			if ($config->get("neoseo_optimizer_minify_css")) {
				$header_href = "/media/" . (str_replace("/", "_", $href));
			} else {
				$header_href = "/" . $href;
			}
			$css_pushes = $config->get('css_pushes');
			if(!in_array($href, $css_pushes)){
				$link = 'Link: <' . $header_href . '>; rel=preload; as=style';
				header($link, false);
				$css_pushes[] = $href;
				$config->set('css_pushes', $css_pushes);
			}
		}
		$this->styles[$href] = array(
			'href'  => $href,
			'rel'   => $rel,
			'media' => $media
		);
	}

	public function getStyles() {
		return $this->styles;
	}

	public function addScript($href, $postion = 'header') {
		global $config;
		if($config->get('neoseo_optimizer_status')) {
			if ($config->get("neoseo_optimizer_minify_js")) {
				$header_href = "/media/" . (str_replace("/", "_", $href));
			} else {
				$header_href = "/" . $href;
			}
			$js_pushes = $config->get('js_pushes');
			if(!in_array(basename($href), $js_pushes)){
				$link = 'Link: <' . $header_href . '>; rel=preload; as=script';
				header($link, false);
				$js_pushes[] = basename($href);
				$config->set('js_pushes', $js_pushes);
			}
		}
		$this->scripts[$postion][$href] = $href;
	}

	public function getScripts($postion = 'header') {
		if (isset($this->scripts[$postion])) {
			return $this->scripts[$postion];
		} else {
			return array();
		}
	}

	public function setOgImage($image) {
		$this->og_image = $image;
	}

	public function getOgImage() {
		return $this->og_image;
	}
}
